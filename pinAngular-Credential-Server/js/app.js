var pin_app =  angular.module( 'pinApp', ['ui.router',
                                                    'pinLoginCtrl.controllers',
                                                    'LocalStorageModule',
                                                    'pinApp.services',
                                                    'pinApp.directives',
                                                    'pinResetPassword.controllers',
                                                    'pinMenu.controllers',
                                                    'pinHome.controllers',
                                                    'pinAddCompany.controllers',
                                                    'pinTagCtrl.controllers',
                                                    'pinCompanyDetailsCtrl.controllers',                     'angularMoment',
                                                    'ui.bootstrap',
                                                    'flow',
                                                    'base64',
                                                    'ngSanitize',
                                                    'rzModule',
                                                    ] )

pin_app.config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('ls');
        localStorageServiceProvider.setNotify(true, true);

    }]);


pin_app.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        singleFile: true
    };
}])

pin_app.config(function($provide) {
    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceReload = function() {
            return $delegate.go($delegate.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });
});


pin_app.config(function($stateProvider, $urlRouterProvider,$httpProvider,$locationProvider) {
    
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.post['Accept'] = 'application/json,text/javascript';

    
    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
   
    $httpProvider.defaults.useXDomain = true;

   
  
        $stateProvider
            .state('login', {
                url:'/login',
                templateUrl:'partials/login.html',
                controller: 'LoginCtrl',
                restricted: false,
                reload:true
        })

        $stateProvider
            .state('home', {
                url:'/home',
                templateUrl:'partials/home.html',
                controller: 'homeCtrl',
                restricted: false,
                reload:true
        })

        $stateProvider
            .state('add-company', {
                url:'/add-company',
                templateUrl:'partials/add-company.html',
                controller: 'addCompanyCtrl',
                restricted: false,
                reload:true
        })  


        $stateProvider
            .state('company-details', {
                url:'/company-details',
                templateUrl:'partials/company-details.html',
                controller: 'detailsCtrl',
                params: {'company_obj' : null, 'company_id' : null},
                restricted: false,
                reload:true
        })  

        $stateProvider
            .state('tags', {
                url:'/tags',
                templateUrl:'partials/tags.html',
                controller: 'tagsCtrl',
                restricted: false,
                reload:true
        })      


        .state('resetpassword', {
                url:'/resetpassword',
                templateUrl:'partials/resetPassword.html',
                controller: 'ctrResetPassword',
                restricted: true,
                reload:true
        })

        .state('changepwd', {
                url:'/changepwd',
                templateUrl:'partials/changePwd.html',
                controller: 'changePwdCtrl',
                restricted: true,
                reload:true
        })
        .state('error', {
                url: '/error',
                templateUrl: 'partials/error.html',           
              
        })
        
       $urlRouterProvider.otherwise('/home');
       $httpProvider.interceptors.push('APIInterceptor'); 
    /*
      $locationProvider.html5Mode({
    enabled: true,
  
  });*/
});

pin_app.run(function($rootScope, localStorageService) {
    if (!localStorageService.isSupported) {
        $rootScope.storageType = 'cookieStorage';
    }
    else if (localStorageService.getStorageType().indexOf('session') >= 0) {
        $rootScope.storageType = 'sessionStorage';
    }
    else {
        $rootScope.storageType = 'localStorage';
    }
});

// Used to show current tab as active on a navigation bar
pin_app.run(['$rootScope', '$location', function($rootScope, $location){
   var path = function() { return $location.path();};
   $rootScope.$watch(path, function(newVal, oldVal){
     $rootScope.activetab = newVal;
   });
}]);


pin_app.run(function($rootScope, $location,$window) {
    $rootScope.$on('$locationChangeSuccess', function() {
     
        if ( $rootScope.user == null && $rootScope.path != $location.path()) 
        {
             $location.path('/login');
         
        }
       

    });
});
