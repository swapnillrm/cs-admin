var app = angular.module('pinMenu.controllers', []);

app.controller('menuCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {

  if(localStorageService.get('userData'))
   {
		$rootScope.user = localStorageService.get('userData');
	 } 
   else
    {
		delete $rootScope.user;
		}

   $scope.getPartial = function () 
   {
    $scope.headerTemplate= "";
    if(($rootScope.user != undefined) ||  (localStorageService.get('userData')!= null) )
    {    
      if(localStorageService.get('userData').role == 'ROLE_ADMIN')
      {
      
        $scope.templates = {
         template: { url: 'partials/AdminHeader.html' }
        };

       $scope.headerTemplate = $scope.templates.template.url;
        return $scope.headerTemplate;
      }

       else 
        {
        
          $scope.templates = {
           template: { url: 'partials/AdminHeader.html' }
          };

         $scope.headerTemplate = $scope.templates.template.url;
           return $scope.headerTemplate;
        }
      }
}

$scope.navDetails = function()
    {
      if($rootScope.user != null)
      {
          $scope.user.Name = $rootScope.user.uName; 
        
      }
      else if(localStorageService.get('userData')) 
      {
      $rootScope.user = localStorageService.get('userData'); 
      $scope.user.Name = $rootScope.user.uName;
    
    } 

 else {
    delete $rootScope.user;
    } 

}

  $scope.goHome = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 25000});
      
    $state.go('home',{},{});
  }


    $scope.LogoutSuccess = function(resData) {

			if(resData.status == 'SUCCESS')
      {
        delete $rootScope.user; 
        delete $http.defaults.headers.common['X-Auth-Token'];
        window.localStorage.removeItem(localStorageService.get('authToken'));
        localStorage.clear();
        localStorageService.clearAll();  
       
       
        $state.go('login', {}, {reload:true});       
        
			}

			else
			{
        delete $rootScope.user; 
        delete $http.defaults.headers.common['X-Auth-Token'];
        window.localStorage.removeItem(localStorageService.get('authToken'));
        localStorage.clear();
        localStorageService.clearAll();  
       
       
        $state.go('login', {}, {reload:true}); 
      
      }
		}

		$scope.logOutUser = function() {
      var reqData = {};
      $.blockUI({ message: '<h4> Logging Out...</h4>'});
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.logoutUser(reqData, $scope.LogoutSuccess)

      //$state.go('login', {}, {reload:true}); 
		}



}])
