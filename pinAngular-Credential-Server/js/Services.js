/*
  Version 0.0.1
  Created by Mrunmayi Davare.
*/
var app = angular.module('pinApp.services',['ngResource']);
//var live = "http://10.0.1.40:8181"; //saurabh left right mind internet
//var live = "http://10.10.10.181:8181"; //saurabh mobile id internet
//var live = "http://10.10.10.164:8181"; // shrish's mobile id
//var live = "http://10.0.1.14:8181"; // chetan's mobile id
//var live = "http://10.0.1.12:8181"; // chetan's mobile id
//var live = "http://10.10.10.56:8181"; // arjuns's mobile id
//var live = "http://52.41.4.52:8080/gs-rest-service-0.1.0"; //live aws link
//var live = "http://10.10.10.176:8080/cs-admin-service-0.1.0"; //local server link
var live = "http://10.10.10.136:8081"; // swapnil mobile id



var path = live;
/*========================User Services for Login =================================*/
app.service("userService", function($resource){    
    
    this.loginUser = function(reqData, nextStep){
     
       var src = $resource(path +'/auth/login' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 

        src.get({},
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
                         
          });
    };

    this.loginToBase = function(reqData, nextStep ,baseUrl){
      var csBaseUrl = baseUrl;
      console.log('trying to see base url in services' +JSON.stringify(csBaseUrl))
       var src = $resource(csBaseUrl +'/auth/login' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 

        src.get({},
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
                         
          });
    };

     this.logoutUser = function(reqData, nextStep){
       var src = $resource(path +'/auth/logout' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      
        src.get({},
            function(result){
                nextStep.call(this, result)
          });
    };

    this.forceLogout = function(reqData, nextStep ,baseUrl){
      var csBaseUrl = baseUrl;
     
       var src = $resource(csBaseUrl +'/auth/forced_logout' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 

        src.post(reqData,
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
                         
          });
    };

  this.getAllCompanies = function(reqData, nextStep){
       var src = $resource(path +'/company/getallcompanies' ,{},{'get':{method:'GET'}}); 
      
        src.get('',
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
              });       
    };

  this.addCompany = function(reqData, nextStep){
       var src = $resource(path +'/company/addcompany' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

 this.getCompanyDetailsById = function(reqData, nextStep){
       var src = $resource(path +'/company/getcompanyprofile' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };


  this.activateCompany = function(reqData, nextStep){
       var src = $resource(path +'/company/activatecompany' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    }; 
    

  this.deactivateCompany = function(reqData, nextStep){
       var src = $resource(path +'/company/deactivatecompany' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
  }; 

  this.updateCompanyInDB = function(reqData, nextStep){
       var src = $resource(path +'/company/updatecompanyprofile' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
  }; 


  this.getTokenFromDB = function(reqData, nextStep,baseUrl){
       var csBaseUrl = baseUrl;
       var src = $resource(csBaseUrl +'/syscon/getToken' ,{},{'save':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
  }; 


  this.updateCompanyDomainInDB = function(reqData, nextStep){
       var src = $resource(path +'/company/updatecomdomains' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };   

  this.getCompanyDomanis = function(reqData, nextStep){      
       var src = $resource(path +'/company/getcomdomains' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
  }; 

  this.addHRInPin = function(reqData, nextStep, baseUrl){
       var pinBaseUrl = baseUrl; 
       var src = $resource(pinBaseUrl +'/user/addhradmin' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
  };
 
  this.getAdminsByCompany = function(reqData, nextStep, baseUrl){
      var pinBaseUrl = baseUrl; 
      var src = $resource(pinBaseUrl +'/user/getallhradmins' ,{},{'save':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                 if(result.status == 401 || result.status == 404){
                  resetAlert()
                  alertify.alert('Data not found')
                }
                else{
                  nextStep.call(this, result);
                }
          });
  };

    this.deactivateUser = function(reqData, baseUrl, nextStep){
      var pinBaseUrl = baseUrl; 
      var src = $resource(pinBaseUrl +'/user/deactivateemployee',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.activateUser = function(reqData, baseUrl, nextStep){
       var pinBaseUrl = baseUrl; 
       var src = $resource(pinBaseUrl +'/user/activateemployee',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.activateInBase = function(reqData, nextStep, baseUrl){
       var pinBaseUrl = baseUrl; 
       var src = $resource(pinBaseUrl +'/syscon/activate_license',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.verifyLicense = function(reqData, nextStep, baseUrl){
       var pinBaseUrl = baseUrl; 
       var src = $resource(pinBaseUrl +'/syscon/verify_license',{},{'save':{method:'GET'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.getUsersCount = function(reqData, nextStep, baseUrl){
       var pinBaseUrl = baseUrl; 
       var src = $resource(pinBaseUrl +'/user/getusercount',{},{'save':{method:'GET'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

  /***************************************************************************************/   
    
 /*   this.getObjByTag = function(reqData, nextStep){
       var src = $resource(path +'/objective/getobjbytags' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    }; */


    
    this.uploadProfile = function(reqData, nextStep){
       var src = $resource(path +'/user/uploadprofileimage' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

 


    this.magicAPI = function(reqData, nextStep){
       var src = $resource(path +'/userrelationships/getmagic',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.savePinToDB = function(reqData, nextStep){
       var src = $resource(path +'/pin/addpin',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updatePasswordfromMail = function(reqData, nextStep){
       var src = $resource(path +'/openservices/updatepasswordusingtoken',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
     this.changepassword = function(reqData, nextStep){
       var src = $resource(path +'/user/updateuserpassword',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updateRelFromDB = function(reqData, nextStep){
       var src = $resource(path +'/userrelationships/updateuserrelationship',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.exportHistoryToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/producecsv' ,{},{'save':{method:'POST'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
  this.importHistoryToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/uploadcsv' ,{},{'save':{method:'POST'},headers: {'Content-Type': undefined}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.importTagsToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/uploadtagcsv' ,{},{'save':{method:'POST'},headers: {'Content-Type': undefined}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.importObjToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/uploadobjectivecsv' ,{},{'save':{method:'POST'},headers: {'Content-Type': undefined}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

   this.getChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/getdata' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

 this.getAvgChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/avg' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

     this.getAvgObjectiveChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/avgobjseries' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

      this.getAvgPerObjectiveChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/ratingsperobjective' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
      this.getobjectivesParPinnee = function(reqData, nextStep){
       var src = $resource(path +'/charts/avgobjseriesobjs' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.sendEmailInlink = function(reqData, nextStep){
       var src = $resource(path +'/openservices/forgotpassword' ,{},{'post':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.updateSettings = function(reqData, nextStep){
       var src = $resource(path +'/syscon/updatesettings' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.getAllsettings = function(reqData, nextStep){
       var src = $resource(path +'/syscon/getsettings' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json'}});  
      
        src.get({},
            function(result){
               
                  nextStep.call(this, result);
                
              });       
          };


}) 


/*========================User Services =================================*/
  app.service('APIInterceptor', function($rootScope, $location) {
    this.responseError = function(response) {
    //  console.log(JSON.stringify(response));
        if (response.data.status === 401) {
            $rootScope.$broadcast('unauthorized');
           // alert(response.data.message);
            $location.path('/login');
        }
        return response;
    };
})



/*app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);*/