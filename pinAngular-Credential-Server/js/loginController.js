var app = angular.module('pinLoginCtrl.controllers', []);

app.controller('LoginCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http','$window',
	function($scope,userService, $state, $rootScope,localStorageService,$http,$window) {
  $.unblockUI();
   // manageMenuBar(false);
    $(".wrapperClass").css("padding",0);
    $('#sidebarRight').hide()
    $('#sidebar').hide()
    $('.cl-sidebar').hide()
    var storageType = localStorageService.getStorageType();
    console.log('storageType '+$rootScope.storageType);


  $scope.sidebar = false;
  autoHeight();

$scope.LoginUserSuccess = function(resData) {
	    $.unblockUI();
	    if (resData.status = 'SUCCESS' && resData.data != null) 
	    {

	      $rootScope.user = resData.data;
	      $scope.user = resData.data;
	      $rootScope.loggedIn = true;
        $rootScope.authToken = resData.data.authstring;



              if($rootScope.storageType == 'localStorage') 
              {
                  localStorageService.set('userData',$rootScope.user);
                  localStorageService.set('authToken', $rootScope.user.authstring);
              }

              console.log('data from local storage service '+ JSON.stringify(localStorageService.get('userData')))
              $scope.sidebar = true;
              //$scope.notifications();
              if(resData.data.role =="ROLE_ADMIN")
                {
                  $rootScope.isAdmin = true;
                  $http.defaults.headers.common['Authorization'] = 'Basic ' +resData.data.authstring;
                  $state.go('home',{});
                }

                else
                {
                  $rootScope.isAdmin = true;
                  $http.defaults.headers.common['Authorization'] = 'Basic ' +resData.data.authstring;
                  $state.go('home',{});
                }
	    } 
	    else {
            if (resData.status = 'FAILURE')
      	    {
      	      resetAlert();
      	      alertify.alert(resData.msg);
              $state.go('login',{});
      	      
      	    }
          else{
                $state.reload()
                resetAlert();
                alertify.alert('Try again.');
              } 
            }   
	}
  
   
  $rootScope.isAdmin = false;

     $scope.login = function() {

        $.blockUI({ message: '<h4> Loading...</h4>',timeout: 2500});

     	  var reqData= {}
        reqData.uName = $scope.userName;
        reqData.pwd = $scope.passWord ;
        //reqData.role = 'ROLE_ADMIN' ; //use this if api is not working

        console.log('reqData '+JSON.stringify(reqData))
        $http.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
        $http.defaults.headers.post['Accept'] = 'application/json,text/javascript';

    
        $http.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
       
        $http.defaults.headers.common['Authorization'] = 'Basic ' +btoa($scope.userName + ':' + $scope.passWord);
        $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
        userService.loginUser(reqData, $scope.LoginUserSuccess);
        
    }




 



 $scope.sendMailSuccess = function(response)
    {
        if(response.status == 'SUCCESS')
        {
            resetAlert();
            alertify.alert("Check Your Mail to Reset Your Password");
             //$.unblockUI();
              closeModal();
             $state.go('login', {}, {reload: true}); 

        }
        else
        {
            resetAlert();
            alertify.alert("Your Mail Id Is not registered with us Please Signup");
           // $.unblockUI();
           closeModal();
            $state.go('login', {}, {reload: true}); 

        }

    }
    $('#resetbtn').one('click', function() {
    $('#resetbtn').attr('disabled',true);
}); 

    $scope.resetPasswordSend = function()
    {
      // blockUI();
        var reqData = {}
        reqData.email = $scope.resetEmail;
        userService.sendEmailInlink(reqData, $scope.sendMailSuccess)
            
    }

  
}])
