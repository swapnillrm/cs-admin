var app = angular.module('pinApp.directives', []);
app.directive('noSpecialChar', function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function(inputValue) {
          if (inputValue == undefined)
            return ''
          cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
          if (cleanInputValue != inputValue) {
            modelCtrl.$setViewValue(cleanInputValue);
            modelCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    }
  });

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.directive('datetimez', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
           var today=new Date(); 
           
          element.datetimepicker({
                     autoclose: true,
              componentIcon: '.s7-date',
              navIcons:{
                rightIcon: 's7-angle-right',
                leftIcon: 's7-angle-left'
              },
            dateFormat:'dd/MM/yyyy hh:ii:ss',
            endDate:new Date(),

            startDate:new Date(today.setDate(today.getDate() - 7)),
            language: 'pt-BR'
          }).on('changeDate', function(e) {
            ngModelCtrl.$setViewValue(e.date);
            scope.$apply();
          });
        }
    };
});

/*var breadcrumbs = function($state, $stateParams) {
    return {
        restrict: 'E',
        templateUrl: 'partials/breadcrumb.html',
        replace: true,
        compile: function(tElement, tAttrs) {
            return function($scope, $elem, $attr) {
                $scope.show = function(state){
                    if(!angular.isDefined(state.data)){
                        return false;
                    }
                    else if(!angular.isDefined(state.data.breadcrumbs)){
                        return false;
                    }
                    return true;
                };
            }
        }
    };
};*/
/*
app.directive("breadcrumbs", function() {
    return {
        restrict: 'E',
        replace: true,
        priority: 100,
        templateUrl: 'partials/breadcrum.html'
    };
});
*/
/*
app.directive("breadcrumbs", function(breadcrumbService) {
      return {
        restrict: 'E',
        replace: true,
        priority: 100,
        templateUrl: 'partials/breadcrum.html',
        link: function($scope) {
          
          $scope.$on('$stateChangeSuccess', function(){
            // Add the current state and params to the scope
            // $scope.current = $state.$current;
            // $scope.params = $stateParams;
            breadcrumbService.generate();
            $scope.breadcrumbList = breadcrumbService.list();
          });
        }
      };
    });*/

  /*  (function(angular) {
    'use strict';

    angular
        .module('ui.breadcrumb')
        .directive('uiBreadcrumb', uiBreadcrumb);

    function uiBreadcrumb($breadcrumb) {

        return {
            restrict: 'AE',
            templateUrl: $breadcrumb.getOption('templateUrl'),
            controller: BreadcrumbController,
            controllerAs: 'vm',
            bindToController: true
        };

        function BreadcrumbController($rootScope, $breadcrumb) {
            var vm = this;

            $rootScope.$on('$viewContentLoaded', updateSteps);
            updateSteps();

            function updateSteps() {
                $breadcrumb.getSteps().then(function (steps) {
                    vm.steps = steps;
                });
            }
        }
    }

})(angular);*/

/*app.directive("select2",function(){
    return {
        restrict: 'AC',
        link: function(scope, element, attrs) {
            var options = [],
                el = $(element),
                angularTriggeredChange = false,
                selectOptions = attrs["selectOptions"].split(" in "),
                property = selectOptions[0],
                optionsObject = selectOptions[1];
            // watch for changes to the defining data model
            scope.$watch(optionsObject, function(n, o){
                var data = [];
                // format the options for select2 data interface
                for(var i in n) {
                    var obj = {id: i, text: n[i][property]};
                    data.push(obj);
                }
                el.select2({data: data});
                // keep local copy of given options
                options = n;
            }, true);
            // watch for changes to the selection data model
            scope.$watch(attrs["selectSelection"], function(n, o) {
                // select2 is indexed by the array position,
                // so we iterate to find the right index
                for(var i in options) {
                    if(options[i][property] === n) {
                        angularTriggeredChange = true;
                        el.val(i).trigger("change");
                    }
                }
            }, true);
            // Watch for changes to the select UI
            el.select2().on("change", function(e){
                // if the user triggered the change, let angular know
                if(!angularTriggeredChange) { 
                    scope.$eval(attrs["selectSelection"]+"='"+options[e.target.value][property]+"'");
                    scope.$digest();
                }
                // if angular triggered the change, then nothing to update
                angularTriggeredChange = false;
            });
            
        }
    };
});*/



app.directive('appFilereader', function(
    $q
  ) {
    
    var slice = Array.prototype.slice;

    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, element, attrs, ngModel) {
        if (!ngModel) return;

        ngModel.$render = function() {}

        element.bind('change', function(e) {
          var element = e.target;
          if(!element.value) return;

          element.disabled = true;
          $q.all(slice.call(element.files, 0).map(readFile))
            .then(function(values) {
              if (element.multiple) ngModel.$setViewValue(values);
              else ngModel.$setViewValue(values.length ? values[0] : null);
              element.value = null;
              element.disabled = false;
            });

          function readFile(file) {
            var deferred = $q.defer();

            var reader = new FileReader()
            reader.onload = function(e) {
            //  deferred.resolve(e.target.result);
           //  deferred.resolve(file.name);

                         // if(file.size < 1048576){
                          //var fileName = file.name.replace(/ /g, "_");
                          var fileName = file.name.replace(/\s+/g, '');
                          bstring = e.target.result;
                          baseString = bstring.split('base64,');
          
                           scope.ngModel = {
                               // lastModified: file.lastModified,
                               // lastModifiedDate: file.lastModifiedDate,
                                //name: file.name,
                                //size: file.size,
                                name: fileName,
                                fileType: file.type,
                                base64String: baseString[1]

                            };
                           
                             deferred.resolve(scope.ngModel);
                       
                           //console.log('ggggg '+fileName);


                   
            }
            reader.onerror = function(e) {
              deferred.reject(e);
            }
            reader.readAsDataURL(file);

            return deferred.promise;
          }

        }); //change

      } //link

    }; //return

  }) //appFilereader
;

app.directive('ngFiles', ['$parse', function ($parse) {

            function fn_link(scope, element, attrs) {
                var onChange = $parse(attrs.ngFiles);
                element.on('change', function (event) {
                    onChange(scope, { $files: event.target.files[0] });
                   
                });
            };

            return {
                link: fn_link
            }
        } ])


app.directive('showtab',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.click(function(e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    });