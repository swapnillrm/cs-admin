var app = angular.module('pinHome.controllers', []);

app.controller('homeCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http','$window',
	function($scope,userService, $state, $rootScope,localStorageService,$http,$window) {
  $.unblockUI();
   // manageMenuBar(false);


    
  var storageType = localStorageService.getStorageType();
  console.log('storageType '+$rootScope.storageType);

  if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $scope.allCompList = [];



  scrollPageToTop(); //scrolling page to top 
  
  showSideBar(); // to show side bar




/*
  $scope.goDetails = function(){
    $state.go('company-details', {}, {reload : true});
  }
*/

/*    GET ALL USERS   */


// this code also handles any other error status like 401,400,500 etc.
$scope.getCompaniesSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
        $.unblockUI();
        
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            $scope.allCompList = resData.data;
            console.log('all users list '+JSON.stringify(resData.data))
      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          $state.reload()

          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          $state.go('login', {},{'reload':true});

          resetAlert();
          alertify.alert('Unauthorized access');
      }
  }
  
} 

 $scope.getCompanies = function() {
      $.blockUI({ message: '<h4> Loading companies...</h4>' });
      var reqData = {};
      
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
         
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET'; 
    
      userService.getAllCompanies(reqData,$scope.getCompaniesSuccess);
  }
  $scope.getCompanies();


$scope.showCompanyDetails = function(comp, companyId) {            
  if($rootScope.user){
      $scope.getProjectbyId(comp, companyId); 
  }
}

$scope.getProjectbyId = function(comp, companyId){
        var reqData ={};
        reqData.id = companyId;

        /*userService.getCompanyById(reqData, function(resData){
            if(resData.status == 'SUCCESS'){
                $scope.companyObj = resData.data;
               
                blockUI({ message: '<h4> Loading details...</h4>' });*/
                 localStorageService.set('companyData',comp)
                 localStorageService.set('companyId',companyId)

                 $state.go('company-details', {'company_id':companyId , 'company_obj' : comp});
           /* }else{
                
                resetAlert();
                alertify.alert("Company details not found.");
            }
            
        })*/

    }






$scope.pfImage='';

$scope.imgSizeRestrict=false;



$scope.profileImgUpload = function(){

  $scope.imageStrings = [];
  $scope.processFiles = function(files){
    angular.forEach(files, function(flowFile, i){
       var fileReader = new FileReader();
$('#imgUpload').show();

        $scope.fileSize= flowFile.file.size/1024;

        console.log('file size '+ $scope.fileSize)

        if($scope.fileSize > 0){
          $('#fileUploadBtn').css('display','inline');
        }

       if($scope.fileSize > 1024){
        $('#fileUploadBtn').attr('disabled','disabled');
        $('#errText').html('<p class="row errorSpan"> File size is too large to upload. Please upload another image.</p>')
        $scope.imgSizeRestrict = true;
       }
       else{

        if(flowFile.file.type == 'image/png' || flowFile.file.type == 'image/jpg' || flowFile.file.type == 'image/jpeg' || flowFile.file.type == 'image/gif')
         {
            $('#fileUploadBtn').removeAttr('disabled');
            $('#errText').css('display','none');
            $scope.imgSizeRestrict=false;
            fileReader.onload = function (event) {
            var uri = event.target.result;
              $scope.imageStrings[i] = uri;

               $scope.typeofFile = flowFile.file.type; 
               $scope.nameofFile = flowFile.file.fileName;  
                
          };
          fileReader.readAsDataURL(flowFile.file);
        }
        else
        {
            $('#fileUploadBtn').attr('disabled','disabled');
            $('#errText').css('display','block');
            $('#errText').html('<p class="row errorSpan"> Allowed formats are jpg,jpeg and png. Please upload another image.</p>')
            $scope.imgSizeRestrict = true;
        }
       }
          
    });
  };
       
}



/* Profile Image to DB */

$scope.isUploading = false;
$scope.profileOfUserSuccess = function(resData) {
      $.unblockUI();
      if(resData.status == "SUCCESS")
      {
           closeModal();
           $state.reload();
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
           
            resetAlert();
            alertify.alert('Profile image changed.')
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
            $state.reload();
            resetAlert();
            alertify.alert('Update not successful.')
      }
  }
} 

 $scope.saveProfile = function(id) {
 // $.blockUI({ message: '<h4> Uploading image.. </h4>' });

      $scope.isUploading = true;
      $('#edit-profile').css('background-color','#37373b');
      $('.login-modal-content').css('opacity','0.8');
      
      var reqData = {};

      $scope.imgProfile = JSON.parse($('#profileImg').val()); 
      $scope.img = JSON.stringify($scope.imgProfile)
         

          $scope.imgType = $scope.img.slice(2);


          $scope.imgProfileOrg2 = $scope.imgType.split('"');
          var projectImage= $scope.imgProfileOrg2[0];
          $scope.base = projectImage.split('base64,');
    
      reqData.base64String = $scope.base[1];
      reqData.fileType = $('#typeoffile').val();
      reqData.id = id;

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.uploadProfile(reqData,$scope.profileOfUserSuccess);
  }
//Profile Image to DB



// to set modal to pristine after clicking close for edit company 
$scope.resetModal = function()
{

  $scope.editCompanyForm.$setUntouched(); // to make the form untouched
  $scope.editCompanyForm.$setPristine();  // to make the form pristine
  $scope.company = '';
}




        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
  
