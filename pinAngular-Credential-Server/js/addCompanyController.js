var app = angular.module('pinAddCompany.controllers', []);


/********************************  Objective controller  *************************/

app.controller('addCompanyCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$modal','$log','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$modal,$log,$http) {
  
  $scope.register = {};


//manageMenuBar(true);
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }
  
  $scope.user.role= $rootScope.user.role;
    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

  panelHeight(); //for window height
  removeModalData(); //remove data 
  scrollPageToTop(); //scrolling page to top 
  useSelect2(); //using select tags
  useTags(); //using input as tags
   


/*   Get objectives   */  
$scope.tagsSeleted = [];


/*  ADD OBJECTIVES  */  
$scope.saveCompanySuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
           
            resetAlert();
            alertify.alert('Company added successfully.');
            $state.go('home',{},{reload : true})
            
        }
        else
        {  
            $state.reload();
            resetAlert();
            alertify.alert('Company insertion failed.');
            
        }  
    }
    
$scope.saveCompany = function() {
        
        var reqData = {};
        reqData.com_name = $scope.register.com_name;
        reqData.com_logo = $scope.register.com_logo;
        reqData.com_baseurl = $scope.register.com_baseurl;
        reqData.contact_person = $scope.register.contact_person;
        reqData.email = $scope.register.email;
        reqData.alt_email = $scope.register.alt_email;
        reqData.location = $scope.register.location;
        reqData.com_website = $scope.register.com_website;
        reqData.phone = $scope.register.phone;
        
        var domain_names= $('#dom').val().toString();
        
        reqData.com_domains_list = domain_names;
      
      console.log('data to add company ' + JSON.stringify(reqData));
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';

      userService.addCompany(reqData, $scope.saveCompanySuccess)
    }


$scope.pfImage='';

$scope.imgSizeRestrict=false;



$scope.profileImgUpload = function(){

  $scope.imageStrings = [];
  $scope.processFiles = function(files){
    angular.forEach(files, function(flowFile, i){
       var fileReader = new FileReader();
$('#imgUpload').show();

        $scope.fileSize= flowFile.file.size/1024;

        console.log('file size '+ $scope.fileSize)

        if($scope.fileSize > 0){
          $('#fileUploadBtn').css('display','inline');
        }

       if($scope.fileSize > 1024){
        $('#fileUploadBtn').attr('disabled','disabled');
        $('#errText').html('<p class="row errorSpan"> File size is too large to upload. Please upload another image.</p>')
        $scope.imgSizeRestrict = true;
       }
       else{

        if(flowFile.file.type == 'image/png' || flowFile.file.type == 'image/jpg' || flowFile.file.type == 'image/jpeg' || flowFile.file.type == 'image/gif')
         {
            $('#fileUploadBtn').removeAttr('disabled');
            $('#errText').css('display','none');
            $scope.imgSizeRestrict=false;
            fileReader.onload = function (event) {
            var uri = event.target.result;
              $scope.imageStrings[i] = uri;

               $scope.typeofFile = flowFile.file.type; 
               $scope.nameofFile = flowFile.file.fileName;  
                
          };
          fileReader.readAsDataURL(flowFile.file);
        }
        else
        {
            $('#fileUploadBtn').attr('disabled','disabled');
            $('#errText').css('display','block');
            $('#errText').html('<p class="row errorSpan"> Allowed formats are jpg,jpeg and png. Please upload another image.</p>')
            $scope.imgSizeRestrict = true;
        }
       }
          
    });
  };
       
}



/* Profile Image to DB */

$scope.isUploading = false;
$scope.profileOfUserSuccess = function(resData) {
      $.unblockUI();
      if(resData.status == "SUCCESS")
      {
           closeModal();
           $state.reload();
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          var user_id = (localStorageService.get('userId')) ;
          $scope.getUserByID(user_id);
           
            resetAlert();
            alertify.alert('Profile image changed.')
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
            $state.reload();
            resetAlert();
            alertify.alert('Update not successful.')
      }
  }
} 

 $scope.saveProfile = function() {
  $.blockUI({ message: '<h4> Uploading image.. </h4>' });
      console.log('Id for file upload '+ $scope.userID_original)
      $scope.isUploading = true;
      $('#edit-profile').css('background-color','#37373b');
      $('.login-modal-content').css('opacity','0.8');
      
      var reqData = {};

      $scope.imgProfile = JSON.parse($('#profileImg').val()); 
      $scope.img = JSON.stringify($scope.imgProfile)
         

          $scope.imgType = $scope.img.slice(2);


          $scope.imgProfileOrg2 = $scope.imgType.split('"');
          var projectImage= $scope.imgProfileOrg2[0];
          $scope.base = projectImage.split('base64,');
    
      reqData.base64String = $scope.base[1];
      reqData.fileType = $('#typeoffile').val();
      reqData.id = $scope.userID_original;

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      //userService.uploadProfile(reqData,$scope.profileOfUserSuccess);

      console.log('profile upload-   '+JSON.stringify(reqData));
  }
//Profile Image to DB





// to set modal to pristine after clicking close
$scope.resetForm = function()
{

  $scope.addCompanyForm.$setUntouched(); // to make the form untouched
  $scope.addCompanyForm.$setPristine();  // to make the form pristine
  $scope.register = '';
}


        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };

/*********************** Function calls *********************/      
 
 

  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
