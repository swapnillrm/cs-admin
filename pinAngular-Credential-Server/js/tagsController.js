var app = angular.module('pinTagCtrl.controllers', []);


app.controller('tagsCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http','$modal','$log',
  function($scope,userService, $state, $rootScope,localStorageService,$http,$modal,$log) {
  
//manageMenuBar(true);
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $scope.req = {};

  panelHeight() //panel window height
  scrollPageToTop(); //scrolling page to top 
  removeModalData(); //remove data 


/*   Get tags   */  
$scope.allTags = [];

$scope.getMetaSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          if (resData.data.tagsList.length > 0) {
            $scope.allTags = resData.data.tagsList;

            //console.log('all tags only '+ JSON.stringify($scope.allTags))
            

            angular.forEach($scope.tagsList, function(value, key){
              $scope.tagsListFinal = value;
              console.log(JSON.stringify($scope.tagsListFinal));
            });
          }
          else{
            resetAlert();
            alertify.alert('Tags not found.')
          }  
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }
 // $.unblockUI();
} 

 $scope.getMeta = function() {
   //  $.blockUI({ message: '<h4> Loading Meta...</h4>' }); 
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.getMetaData(reqData,$scope.getMetaSuccess);
  }
  $scope.getMeta();

/*  Add Tags  */  
$scope.addTagSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   closeModal();
            resetAlert();
            alertify.alert('Tag added successfully.');
            $scope.getMeta();
        }
        else
        {   closeModal();
            resetAlert();
            alertify.alert('Tag insertion failed.');
            $state.go('tags',{'reload':true});
        }  
        $.unblockUI();
    }
$scope.addTag = function() {
$.blockUI({ message: '<h4> Add Tag...</h4>' });
        var reqData = {}
        reqData.tagTitle = $scope.register.tagTitle.replace(/^\s+/, '').replace(/\s+$/, '');
        reqData.tagDescription = $scope.register.tagDescription.replace(/^\s+/, '').replace(/\s+$/, '');
       // console.log('reg data' + JSON.stringify(reqData) );

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');

      userService.addNewTag(reqData, $scope.addTagSuccess)
    }


/*  UPDATE TAGS  */  
$scope.updateTagSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {
            resetAlert();
            alertify.alert('Tag updated successfully.');
            //$scope.getMeta();
            //$state.go('tags',{'reload':true});
            $state.reload();
        }
        else
        {
            resetAlert();
            alertify.alert('Tag updation failed.');
            //$state.go('tags',{'reload':true});
            $state.reload();
        } 
        $.unblockUI(); 
    }
$scope.updateTag = function(tag) {
  $.blockUI({ message: '<h4> Updating Tag </h4>' });

        var reqData = {}
        reqData = tag;
        console.log('reg data for update' + JSON.stringify(reqData) );
        console.log(reqData.tagTitle.length + 'lala ' + reqData.tagDescription.length)

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');

      if(reqData.tagTitle.length > 0 && reqData.tagDescription.length > 0)
      {
        userService.updateTagInDB(reqData, $scope.updateTagSuccess)
        //resetAlert();
        //alertify.alert('lalalall.');
      }
      else
      {
        //$('#updateTagBtn').attr('disabled','disabled');
        $.unblockUI(); 
        resetAlert();
        alertify.alert('You cannot keep the fields empty.');
        $state.reload();
      }
    }

/*   DELETE TAGS   */    
$scope.deleteTagSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {
            resetAlert();
            alertify.alert('Tag deleted successfully.');
            $scope.getMeta();
        }
        else
        {
            resetAlert();
            alertify.alert('Tag deletion failed. Please refresh the page and try again.');
            $state.reload();
        } 
        $.unblockUI(); 
    }
$scope.deleteTag = function(id) {
        var reqData = {}
        reqData.id = id;
        console.log('reg data' + JSON.stringify(reqData) );
        resetConfirm();
        alertify.confirm("Do you want to delete the tag?", function (e) {
            if (e) {
              $.blockUI({ message: '<h4> Deleting Tag...</h4>' });

               $http.defaults.headers.common = { 'Authorization' : undefined};      
               $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
                userService.deleteTagFromDB(reqData, $scope.deleteTagSuccess);
            }
            else{
                alertify.error('Cancelled.');
            }
    });
}

//Import CSV
$scope.uploadedFile = function(element) {
 $scope.$apply(function($scope) {
   $scope.files = element.files;         
 });
}
$scope.addFile = function() {
            var fd = new FormData();
            var files = $scope.files
            angular.forEach(files,function(file)
            {
                 fd.append('file',file);
            });
            $http.defaults.headers.common = { 'Authorization' : undefined};      
            $http.defaults.headers.common = { 'Content-Type' : undefined};      
            $http.defaults.headers.post = {'Content-Type' : undefined};
            $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');

            userService.importTagsToDB(fd,function( resData ) // success
            {
                  if(resData.status == "SUCCESS")
                    {
                         closeModal();
                         $state.reload();
                        if($rootScope.storageType == 'localStorage') 
                        {
                            localStorageService.set('userData',$rootScope.user);
                            localStorageService.set('authToken', $rootScope.user.authstring);
                        }
                         
                          resetAlert();
                          alertify.alert('Your file has been saved in the database.')
                    }
     
                    else  
                    {
                      if(resData.msg == 'UNAUTHORIZED_ACCESS'){
                        delete $rootScope.user;  
                        localStorageService.clearAll();
                        resetAlert();
                        alertify.alert(resData.msg);
                                
                        $state.go('login', {},{'reload':true}); 
                    }
                    else{
                          $state.reload();
                          resetAlert();
                          alertify.alert('Operation failed. Please refresh the page and try again.')
                    }
                }
            });
}

//Import CSV ENDS HERE




//to check input filed for empty while edit tag

$scope.checkEmpty = function()
{
  $scope.t1 = $('#titleUpdate').length;
  console.log('mau '+$scope.t1);

}

// to set modal to pristine after clicking close
$scope.resetModal = function()
{
  $scope.addTagsForm.$setUntouched(); // to make the form untouched
  $scope.addTagsForm.$setPristine();  // to make the form pristine
  $scope.register = '';
}

        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
