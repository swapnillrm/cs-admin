var app = angular.module('pinCompanyDetailsCtrl.controllers', []);

app.controller('detailsCtrl', ['$scope','userService', '$state','$rootScope', '$stateParams','localStorageService','$http',
	function($scope,userService, $state, $rootScope, $stateParams, localStorageService,$http) {
  



  //console.log("stateParams--->"+JSON.stringify($stateParams));



 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }

  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $rootScope.activeFlow = {};
  $scope.ratings = {};
  $scope.company = {};
  $scope.adminData = {};
  $scope.domainData = {};
  $scope.params = {};
  $scope.original_lic_set = {};
  $scope.licSetToken;




 
  $scope.comDetails_original = localStorageService.get('companyData');
  $scope.comDetails_copy = localStorageService.get('companyData');

  $scope.comID_original = localStorageService.get('companyId')
  $scope.comID_copy = localStorageService.get('companyId')
  

    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

	panelHeight()            //panel window height
  scrollPageToTop();       //scrolling page to top 
  //removeModalData();       //remove data 
  showSideBar();           //show menu bar
  useTags();
  useDatePicker();         //use datepicker
  
  /*   Get objectives   */  


$scope.getCompDetailsSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }


          if (resData.data.length > 0) {

           $scope.companyDetailsArray = resData.data;
           console.log('all details '+JSON.stringify($scope.companyDetailsArray))

          }
          else{
            resetAlert();
            alertify.alert('Company details not found.')
          }  
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }

  $.unblockUI();
} 

 $scope.getCompDetails = function(companyId) {
     // $.blockUI({ message: '<h4> Loading...</h4>' });
      
      var reqData = {};
      reqData.id = companyId;
      console.log('getting details for id' + JSON.stringify(localStorageService.get('companyData')))

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      //userService.getMetaData(reqData,$scope.getCompDetailsSuccess);
  }
  //$scope.getCompDetails(companyId);

/*****************    get details by id   ***********************/

$scope.getCompByIDSuccess = function(resData) {
    console.log('we are')
     $.unblockUI();
     closeModal()
      if(resData.status == "SUCCESS")
      {

          $.unblockUI();
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }


          if (resData.data) {
          console.log('here');
          $scope.companyDetailsArray = resData.data;
          localStorageService.set('companyData', resData.data);
          $state.reload()
          }
          else{
            resetAlert();
            alertify.alert('Company details not found.')
          }  
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }  
} 

 $scope.getCompByID = function(companyId) {
      $.blockUI({ message: '<h4> Loading company details...</h4>' });
      
      var reqData = {};
      reqData.com_id = companyId;
      console.log('getting details for id' + JSON.stringify(reqData))

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';

      userService.getCompanyDetailsById(reqData, $scope.getCompByIDSuccess);
}




/**********************  GET DOMAINS  ****************************/

/*  Add Tags  */ 
var domainToEdit = []; 
$scope.myDom;
$scope.getAllDomainsSuccess = function(resData) {

    
        if(resData.status == "SUCCESS")
        {   
          $scope.domainData = resData.data;

          angular.forEach($scope.domainData, function(value, key){
            domainToEdit.push(value.com_login_domain);
            return domainToEdit;

          });

          $scope.myDom = angular.copy(domainToEdit)

          console.log('domains to edit '+ $scope.myDom)
           
        }
        else
        { 
            resetAlert();
            alertify.alert('Domains not found.');            
        }  
       
}



$scope.getAllDomains = function() {
      
      var reqData = {}
      reqData.com_id = (localStorageService.get('companyData').com_id);
      
      
      console.log('reg data' + JSON.stringify(reqData));

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');

      userService.getCompanyDomanis(reqData, $scope.getAllDomainsSuccess)
}


/***************************** Activate/ Deactivate flow *******************************/


$scope.deactivateCompanyInCS = function(params){
  var reqData ={}

  reqData.com_id = $scope.comID_original;
  reqData.license_key = params.license_key;
  reqData.activation_key = params.activation_key;
  reqData.token = params.token;
  reqData.doa = params.doa;
  reqData.dod = params.dod;

  console.log(JSON.stringify(reqData))

       
          

        $http.defaults.headers.common = { 'Authorization' : undefined};      
        $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
        $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              
        userService.deactivateCompany(reqData, function(resData)
          {
            if(resData.status=="SUCCESS")
              {
                var comp_id = (localStorageService.get('companyData').com_id) ;
                $scope.getCompByID(comp_id);

                resetAlert();
                alertify.alert('Company deactivated.')
                $state.reload();
              }
                
              else{
                $.unblockUI();                  
                resetAlert();
                alertify.alert('Could not deactivate. Please try again.')
              }
            })             
              
}



$scope.activateCompanyInCS = function(params){
  var reqData ={}
  reqData.com_id = $scope.comID_original;
  reqData.license_key = params.license_key;
  reqData.activation_key = params.activation_key;
  reqData.token = params.token;
  reqData.doa = params.doa;
  reqData.dod = params.dod;

  console.log(JSON.stringify(reqData));

     
              $.blockUI({ message: '<h4>Activating User...</h4>' });

              $http.defaults.headers.common = {'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              
              userService.activateCompany(reqData, function(resData)
              {
                if(resData.status=="SUCCESS")
                {
                  //localStorageService.set('companyData',resData.data);
                  var comp_id = (localStorageService.get('companyData').com_id) ;
                  $scope.getCompByID(comp_id);

                  $.unblockUI();
                  resetAlert();
                  alertify.alert('Company activated.')
                  $state.reload();
                }
                else{
                  $.unblockUI();                  
                  resetAlert();
                  alertify.alert('Could not activate. Please try again.')
                }
              });
              //alertify.success('Deactivated this company')
}
  




$scope.activateCompanyBaseSuccess = function(resData){
  
    if(resData.status == 'SUCCESS')
    {    
        console.log('now we will activate in cs')
        $scope.params.license_key = resData.data.license_key;
        $scope.params.activation_key = resData.data.activation_key;
        $scope.params.token = resData.data.token;
        $scope.params.doa = resData.data.doa;
        $scope.params.dod = resData.data.dod;

        console.log('params '+ JSON.stringify($scope.params))

        $scope.activateCompanyInCS($scope.params);         
    }

    else 
    {
        console.log('now we will deactivate in cs')


      /*if(resData.status == 'FAILURE')
      {*/
        $scope.params.license_key = resData.data.license_key;
        $scope.params.activation_key = resData.data.activation_key;
        $scope.params.token = resData.data.token;
        $scope.params.doa = resData.data.doa;
        $scope.params.dod = resData.data.dod;
        console.log('params '+ JSON.stringify($scope.params))

        $scope.deactivateCompanyInCS($scope.params);
     // }
      /*else
      {
          $state.reload();
          resetAlert();
          alertify.alert('Could not activate company. Please try again.');
      }*/

    } 

}

$scope.activateCompanyBase = function(){
    console.log('we are in activate')

      var baseUrl = $scope.comDetails_original.com_baseurl;

      var reqData = {};
      reqData.license_key = $rootScope.activeFlow.license_key;
      reqData.activation_key = $rootScope.activeFlow.activation_key;
      reqData.token = $rootScope.activeFlow.token;      
      reqData.doa = $rootScope.activeFlow.doa;      
      reqData.dod = $rootScope.activeFlow.dod;      

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      
      console.log('reg data for update' + JSON.stringify(reqData) + baseUrl); 


        if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){
          console.log('we have the session ' + localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH'))
          userService.activateInBase(reqData, $scope.activateCompanyBaseSuccess, baseUrl);
         /*$scope.activateCompanyInCS($scope.params);*/
        }
        else{
          $scope.login();
        }       
      
}


$scope.deactivateCompanyBase = function() {

  console.log('we are in deactivate')

  resetConfirm();
  alertify.confirm("Do you want to deactivate this company?", function (e) {
    if (e) 
    {
      var baseUrl = $scope.comDetails_original.com_baseurl;

      var reqData = {};
      reqData.license_key = 'someRandomLicKeyIsSentForDeactivation';
      reqData.activation_key = 'Some Activation Key';
      reqData.token = 'Some Token';      
      reqData.doa = '2016-09-19 11:57:36';      
      reqData.dod = '2016-09-21 11:57:36';      

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      
      console.log('req data for update' + JSON.stringify(reqData)); 

       if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){
          console.log('we have the session ' + localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH'))
          userService.activateInBase(reqData, $scope.activateCompanyBaseSuccess, baseUrl);
        
        }
        else{
          $scope.login();
        }
    }

    else
    {
      alertify.error('Cancelled.');
    }
  });       
}



/*  UPDATE COMPANY  */  
/*
$scope.updateCompanySuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
            $state.reload();
            resetAlert();
            alertify.alert('Company domains updated successfully.');

        }
        else
        {
            $state.reload();
            resetAlert();
            alertify.alert('Update of company domains failed.');
        }          
  }

$scope.updateCompanyDomain = function(id,myDom) {
     
      
      var reqData = {};
      reqData.com_id = id;
      reqData.com_domains_list = myDom

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      
      console.log('reg data for update' + JSON.stringify(reqData));        
      userService.updateCompanyDomainInDB(reqData, $scope.updateCompanySuccess)
     
}*/


$scope.updateCompanySuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
            $.unblockUI();
            var comp_id = (localStorageService.get('companyData').com_id) ;
            $scope.getCompByID(comp_id);

            resetAlert();
            alertify.alert('Company details updated successfully.');

        }
        else
        {
            $state.reload();
            resetAlert();
            alertify.alert('Update of company details failed.');
        }          
  }

$scope.updateCompany = function(comDetails_copy) {
      $.blockUI({ message: '<h4> Update company details...</h4>' });
      
      var reqData1 = {};
      reqData1.activation_key = comDetails_copy.activation_key;
      reqData1.alt_email = comDetails_copy.alt_email;
      reqData1.com_baseurl = comDetails_copy.com_baseurl;
      reqData1.com_domains_list = $('#domains').val().toString();

      reqData1.com_id = comDetails_copy.com_id;
      reqData1.com_logo = comDetails_copy.com_logo;
      reqData1.com_name = comDetails_copy.com_name;
      reqData1.com_website = comDetails_copy.com_website;

      reqData1.contact_person = comDetails_copy.contact_person;
      reqData1.email = comDetails_copy.email;
      reqData1.license_key = comDetails_copy.license_key;
      reqData1.location = comDetails_copy.location;
      reqData1.phone = comDetails_copy.phone;
      reqData1.status = comDetails_copy.status;
      reqData1.token = comDetails_copy.token;

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      
      console.log('reg data for update' + JSON.stringify(reqData1));        
      userService.updateCompanyInDB(reqData1, $scope.updateCompanySuccess)
     
}



 /*  DEACTIVATE ADMIN  */ 

$scope.deactivateAdmin = function(id){
  var reqData ={}
  reqData.id = id;
  console.log(JSON.stringify(reqData))

      resetConfirm();
      alertify.confirm("Do you want to deactivate this admin?", function (e) {
            if (e) 
            {
              $.blockUI({ message: '<h4>Deactivating User...</h4>' });
              var baseUrl = $scope.comDetails_original.com_baseurl;

              $http.defaults.headers.common = { 'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              
              userService.deactivateUser(reqData,baseUrl, function(resData)
              {
                $.unblockUI();
                if(resData.status=="SUCCESS")
                {
                  
                  $state.reload();
                  resetAlert();
                  alertify.alert('Admin deactivated.')
                  
                }
                else{
                 
                  resetAlert();
                  alertify.alert('Could not deactivate. Please try again.')
                }
              })
              alertify.success('Deactivated this admin')
            }
            else{
                alertify.error('Cancelled');
            }
        });

          
}



/*  ACTIVATE ADMIN  */   

$scope.activateAdmin = function(id){
  var reqData ={}
  reqData.id = id;

      resetConfirm();
      alertify.confirm("Do you want to activate this admin?", function (e) {
            if (e) 
            {
              $.blockUI({ message: '<h4>Activating User...</h4>' });
              var baseUrl = $scope.comDetails_original.com_baseurl;

              $http.defaults.headers.common = {'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              
              userService.activateUser(reqData, baseUrl, function(resData)
              {
                $.unblockUI();
                if(resData.status=="SUCCESS")
                {
                  
                  resetAlert();
                  alertify.alert('Account has been activated.')
                  $state.reload();
                }
                else{
                  resetAlert();
                  alertify.alert('Could not activate. Please try again.')
                }
              })
              alertify.success('Admin activated');
            }
            else{
                alertify.error('Cancelled');
            }
        });
}





// to set modal to pristine after clicking close
$scope.resetModal = function()
{
  $scope.editCompanyForm.$setUntouched(); // to make the form untouched
  $scope.editCompanyForm.$setPristine();  // to make the form pristine
  $scope.company = '';
}

$scope.resetModal1 = function()
{
  removeModalData();
  $scope.addAdminForm.$setUntouched(); // to make the form untouched
  $scope.addAdminForm.$setPristine();  // to make the form pristine
  $scope.register = '';
}



/************************* ADDING HR ADMIN IN PIN INSTANCE******************/

/*  Add Tags  */  
$scope.addAdminSuccess = function(resData) {

     $.unblockUI();

        if(resData.status == "SUCCESS")
        {   
          var comp_id = (localStorageService.get('companyData').com_id) ;
          $scope.getCompByID(comp_id)

            resetAlert();
            alertify.alert('Admin added successfully.');
           
        }
        else
        { 
            closeModal();
            $state.reload();
            resetAlert();
            alertify.alert('Admin insertion failed.');            
        }  
       
}



$scope.addAdmin = function() {
      $.blockUI({ message: '<h4> Add Admin...</h4>' });
      var reqData = {}
      var baseUrl = $scope.comDetails_original.com_baseurl;
      reqData.username = $scope.register.email;
      reqData.firstname = $scope.register.firstname;
      reqData.lastname = $scope.register.lastname;
      reqData.email = $scope.register.email
      console.log('reg data' + JSON.stringify(reqData) );

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');

      userService.addHRInPin(reqData, $scope.addAdminSuccess, baseUrl)
}



/*******************************************/


$scope.getAdminSuccess = function(resData) {

     $.unblockUI();

        if(resData.status == "SUCCESS")
        {   

            if(resData.data){

              $scope.adminData = resData.data;

            }
            else{
            
            alertify.error('Admin details not found.');
            } 
        }
        else
        { 
         
            alertify.error('Admin details not found. Please reload to try again.');            
        }  
       
}

$scope.getAdmin = function() {
      $.blockUI({ message: '<h4> Loading details...</h4>' });
      var reqData = {}
      var baseUrl = $scope.comDetails_original.com_baseurl;
      

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');

      userService.getAdminsByCompany(reqData, $scope.getAdminSuccess, baseUrl)
}



//login to baseurl instance

$scope.LoginUserSuccess = function(resData) {
      $.unblockUI();
      if (resData.status = 'SUCCESS' && resData.data != null) 
      {
        $rootScope.authTokenForPin = resData.data.authstring;
        if($rootScope.storageType == 'localStorage') 
          {
            localStorageService.set('userData1',$rootScope.user);
            localStorageService.set($scope.comDetails_original.com_baseurl+'-AUTH',$rootScope.authTokenForPin);
            localStorageService.set('authTokenForPin', $rootScope.authTokenForPin);
          }

          //console.log('localStorage data '+ JSON.stringify(localStorageService.get('http://10.10.10.159:8080/pin-india-service-0.1userData1')))
          console.log('authTokenForPin '+ JSON.stringify(localStorageService.get('authTokenForPin')))
             
          $scope.getAdmin();  //calling the get admin api
          $scope.activateCompanyBase();
          $scope.getToken();
          
      } 
      else {
            if (resData.status = 'FAILURE')
            {
              resetAlert();
              alertify.alert(resData.msg);
              $state.go('login',{});
              
            }
          else{
                $state.reload()
                resetAlert();
                alertify.alert('Try again.');
              } 
            }   
  }
  
   
 

     $scope.login = function() {

        $.blockUI({ message: '<h4> Loading details...</h4>',timeout: 2500});

        var reqData= {}
        reqData.userName = 'superadmin@leftrightmind.com';
        reqData.passWord = 'superadmin' ;
        var baseUrl = $scope.comDetails_original.com_baseurl;

        console.log('reqData '+JSON.stringify(reqData))
        $http.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
        $http.defaults.headers.post['Accept'] = 'application/json,text/javascript';

    
        $http.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
       
        $http.defaults.headers.common['Authorization'] = 'Basic ' +btoa(reqData.userName + ':' + reqData.passWord);
        $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';

        if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){
          console.log('we have the session ' + localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH'))
          $scope.getAdmin();
        }
        else{
          userService.loginToBase({}, $scope.LoginUserSuccess, baseUrl);
        }
        
        
    }

/****************************** Forced logout *****************************************/

    $scope.logOutFromBaseSuccess = function(resData) {

      if(resData.status == 'SUCCESS')
      {
        $.unblockUI();
        resetAlert();
        alertify.alert('You have now logged out of the Company Instance');   
            
      }

      else
      {
        $.unblockUI();
        resetAlert();
        alertify.alert('Operation failed. '+resData.msg);
      
      }
    }

    $scope.logOutFromBase = function() {
      var reqData = {};

      reqData.authstring = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      var baseUrl = $scope.comDetails_original.com_baseurl;  

      $.blockUI({ message: '<h4> Logging out of Company Instance...</h4>'});
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';

      if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){
        userService.forceLogout(reqData, $scope.logOutFromBaseSuccess, baseUrl)
      }
      else{
        $.unblockUI();
        resetAlert();
        alertify.alert('You were not logged in.')
      }
      

      //$state.go('login', {}, {reload:true}); 
    }

/********************** Update LIC set in CS ***************************/  

$scope.updateLicSetSuccess = function(resData){
  if (resData.status == 'SUCCESS')
  {
    $scope.getCompanyDetailsById();
  }
  else
  {
    resetAlert();
    alertify.alert('Mission not successful') 
  }  
}


$scope.updateLicSet = function(lic_Set){
   var reqData = {}
   reqData = lic_Set;

   $http.defaults.headers.common = {'Authorization' : undefined};      
   $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
   $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';

   userService.updateLicInDB(reqData, $scope.updateLicSetSuccess)
}  

/*************************  Verify and Copy ***************************/

$scope.verifyAndCopySuccess = function(resData){
  
    if(resData.status == 'SUCCESS'){

      /*if(resData.data){
        $scope.original_lic_set = resData.data;
        $scope.updateLicSet($scope.original_lic_set)
      }*/
      resetAlert();
      alertify.alert('Verified and copied')   
    }

    else 
    {
      resetAlert();
      alertify.alert('Mission not successful.')

    } 

}

$scope.verifyAndCopy = function(){
      var baseUrl = $scope.comDetails_original.com_baseurl;
      var reqData = {};
      /*  reqData.license_key = $scope.comDetails_original.license_key;
      reqData.activation_key = $scope.comDetails_original.activation_key;
      reqData.token = $scope.comDetails_original.token;      
      reqData.doa = $scope.comDetails_original.doa;      
      reqData.dod = $scope.comDetails_original.dod;  */    

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      
      console.log('reg data for update' + JSON.stringify(reqData) + baseUrl); 


        if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){
          console.log('we have the session ' + localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH'))
          userService.verifyLicense(reqData, $scope.verifyAndCopySuccess, baseUrl);
        }
        else{
          $scope.login();
        }       
      
}

/*************************  Verify company  ***************************/


$scope.verifySuccess = function(resData){
  
    if(resData.status == 'SUCCESS'){
      resetAlert();
      alertify.alert('Verified')   
    }

    else 
    {
      resetAlert();
      alertify.alert('Not verified')

    } 

}

$scope.verify = function(){
    console.log('we are in activate')

      var baseUrl = $scope.comDetails_original.com_baseurl;

      var reqData = {};
    /*  reqData.license_key = $scope.comDetails_original.license_key;
      reqData.activation_key = $scope.comDetails_original.activation_key;
      reqData.token = $scope.comDetails_original.token;      
      reqData.doa = $scope.comDetails_original.doa;      
      reqData.dod = $scope.comDetails_original.dod;  */    

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      
      console.log('reg data for update' + JSON.stringify(reqData) + baseUrl); 


        if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){
          console.log('we have the session ' + localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH'))
          userService.verifyLicense(reqData, $scope.verifySuccess, baseUrl);
        }
        else{
          $scope.login();
        }       
      
}


$scope.getUsercountSuccess = function(resData)
{
   if(resData.status == "SUCCESS")
      {
        localStorageService.set('activeCount',resData.data.activeUserCount);
        localStorageService.set('deactiveCount',resData.data.deactiveUserCount);
        localStorageService.set('total',resData.data.totalUserCount);

        $rootScope.activeCount = localStorageService.get('activeCount');
        $rootScope.deactiveCount = localStorageService.get('deactiveCount');
        $rootScope.Total = localStorageService.get('total');      
        
        
      }
      else
      {
        $scope.activeCount = 0;
        $scope.deactiveCount = 0;
        $scope.Total = 0;
      }
}

$scope.getCount = function()
{      var baseUrl = $scope.comDetails_original.com_baseurl;
      var reqData = {};
     
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';

      if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){         
          userService.getUsersCount(reqData,$scope.getUsercountSuccess , baseUrl);
        }
      else{
          $scope.login();
        }  
    

}
/**********************************************************************/


/********************** Get Token From DB ***********************/


$scope.getTokenSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
          $rootScope.activeFlow.token = resData.token;

        }
        else
        {          
          resetAlert();
          alertify.alert('Token not found.');
        }          
  }

$scope.getToken = function() {     
      
      var reqData = {};
      var baseUrl = $scope.comDetails_original.com_baseurl;

     // $rootScope.activeFlow.token = 'titDohkitanobseeBniginnysve';

      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      
       if(localStorageService.get($scope.comDetails_original.com_baseurl+'-AUTH')){         
          userService.getTokenFromDB(reqData, $scope.getTokenSuccess,baseUrl);
        }
      /*else{
          $scope.login();
        }*/       
      
     
}
$scope.getToken();


/********************** Get Token From DB ***********************/



        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
app.filter('orderObjectBy', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }

    array.sort(function(a, b){
        a = parseInt(a[attribute]);
        b = parseInt(b[attribute]);
        return a - b;
    });
    return array;
 }
});