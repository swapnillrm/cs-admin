-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2016 at 06:48 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `springbootapp1`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `log_type` enum('LOG_ERROR','LOG_WARN','LOG_NOTICE','LOG_TRANS','','LOG_EMAIL') NOT NULL,
  `log_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_message` longtext NOT NULL,
  `log_trace` longtext NOT NULL,
  `log_status_code` varchar(25) NOT NULL,
  `user_id` int(11) NOT NULL,
  `log_source` varchar(20) DEFAULT NULL,
  `log_remote_ip` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'This is the user to be notified. This user gets the notification if the status is unread',
  `msg_type` varchar(25) NOT NULL COMMENT 'EMAIL , PIN SAVE, RELATION CHANGE',
  `msg_activity` varchar(25) NOT NULL,
  `msg_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date at which message is created',
  `msg_from` int(11) NOT NULL COMMENT 'is the user who is the sender. But the system will send with its own email or sender id. ',
  `msg_subject` varchar(50) NOT NULL COMMENT 'email subject or msg subject',
  `msg_body` mediumtext COMMENT 'body of the message',
  `msg_read_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'read or unread 0 = false = unread'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `msg_type`, `msg_activity`, `msg_timestamp`, `msg_from`, `msg_subject`, `msg_body`, `msg_read_status`) VALUES
(1, 28, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 06:44:58', 28, 'You have received a new Pin', 'Freddie has given a pin to your DR Michael', 1),
(2, 28, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 08:30:01', 28, 'You have received a new Pin', 'Freddie has given a pin to your DR William', 1),
(3, 28, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 08:33:11', 28, 'You have received a new Pin', 'Freddie has given a pin to your DR William', 0),
(4, 32, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 08:41:49', 32, 'You have received a new Pin', 'William has given a pin to your DR Marcel', 0),
(5, 28, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 08:42:19', 28, 'You have received a new Pin', 'Freddie has given a pin to your DR Saurabh', 0),
(6, 3, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 08:55:56', 3, 'You have received a new Pin', 'Drew has given a pin to your DR José Carlos', 0),
(7, 28, 'NOTIFICATION', 'ADD_PIN', '2016-07-01 09:40:30', 3, 'You have received a new Pin', 'Drew has given a pin to your DR Saurabh', 0),
(8, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:18:06', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(9, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:32:29', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(10, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:33:38', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(11, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:35:03', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(12, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:36:16', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(13, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:38:24', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(14, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:39:23', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0),
(15, 32, 'EMAIL', 'ADD_PIN', '2016-07-03 16:40:57', 21, 'You have received a new Pin', '<!doctype html>\n<html>\n<head>\n<meta name="viewport" content="width=device-width">\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n<title>You have received a pin</title>\n<style>\n/* -------------------------------------\n    GLOBAL\n------------------------------------- */\n* {\n  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;\n  font-size: 100%;\n  line-height: 1.6em;\n  margin: 0;\n  padding: 0;\n}\n\nimg {\n  max-width: 600px;\n  width: 100%;\n}\n\nbody {\n  -webkit-font-smoothing: antialiased;\n  height: 100%;\n  -webkit-text-size-adjust: none;\n  width: 100% !important;\n}\n\n\n/* -------------------------------------\n    ELEMENTS\n------------------------------------- */\na {\n  color: #348eda;\n}\n\n.btn-primary {\n  Margin-bottom: 10px;\n  width: auto !important;\n}\n\n.btn-primary td {\n  background-color: #348eda; \n  border-radius: 25px;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; \n  font-size: 14px; \n  text-align: center;\n  vertical-align: top; \n}\n\n.btn-primary td a {\n  background-color: #348eda;\n  border: solid 1px #348eda;\n  border-radius: 25px;\n  border-width: 10px 20px;\n  display: inline-block;\n  color: #ffffff;\n  cursor: pointer;\n  font-weight: bold;\n  line-height: 2;\n  text-decoration: none;\n}\n\n.last {\n  margin-bottom: 0;\n}\n\n.first {\n  margin-top: 0;\n}\n\n.padding {\n  padding: 10px 0;\n}\n\n\n/* -------------------------------------\n    BODY\n------------------------------------- */\ntable.body-wrap {\n  padding: 20px;\n  width: 100%;\n}\n\ntable.body-wrap .container {\n  border: 1px solid #f0f0f0;\n}\n\n\n/* -------------------------------------\n    FOOTER\n------------------------------------- */\ntable.footer-wrap {\n  clear: both !important;\n  width: 100%;  \n}\n\n.footer-wrap .container p {\n  color: #666666;\n  font-size: 12px;\n  \n}\n\ntable.footer-wrap a {\n  color: #999999;\n}\n\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1, \nh2, \nh3 {\n  color: #111111;\n  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n  font-weight: 200;\n  line-height: 1.2em;\n  margin: 40px 0 10px;\n}\n\nh1 {\n  font-size: 36px;\n}\nh2 {\n  font-size: 28px;\n}\nh3 {\n  font-size: 22px;\n}\n\np, \nul, \nol {\n  font-size: 14px;\n  font-weight: normal;\n  margin-bottom: 10px;\n}\n\nul li, \nol li {\n  margin-left: 5px;\n  list-style-position: inside;\n}\n\n/* ---------------------------------------------------\n    RESPONSIVENESS\n------------------------------------------------------ */\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n  clear: both !important;\n  display: block !important;\n  Margin: 0 auto !important;\n  max-width: 600px !important;\n}\n\n/* Set the padding on the td rather than the div for Outlook compatibility */\n.body-wrap .container {\n  padding: 20px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n  display: block;\n  margin: 0 auto;\n  max-width: 600px;\n}\n\n/* Lets make sure tables in the content area are 100% wide */\n.content table {\n  width: 100%;\n}\n\n</style>\n</head>\n\n<body bgcolor="#f6f6f6">\n\n<!-- body -->\n<table class="body-wrap" bgcolor="#f6f6f6" style="width:100%;padding:5%;">\n  <tr>\n    <td></td>\n    <td class="container" bgcolor="#FFFFFF">\n\n      <!-- content -->\n      <div class="content">\n      <table>\n        <tr>\n          <td>\n            <p>Hi William,</p>\n            <p></p>\n            <h1>You have received a new PIN</h1>\n            <p>Marcel has Pinned you!</p>\n            \n             <p>Marcel has pinned you as their MGR. They are now your DR.</p>\n\n            <!-- button -->\n            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">\n              <tr>\n                <td style="height:40px;background-color:#14aae2;color:#ffffff;">\n                  <a href="#">Click here to view this</a>\n                </td>\n              </tr>\n            </table>\n            <!-- /button -->\n           \n          </td>\n        </tr>\n      </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /body -->\n\n<!-- footer -->\n<table class="footer-wrap">\n  <tr>\n    <td></td>\n    <td class="container">\n      \n      <!-- content -->\n      <div class="content">\n        <table>\n          <tr>\n            <td align="center">\n              <p>You are getting these notifications because the PIN administrator has made sure you are informed when you receive a pin.? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.\n              </p>\n            </td>\n          </tr>\n        </table>\n      </div>\n      <!-- /content -->\n      \n    </td>\n    <td></td>\n  </tr>\n</table>\n<!-- /footer -->\n\n</body>\n</html>\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `objectives`
--

CREATE TABLE `objectives` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objectives`
--

INSERT INTO `objectives` (`id`, `title`, `description`, `created_date`, `is_active`) VALUES
(1, 'Self Development', 'Making sure there is learnability', '2016-06-02 10:36:36', 1),
(2, 'Customer Focus', 'Making sure customer demands are met.', '2016-06-02 10:37:01', 1),
(3, 'Time Management', 'Deliverables are on time or before expectations.\r\nFocused.', '2016-06-02 10:37:31', 1),
(4, 'Dependability', 'this is objective 4', '2016-06-02 10:37:44', 1),
(5, 'Team work', 'Being a good team player', '2016-06-02 10:37:56', 1),
(6, 'Critical Thinking', 'this is objective 6', '2016-06-02 10:38:08', 1),
(7, 'Leadership', 'this is objective 7', '2016-06-02 10:38:20', 1),
(8, 'Obj1', 'Obj1', '2016-06-02 10:38:31', 1),
(9, 'Objectives 3', 'some pbjectibe', '2016-06-30 10:50:44', 1),
(10, 'My OBJ', 'My OBJ', '2016-06-30 10:54:57', 1),
(11, 'testing skills', 'checking testing effectiviness', '2016-06-30 10:55:57', 1),
(12, 'ret', 'ery', '2016-06-30 15:01:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `objective_tag`
--

CREATE TABLE `objective_tag` (
  `obj_tag_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objective_tag`
--

INSERT INTO `objective_tag` (`obj_tag_id`, `obj_id`, `tag_id`, `created_date`) VALUES
(1, 1, 1, '2016-06-02 10:39:50'),
(2, 2, 1, '2016-06-02 10:40:16'),
(3, 3, 1, '2016-06-02 10:40:27'),
(4, 2, 3, '2016-06-06 13:28:23'),
(5, 4, 2, '2016-06-06 13:52:43'),
(6, 8, 3, '2016-06-22 12:02:38'),
(7, 7, 3, '2016-06-22 12:02:38'),
(8, 6, 3, '2016-06-22 12:03:32'),
(9, 5, 3, '2016-06-22 12:03:32'),
(10, 8, 2, '2016-06-22 12:04:32'),
(11, 1, 5, '2016-06-22 12:04:32'),
(12, 3, 4, '2016-06-22 12:04:32'),
(13, 5, 2, '2016-06-22 12:04:32'),
(14, 9, 2, '2016-06-30 10:50:44'),
(15, 9, 3, '2016-06-30 10:50:44'),
(16, 10, 1, '2016-06-30 10:54:57'),
(17, 11, 1, '2016-06-30 10:55:57'),
(18, 12, 2, '2016-06-30 15:01:08'),
(19, 12, 3, '2016-06-30 15:01:08'),
(20, 12, 4, '2016-06-30 15:01:08');

-- --------------------------------------------------------

--
-- Table structure for table `pins`
--

CREATE TABLE `pins` (
  `id` int(11) NOT NULL,
  `event` varchar(255) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `rating_set` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `pinner` int(11) NOT NULL,
  `pinnee` int(11) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `objective` int(11) NOT NULL,
  `pinnee_relationship` int(11) NOT NULL,
  `pin_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1 COMMENT='This table stores all pins . Most of the fields are relational foreign mappings. However, ensuring sanctity of the data is the responsibility of the application while inserting meta and actual pin data.';

--
-- Dumping data for table `pins`
--

INSERT INTO `pins` (`id`, `event`, `comment`, `rating_set`, `rating`, `pinner`, `pinnee`, `tag`, `objective`, `pinnee_relationship`, `pin_date`, `created_date`, `status`) VALUES
(46, 'event3 ', 'comment3', 1, 0, 28, 3, '5', 1, 4, '2016-06-10 16:48:11', '2016-06-29 18:13:33', 1),
(47, 'event3 ', 'comment3', 1, 0, 5, 32, '5', 1, 4, '2016-06-10 16:48:11', '2016-07-02 18:49:36', 1),
(48, 'event3 ', 'comment3', 1, 0, 5, 32, '5', 1, 4, '2016-06-10 00:00:00', '2016-07-02 18:49:31', 1),
(49, 'event3 ', 'comment3', 1, 0, 5, 32, '5', 1, 4, '2016-06-10 00:00:00', '2016-07-02 18:49:28', 1),
(50, 'event3 ', 'comment3', 1, 0, 5, 3, '5', 1, 4, '2016-06-10 00:00:00', '2016-06-29 18:35:37', 1),
(51, 'event3 ', 'comment3', 1, 0, 5, 3, '5', 1, 4, '2016-06-10 00:00:00', '2016-06-29 18:35:38', 1),
(52, 'event3 ', 'comment3', 1, 0, 5, 32, '5', 1, 4, '2016-06-10 00:00:00', '2016-07-02 18:49:25', 1),
(53, 'do you  ', 'Testing example ', 1, 13, 3, 20, '2,4', 3, 3, '2016-06-22 00:00:00', '2016-06-29 18:37:46', 1),
(54, 'Android Event ', 'good work\n', 1, 0, 28, 21, '3', 2, 2, '2016-06-30 01:04:45', '2016-06-30 13:05:29', 1),
(55, 'ios event ', 'excellent work\n', 1, 0, 28, 21, '3', 5, 2, '2016-06-30 01:06:44', '2016-06-30 13:07:16', 1),
(56, 'texfgbbjjnn nnnvgjjj jkmnjk jnnnkkoifcc ', 'vhjjjjfftyhh hjnnloougder hhhjkk nnhvhsrtuujh\nvjjkbhjkkjkj hkmm', 1, 12, 3, 32, '5,2,1,4,3', 6, 3, '2016-12-22 01:39:37', '2016-07-02 18:28:13', 1),
(57, 'android pin designs ', 'good job on designs', 1, 14, 28, 20, '4', 3, 3, '2016-06-25 02:42:44', '2016-06-30 14:43:22', 1),
(58, 'design phase 2 ', 'need to be more creative and requires a better understanding of user experience.', 1, 12, 28, 20, '4', 3, 3, '2016-06-30 02:45:17', '2016-06-30 14:46:11', 1),
(59, 'between ', 'Aa', 1, 13, 32, 31, '3,1', 1, 3, '2016-06-23 00:00:00', '2016-07-02 17:56:20', 1),
(60, 'cybg ', 'Dvgf', 1, 0, 28, 27, '2', 4, 2, '2016-06-29 00:00:00', '2016-06-30 15:12:14', 1),
(61, 'testing testing testing ', 'hi', 1, 14, 28, 32, '5,1,3', 3, 3, '2016-02-27 03:46:37', '2016-07-02 18:06:20', 1),
(62, 'oooo ', 'Wowow', 1, 0, 32, 28, '5,3', 7, 1, '2016-06-29 00:00:00', '2016-06-30 15:50:01', 1),
(63, 'jdjsjskksks jdjdjdjdnd hdjeje ', 'nskslskvxksks xnkdkdmd', 1, 0, 28, 32, '3', 7, 4, '2016-06-27 03:49:41', '2016-07-02 18:49:43', 1),
(64, 'arrere ', 'coommmeeennnnntttttttt', 1, 14, 28, 32, '1,3,5', 5, 3, '2016-06-29 04:12:00', '2016-06-30 16:12:28', 1),
(65, 'oijygdfj ', 'Rrrrr', 1, 15, 32, 31, '4,5,1', 10, 3, '2016-06-23 00:00:00', '2016-06-30 16:15:03', 1),
(66, 'ndjdkkdkdkfkkfkfkkrkdkjdje jdkdjdndnje ', 'Jdkdkdorod jdkdkdorod kdkdkdjdodkidkdjrodkskpww', 1, 0, 32, 21, '2,4', 4, 4, '2016-06-30 00:00:00', '2016-06-30 16:18:38', 1),
(67, 'rating ', 'New rate', 1, 15, 32, 31, '5,1,4', 3, 3, '2016-06-29 00:00:00', '2016-06-30 17:05:39', 1),
(68, 'rate3 ', 'Cgbv', 1, 13, 32, 31, '1,5,4', 1, 3, '2016-06-23 00:00:00', '2016-06-30 17:08:28', 1),
(69, 'the  ', 'Definitely worth it ', 1, 12, 3, 33, '4,2,3,1,5', 11, 3, '2016-06-30 00:00:00', '2016-06-30 17:09:28', 1),
(70, 'add pin check android ', 'lets check', 1, 0, 28, 3, '3', 9, 4, '2016-06-30 05:13:26', '2016-07-02 17:48:06', 1),
(71, 'igckgxixifxt ', 'if incorrect FCC ohvohvyov ohvohvo oh ovy', 1, 0, 28, 22, '3', 7, 4, '2016-06-29 05:14:19', '2016-07-02 17:47:40', 1),
(72, 'Hshdb ', 'Bend', 1, 11, 32, 29, '5', 1, 3, '2016-06-28 00:00:00', '2016-06-30 17:40:37', 1),
(73, 'Shan ', 'Hssn', 1, 14, 32, 29, '5,2,4', 5, 3, '2016-06-23 00:00:00', '2016-06-30 17:41:12', 1),
(74, 'hdbd ', 'Shah', 1, 0, 32, 29, '2,5', 9, 4, '2016-06-29 00:00:00', '2016-06-30 17:42:00', 1),
(75, 'rating check ', 'check ', 1, 0, 28, 3, '5', 1, 4, '2016-06-25 06:10:36', '2016-06-30 18:10:56', 1),
(76, 'test ', 'test', 1, 0, 6, 3, '1, 3', 3, 2, '2016-07-07 10:07:31', '2016-07-01 10:05:35', 1),
(77, 'gzhsbdbsh havsvzbbz ', 'Gsvsb', 1, 0, 28, 27, '2', 5, 2, '2016-07-01 00:00:00', '2016-07-01 10:45:56', 1),
(78, 'pin web API support ', 'good job on the web front', 1, 0, 39, 40, '4', 3, 2, '2016-07-01 10:52:27', '2016-07-01 10:53:42', 1),
(79, 'web api support ', 'good job on web front', 1, 14, 39, 40, '4', 3, 3, '2016-07-01 10:57:00', '2016-07-01 10:57:35', 1),
(80, 'Ask the  ', 'Regal', 1, 0, 3, 20, '5,1', 3, 1, '2016-07-01 00:00:00', '2016-07-01 11:02:44', 1),
(81, 'changed dr to pin ', 'testing add pin now', 1, 0, 39, 40, '44', 3, 3, '2016-07-01 11:12:00', '2016-07-01 11:14:19', 1),
(82, 'bdjsjsjsjjdjjdjd ', 'nxnjxkxks ndndmdkd ndmkdks\ndnnx', 1, 0, 22, 33, '2', 4, 2, '2016-07-01 11:21:05', '2016-07-01 11:21:38', 1),
(83, 'other to peer ', 'testing', 1, 0, 28, 3, '55', 1, 2, '2016-07-01 11:39:20', '2016-07-01 11:39:44', 1),
(84, 'xs ', 'Assets', 1, 13, 32, 37, '1', 3, 3, '2016-06-24 00:00:00', '2016-07-01 11:45:35', 1),
(85, 'jsjskks kdldlfllfldpdkifjf dnmdkd ', 'bdjdkzlmfkfnkodlmf', 1, 13, 28, 32, '1,3,55,1,3', 6, 3, '2016-04-01 11:49:37', '2016-07-02 18:06:30', 1),
(86, 'hrjdjndjdjdkdjjfjd jdjdndkks McKenna ', 'Jdjdndkks is jdjdndkks difficult', 1, 0, 28, 32, '3,1', 3, 2, '2016-06-24 00:00:00', '2016-07-01 11:52:43', 1),
(87, 'je ', 'js', 1, 0, 28, 27, '22', 5, 3, '2016-07-01 12:14:39', '2016-07-01 12:14:58', 1),
(88, 'adfh ', 'chgf', 1, 0, 28, 22, '333', 5, 2, '2016-07-01 12:27:48', '2016-07-01 12:28:32', 1),
(89, 'solving tagid list issue ', 'i think it us solved now ', 1, 0, 28, 21, '3', 7, 4, '2016-07-01 12:36:47', '2016-07-01 12:37:32', 1),
(90, 'purchase task ', 'Vdh', 1, 0, 32, 35, '5,1', 3, 4, '2016-06-24 00:00:00', '2016-07-01 12:49:38', 1),
(91, 'check pins by manager ', 'asdfghjkl', 1, 13, 28, 32, '2', 8, 3, '2016-07-01 01:59:21', '2016-07-01 14:00:01', 1),
(92, 'pin not showing in view performance ', 'testing', 1, 12, 28, 32, '2', 4, 3, '2015-12-01 02:02:43', '2016-07-02 18:06:44', 1),
(93, 'gjgjg ', 'jgjgjh', 1, 0, 6, 5, '1, 3, 5', 5, 1, '2016-07-16 14:07:19', '2016-07-01 14:06:25', 1),
(94, 'dhdbb ', 'Nice', 1, 14, 32, 21, '1', 11, 3, '2016-06-24 00:00:00', '2016-07-01 14:11:49', 1),
(95, 'android development ', 'good job buddy !', 1, 14, 28, 39, '4', 3, 3, '2016-07-01 02:11:47', '2016-07-01 14:12:19', 1),
(96, 'selling ', 'we had a good meeting', 1, 14, 3, 33, '3', 7, 3, '2016-06-28 14:06:29', '2016-07-01 14:25:56', 1),
(97, 'Feds ', 'Say rang ko boiled ', 1, 0, 3, 39, '1,4', 10, 4, '2016-06-24 00:00:00', '2016-07-01 15:10:30', 1),
(100, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 21:48:05', 1),
(101, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:02:29', 1),
(102, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:03:38', 1),
(103, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:05:03', 1),
(104, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:06:15', 1),
(105, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:08:24', 1),
(106, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:09:23', 1),
(107, 'event1 ', 'comment', 1, 0, 21, 32, '1', 1, 1, '2016-06-07 16:48:11', '2016-07-03 22:10:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `rating_val_id` int(11) NOT NULL,
  `rating_set_id` int(11) NOT NULL COMMENT 'rating set_id will the the id that is used in the configuration',
  `rating_value` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `rating_title` varchar(30) NOT NULL COMMENT 'This is the display title',
  `hex_value` varchar(10) NOT NULL COMMENT 'the color of the rating',
  `asset_on` varchar(255) NOT NULL COMMENT 'URL',
  `asset_off` varchar(255) NOT NULL COMMENT 'URL',
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='This is the table tthat contains all the possible ratings in rating sets';

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`rating_val_id`, `rating_set_id`, `rating_value`, `description`, `rating_title`, `hex_value`, `asset_on`, `asset_off`, `is_active`) VALUES
(0, 1, 0, 'No Rating', 'NO RATING', '#445454', '', '', 1),
(11, 1, 1, 'Poor Performance', 'Poor', '#b34532', '', '', 1),
(12, 1, 2, 'Weak Performance', 'Weak', '#b37432', '', '', 1),
(13, 1, 3, 'Average Performance', 'Average', '#b39d32', '', '', 1),
(14, 1, 4, 'Meets Expectations', 'Good', '#5284b3', '', '', 1),
(15, 1, 5, 'Exceeds Expectations', 'Super', '#44b377', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rating_sets`
--

CREATE TABLE `rating_sets` (
  `rating_set_id` int(11) NOT NULL,
  `rating_set_title` varchar(50) NOT NULL,
  `rating_set_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating_set_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `relationships`
--

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL,
  `rel_role` varchar(25) NOT NULL,
  `description` varchar(75) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='this table is used for defining relationships in the system. Eg DR = Direct Report  , MGR = manager';

--
-- Dumping data for table `relationships`
--

INSERT INTO `relationships` (`id`, `rel_role`, `description`, `created_date`) VALUES
(1, 'MGR', 'Manager', '2016-05-26 04:47:32'),
(2, 'PEER', 'Peer', '2016-05-26 04:47:32'),
(3, 'DR', 'Direct Report', '2016-05-26 04:49:37'),
(4, 'OTHER', 'Other', '2016-05-26 04:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `relation_possibilities`
--

CREATE TABLE `relation_possibilities` (
  `id` int(11) NOT NULL,
  `U1_rel_role` int(11) NOT NULL COMMENT 'U1 is always principle actor, primary actor.',
  `U2_rel_role` int(11) NOT NULL COMMENT 'U2 role is the role that U1 sees U2 AS.U2 is secondary actor. ',
  `is_relation_possible` varchar(12) NOT NULL DEFAULT 'NO' COMMENT 'This gives whether a relationship between U1 and U2 is possible.',
  `is_edit_relation_possible` varchar(12) NOT NULL DEFAULT 'NO' COMMENT 'IF U1 is the role given, can U1 can make U2 as U2 role? Options are YES,NO , MGR_LOGIC, DR_LOGIC',
  `is_rating_possible` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Defines if Rating is Possible',
  `notify_pinner` tinyint(1) NOT NULL DEFAULT '1',
  `notify_pinnee` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COMMENT='This is a configuration table that defines the relation_possibilities. Precondition is that relation roles are predefined and this table is updated.';

--
-- Dumping data for table `relation_possibilities`
--

INSERT INTO `relation_possibilities` (`id`, `U1_rel_role`, `U2_rel_role`, `is_relation_possible`, `is_edit_relation_possible`, `is_rating_possible`, `notify_pinner`, `notify_pinnee`) VALUES
(1, 1, 1, 'NO', 'MGR_LOGIC', 0, 1, 1),
(2, 1, 2, 'NO', 'YES', 0, 1, 1),
(3, 1, 3, 'YES', 'YES', 1, 1, 1),
(4, 1, 4, 'NO', 'YES', 0, 1, 1),
(5, 2, 1, 'NO', 'MGR_LOGIC', 0, 1, 1),
(6, 2, 2, 'YES', 'YES', 0, 1, 1),
(7, 2, 3, 'NO', 'DR_LOGIC', 0, 1, 1),
(8, 2, 4, 'NO', 'YES', 0, 1, 1),
(9, 3, 1, 'YES', 'YES', 0, 1, 1),
(10, 3, 2, 'NO', 'NO', 0, 1, 1),
(11, 3, 3, 'NO', 'NO', 0, 1, 1),
(12, 3, 4, 'NO', 'NO', 0, 1, 1),
(13, 4, 1, 'NO', 'MGR_LOGIC', 0, 1, 1),
(14, 4, 2, 'NO', 'YES', 0, 1, 1),
(15, 4, 3, 'NO', 'DR_LOGIC', 0, 1, 1),
(16, 4, 4, 'YES', 'YES', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` varchar(25) NOT NULL DEFAULT 'ROLE_USER' COMMENT 'Roles as defined in the system, primary key is the role ID',
  `description` varchar(150) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'o= false=not default'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Roles meta table';

-- --------------------------------------------------------

--
-- Table structure for table `spring_session`
--

CREATE TABLE `spring_session` (
  `SESSION_ID` char(36) NOT NULL DEFAULT '',
  `CREATION_TIME` bigint(20) NOT NULL,
  `LAST_ACCESS_TIME` bigint(20) NOT NULL,
  `MAX_INACTIVE_INTERVAL` int(11) NOT NULL,
  `PRINCIPAL_NAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spring_session`
--

INSERT INTO `spring_session` (`SESSION_ID`, `CREATION_TIME`, `LAST_ACCESS_TIME`, `MAX_INACTIVE_INTERVAL`, `PRINCIPAL_NAME`) VALUES
('9d4a794a-4837-4bd3-829e-e6c148a11926', 1467562262406, 1467564057596, 2592000, 'mayursojrani');

-- --------------------------------------------------------

--
-- Table structure for table `spring_session_attributes`
--

CREATE TABLE `spring_session_attributes` (
  `SESSION_ID` char(36) NOT NULL DEFAULT '',
  `ATTRIBUTE_NAME` varchar(200) NOT NULL DEFAULT '',
  `ATTRIBUTE_BYTES` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spring_session_attributes`
--

INSERT INTO `spring_session_attributes` (`SESSION_ID`, `ATTRIBUTE_NAME`, `ATTRIBUTE_BYTES`) VALUES
('9d4a794a-4837-4bd3-829e-e6c148a11926', 'session_user_id', 0xaced0005737200116a6176612e6c616e672e496e746567657212e2a0a4f781873802000149000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b020000787000000006),
('9d4a794a-4837-4bd3-829e-e6c148a11926', 'SPRING_SECURITY_CONTEXT', 0xaced00057372003d6f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e636f6e746578742e5365637572697479436f6e74657874496d706c00000000000001900200014c000e61757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b78707372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001900200024c000b63726564656e7469616c737400124c6a6176612f6c616e672f4f626a6563743b4c00097072696e636970616c71007e0004787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c7371007e0004787001737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00067870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001900200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000a524f4c455f41444d494e7371007e000e740009524f4c455f555345527871007e000d737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001900200024c000d72656d6f74654164647265737371007e000f4c000973657373696f6e496471007e000f787074000f303a303a303a303a303a303a303a317070737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001900200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657374000f4c6a6176612f7574696c2f5365743b4c000870617373776f726471007e000f4c0008757365726e616d6571007e000f787001010101737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e000a737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000190020000787077040000000271007e001071007e0012787074000c6d61797572736f6a72616e69),
('9d4a794a-4837-4bd3-829e-e6c148a11926', 'username', 0xaced000574000c6d61797572736f6a72616e69);

-- --------------------------------------------------------

--
-- Table structure for table `system_configuration`
--

CREATE TABLE `system_configuration` (
  `id` int(11) NOT NULL,
  `config_type` varchar(20) NOT NULL DEFAULT 'GLOBALS' COMMENT 'This  can either be globals,lang,flow',
  `config_param` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `config_value` varchar(9192) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='System Configuration definition';

--
-- Dumping data for table `system_configuration`
--

INSERT INTO `system_configuration` (`id`, `config_type`, `config_param`, `description`, `config_value`, `created_date`) VALUES
(1, 'GLOBALS', 'ALLOW_MULTIPLE_MANAGERS', 'This config key describes if the system allows multiple managers to be desinged as in relationship for employee.', 'NO', '2016-06-13 11:00:08'),
(2, 'GLOBALS', 'NOTIFY_ON_PIN_ALL_PINNEE_MGR', 'This setting describes if A Pins B , B has manager C. C gets notified regardless of relationship between A and B', 'YES', '2016-06-14 09:36:48'),
(3, 'GLOBALS', 'DEFAULT_NOTIFICATION_TYPE', 'This is the default notification type that is used to get or send all notifications to user. This can be either EMAIL or NOTIFICATION', 'NOTIFICATION', '2016-06-18 05:16:11'),
(4, 'GLOBALS', 'EMAIL_ON_PIN_ALL_PINNEE_MGR', 'if A pins B , and B has manager(s) C , this configuration setting denotes if EMAIL should go to pinnee''s manager', 'YES', '2016-06-18 10:31:33'),
(5, 'GLOBALS', 'NOTIFY_ON_PIN_PINNEE', 'if YES , then PINNEE will be notified', 'NO', '2016-06-18 10:31:33'),
(6, 'GLOBALS', 'EMAIL_ON_PIN_PINNEE', 'This will add notification to pinnee on getting pinned', 'YES', '2016-06-18 21:12:49'),
(7, 'GLOBALS', 'GLOBAL_NOTIFICATIONS_ON', 'Is notifications enabled', 'YES', '2016-06-18 21:13:45'),
(8, 'GLOBALS', 'GLOBAL_EMAILS_ON', 'Will turn off Email sending completely', 'YES', '2016-06-18 21:15:12'),
(9, 'GLOBALS', 'ALLOW_MGR_TO_SEE_DR_ALL_PINS', 'This setting allows manager to see all DR pins', 'YES', '2016-06-22 09:48:16'),
(10, 'GLOBALS', 'SKIP_ZERO_RATING_FOR_CHARTS', 'This field will skip all zero ratings for chart calculations', 'YES', '2016-07-02 18:53:03'),
(11, 'GLOBALS', 'SKIP_INACTIVE_USERS_FOR_CHARTS', 'This field will skip all inactive users who were either Pinners or Pinnees for chart calculations', 'NO', '2016-07-02 20:11:39'),
(12, 'GLOBALS', 'SKIP_INACTIVE_USERS_FOR_PINS', 'This field will skip all inactive users who were either Pinners or Pinnees for gettings PINs', 'NO', '2016-07-02 20:11:39');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` varchar(150) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Tags_meta';

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `description`, `created_date`, `is_active`) VALUES
(1, 'Sales', 'This Is IT', '2016-06-01 09:23:32', 1),
(2, 'Marketing', 'this is tag 2', '2016-06-01 09:24:51', 1),
(3, 'Customer Orientation', 'this is tag 3', '2016-06-01 09:25:04', 1),
(4, 'Development', 'this is tag 4', '2016-06-01 09:25:17', 1),
(5, 'Purchase', 'this is tag 5', '2016-06-01 09:25:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` smallint(10) unsigned NOT NULL COMMENT 'uid',
  `username` varchar(255) NOT NULL COMMENT 'Username',
  `firstname` varchar(100) DEFAULT NULL COMMENT 'First Name of the user',
  `lastname` varchar(100) DEFAULT NULL COMMENT 'Last Name of the user',
  `email` varchar(255) NOT NULL COMMENT 'Email',
  `password` varchar(255) NOT NULL COMMENT 'Password',
  `department` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `employee_id` varchar(12) DEFAULT '0',
  `created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'User Created Date',
  `last_login_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Last login Date',
  `activation_fpwd_key` varchar(500) DEFAULT NULL COMMENT 'Session_token',
  `activation_expiry` datetime DEFAULT NULL,
  `token_time` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `department`, `location`, `employee_id`, `created_date`, `last_login_date`, `activation_fpwd_key`, `activation_expiry`, `token_time`, `profile_image`, `enabled`) VALUES
(3, 'Drew', 'Drew', 'Anderson', 'mayur.sojrani@leftrightmind.com', '$2a$10$ydihbQmgb.BsXzYEGJwqPeQz5A1i9dUPaRKWUbdDb7SYy9Sst.G9C', 'Human Resources', 'San Antenio', '0', '2016-06-30 11:08:12', '2016-06-01 10:19:24', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg', 1),
(5, 'Lexi', 'Lexi', 'Simard', 'arun.jangid@leftrightmind.com', '$2a$10$UHzCbINC5jV3mJNrqikaVOvdwOjTkeN6DMfSBdVIhG6Y/iCr097F6', 'Human Resource', 'Interlaken', '0', '2016-06-30 11:08:25', '2016-06-01 10:23:57', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg', 1),
(6, 'mayursojrani', 'Mayur', 'Sojrani', 'shrish.apte@leftrightmind.com', '$2a$10$nl4kREmgF.W62GF1oaNAleLufzPmSe./odLerjydKaD.WTDdvlSpq', 'Operations', 'Freemont', '0', '2016-06-30 11:08:43', '2016-06-01 11:07:49', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/soyjavi/128.jpg', 1),
(16, 'Pamela', 'Pamela', 'Arias', 'mrunmayi.davare@leftrightmind.com', '$2a$10$6k39DMqA1tsO.XUda4sthOji8TCEZh0E7kTxh1vlmMtUoETrXdVQW', 'Sales', 'Fort Lauredel', '34728', '2016-06-30 11:08:54', '2016-06-01 14:19:44', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/esthercrawford/128.jpg', 1),
(17, 'johndoe', 'John', 'Doe', 'johndoe@gmail.com', '$2a$10$lgY9BWl2.slJCGU9d4uAg.eeMthT/qwMLEng9NIIInmFoTu6QSd0i', 'Legal', 'Orenburg Oblast', '0', '2016-06-30 11:09:54', '2016-06-02 13:26:21', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/marcogomes/128.jpg', 1),
(20, 'Emilie', 'Emilie', 'Wouters', 'samas@gmail.com', '$2a$10$zjMyV3hG7zXY7/2PFp.wDetugcSfyU5YqV4uBJf2wFBos6Xb1rHyW', 'Inventory', 'New Jersy', '2345', '2016-06-30 11:10:36', '2016-06-02 13:29:35', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/uxceo/128.jpg', 1),
(21, 'Marcel', 'Marcel', 'Schmid', 'test1@gmail.com', '$2a$10$5VGAUoCLzYKjcd0zFeaxEum87Asv1zLKui18w8/dqo94U0X1izdeu', 'software', 'Fremont', '008', '2016-06-30 11:10:52', '2016-06-02 14:07:27', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/jadlimcaco/128.jpg', 1),
(22, 'Markus', 'Markus', 'Johanson', 'md@gmail.com', '$2a$10$GhwKQqN0UcAaDRNNmWncT.TZln0JIKKyhWomwGT1xgpiYfjigOfSW', 'software', 'Sunnyvale', '019', '2016-06-30 11:11:03', '2016-06-03 10:30:48', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/lucaorio_/128.jpg', 1),
(27, 'Michael', 'Michael', 'Collins', 'shrish2406@gmail.com', '$2a$10$/NcF0WegBXa6tWL9I6uLpu3rQ0S1X9/kRCSkxzHu1iAKRMCepswyy', 'Marketing and Sales', 'New Hampshire', 'ABC123', '2016-06-30 11:11:16', '2016-06-22 11:36:30', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/_arashasghari/128.jpg', 1),
(28, 'Freddie', 'Freddie', 'Knight', 'test11@gmail.com', '$2a$10$KeKhs00nXyB0zM7pYzJy6.p290fmB1aI6vckgvZQ2ZhZOG4JCD1nC', 'Purchase', 'San Jose', 'abc234', '2016-06-30 11:11:28', '2016-06-22 11:39:30', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/c_southam/128.jpg', 1),
(29, 'Lewis', 'Lewis', 'Bennett', 'udita.joshi@leftrightmind.com', '$2a$10$g68JEaah99TSJe.O2rnOo.uvs8Mrly9x.YoS8bjBzF61ZU65m0taG', 'UI UX', 'Saint Louis', 'abc345', '2016-06-30 11:11:45', '2016-06-22 11:57:43', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/kurtvarner/128.jpg', 1),
(31, 'something@nowhere.com', 'Rivera', 'Raymond Rivera', 'something@nowhere.com', '$2a$10$WqStkVmfWEYx2gXQ/jIzB.aRBWDqStrX5xfaJJZLT/HPwe6P1nHjq', 'Human Resource Management', 'Colorado Springs', '123124', '2016-06-30 11:11:57', '2016-06-24 12:22:17', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/jlantunez/128.jpg', 1),
(32, 'test1@pin.com', 'William', 'Rodriguez', 'test1@pin.com', '$2a$10$a6hypFj0giTzx/MC2c281eFGhely5ecDxMVXZJBwXTyx7LanqGv0G', 'Human Resource Management', 'New York City', '111222', '2016-06-30 11:12:12', '2016-06-24 12:24:18', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/sortino/128.jpg', 1),
(33, 'test2@pin.com', 'José Carlos', 'Vicente', 'test2@pin.com', '$2a$10$gLSzwJgLY4f41/J6GccHJOTTt4EnhtZz2P5MrvizevscBk20c8mXm', 'Legal Department', 'Virginia Beach', 'ABCD123', '2016-06-30 11:12:23', '2016-06-24 12:26:49', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/aigarssilkalns/128.jpg', 1),
(34, 'test3@pin.com', 'Frederick', 'Holmes', 'test3@pin.com', '$2a$10$POkp73EHFdMFmYGtKwPJu.bUAuCOXtsai.JNtYIx3E1bQ9NYhnHtu', 'Legal Department', 'Long Island', 'ABCD1234', '2016-06-30 11:12:33', '2016-06-24 16:38:19', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/abn/128.jpg', 1),
(35, 'test4@pin.com', 'Daniel', 'Männik', 'test4@pin.com', '$2a$10$hfJfVpTIllExXVb5x9QGYuEfBRZLzYevB3LxG2MU4Tkm2q1cUi5S.', 'Legal Department', 'New Jersy', '24878', '2016-07-01 10:50:31', '2016-06-30 16:07:22', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg', 1),
(36, 'udita@pin.com', 'Udita', 'Joshi', 'udita@pin.com', '$2a$10$Emzm5QWAdTNXX7O5DlgLguOpulJeKaAi9Suqvb.Z5I6RBmk3R4RbO', 'Visual Interaction', 'Mumbai', '334455', '2016-07-01 10:50:41', '2016-06-30 16:23:04', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/indianguru/128.jpg', 1),
(37, 'nishant@pin.com', 'Nishant', 'Shirbhate', 'nishant@pin.com', '$2a$10$Zx6mQ.XYV5Z.AwQFQ9rcueVDdBTuyRfdPeIfipsI6rudTdhSElRXy', 'Visual Design', 'Yavatmal', '887766', '2016-07-01 10:50:51', '2016-06-30 16:24:05', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/kfriedson/128.jpg', 1),
(38, 'md@pin.com', 'Mrunmayi', 'Davare', 'md@pin.com', '$2a$10$1...QQ3A/409xJNZDLXiruuLNoK9JeYYgPc65X7LeI5R3T7a8yWya', 'UX', 'Nagar', '767676', '2016-07-01 10:51:00', '2016-06-30 17:27:39', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/towhidzaman/128.jpg', 1),
(39, 'saurabh@pin.com', 'Saurabh', 'Godbole', 'saurabh@pin.com', '$2a$10$TzEuZi2t.MfMZ2P.8lhxlefVvp8/1g084j.kZMo8a6h13ZIi4btkS', 'Legal', 'San Jose', '17', '2016-07-01 10:50:09', '2016-07-01 10:47:21', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/felipebsb/128.jpg', 1),
(40, 'mrun@pin.com', 'Mrunmayi', 'Davare', 'mrun@pin.com', '$2a$10$ZfBVB8t/HimgPETb.qCYCOg.UfmzKVgZRHn9HPmg3/bruXVjg3ohG', 'HTML & CSS', 'Nagar', '23', '2016-07-01 10:50:20', '2016-07-01 10:47:53', NULL, NULL, NULL, 'https://s3.amazonaws.com/uifaces/faces/twitter/itsjonq/128.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_relationships`
--

CREATE TABLE `user_relationships` (
  `id` int(11) NOT NULL,
  `user1_id` int(11) NOT NULL,
  `user1_rel_role` int(11) NOT NULL,
  `user1_tags` text COMMENT 'tags given by user2 for user1',
  `user2_id` int(11) NOT NULL,
  `user2_rel_role` int(11) NOT NULL COMMENT 'user2''s relation as seen by user1',
  `user2_tags` text COMMENT 'tags by user1 for user2',
  `created_date` datetime NOT NULL,
  `last_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_relationships`
--

INSERT INTO `user_relationships` (`id`, `user1_id`, `user1_rel_role`, `user1_tags`, `user2_id`, `user2_rel_role`, `user2_tags`, `created_date`, `last_modified_date`) VALUES
(37, 6, 1, NULL, 17, 3, NULL, '2016-06-29 18:07:56', '2016-06-30 09:30:56'),
(38, 28, 4, NULL, 3, 4, NULL, '2016-07-01 14:53:19', '2016-07-01 09:23:19'),
(39, 3, 4, '5', 5, 4, '5', '2016-06-29 18:35:49', '2016-06-29 13:05:49'),
(40, 3, 3, NULL, 20, 1, '5,1', '2016-07-01 11:02:44', '2016-07-01 05:32:44'),
(41, 6, 1, NULL, 16, 3, NULL, '2016-06-30 11:59:43', '2016-06-30 06:29:43'),
(42, 6, 3, NULL, 5, 1, '1, 3, 5', '2016-07-01 14:06:25', '2016-07-01 08:36:25'),
(43, 28, 4, NULL, 21, 4, '3', '2016-07-01 12:37:32', '2016-07-01 07:07:32'),
(44, 3, 1, NULL, 33, 3, NULL, '2016-07-01 15:15:56', '2016-07-01 09:45:56'),
(45, 28, 2, NULL, 20, 2, NULL, '2016-07-01 14:53:36', '2016-07-01 09:23:36'),
(46, 28, 3, NULL, 17, 1, NULL, '2016-06-30 18:32:13', '2016-06-30 13:02:13'),
(47, 28, 4, NULL, 27, 4, NULL, '2016-07-01 14:10:17', '2016-07-01 08:40:17'),
(48, 28, 2, NULL, 32, 2, NULL, '2016-07-01 14:09:49', '2016-07-01 08:39:49'),
(49, 28, 2, '5,1,3', 22, 2, '3', '2016-07-01 12:28:32', '2016-07-01 07:18:29'),
(50, 32, 2, NULL, 31, 2, NULL, '2016-07-01 11:27:46', '2016-07-01 05:57:46'),
(51, 21, 3, '1', 32, 1, '1', '2016-07-03 22:10:57', '2016-07-03 16:40:57'),
(52, 32, 2, NULL, 29, 2, NULL, '2016-06-30 17:42:13', '2016-06-30 12:12:13'),
(53, 6, 2, NULL, 3, 2, '1, 3', '2016-07-01 10:05:35', '2016-07-01 04:35:35'),
(54, 39, 1, NULL, 40, 3, '4', '2016-07-01 11:14:19', '2016-07-01 07:18:36'),
(55, 22, 2, NULL, 33, 2, '2', '2016-07-01 11:21:38', '2016-07-01 05:51:38'),
(56, 32, 4, NULL, 37, 4, NULL, '2016-07-01 12:46:50', '2016-07-01 07:16:50'),
(57, 32, 2, NULL, 35, 2, NULL, '2016-07-01 12:50:27', '2016-07-01 07:20:27'),
(58, 28, 2, NULL, 39, 2, NULL, '2016-07-01 15:29:20', '2016-07-01 09:59:20'),
(59, 3, 4, NULL, 39, 4, '1,4', '2016-07-01 15:10:30', '2016-07-01 09:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL,
  `role` varchar(45) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `role`, `userid`) VALUES
(27, 'ROLE_ADMIN', 6),
(26, 'ROLE_ADMIN', 25),
(15, 'ROLE_USER', 1),
(7, 'ROLE_USER', 3),
(8, 'ROLE_USER', 4),
(17, 'ROLE_USER', 5),
(18, 'ROLE_USER', 6),
(13, 'ROLE_USER', 9),
(14, 'ROLE_USER', 10),
(19, 'ROLE_USER', 16),
(20, 'ROLE_USER', 17),
(21, 'ROLE_USER', 20),
(22, 'ROLE_USER', 21),
(23, 'ROLE_USER', 22),
(24, 'ROLE_USER', 23),
(25, 'ROLE_USER', 24),
(36, 'ROLE_USER', 27),
(28, 'ROLE_USER', 28),
(29, 'ROLE_USER', 29),
(32, 'ROLE_USER', 31),
(33, 'ROLE_USER', 32),
(34, 'ROLE_USER', 33),
(35, 'ROLE_USER', 34),
(37, 'ROLE_USER', 35),
(38, 'ROLE_USER', 36),
(39, 'ROLE_USER', 37),
(40, 'ROLE_USER', 38),
(41, 'ROLE_USER', 39),
(42, 'ROLE_USER', 40);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_notifications`
--
CREATE TABLE `view_notifications` (
`notify_id` int(11)
,`U1_to_id` int(11)
,`U1_to_firstname` varchar(100)
,`U1_to_lastname` varchar(100)
,`U1_to_email` varchar(255)
,`U1_to_username` varchar(255)
,`U1_to_enabled` tinyint(1)
,`U2_from_id` int(11)
,`U2_from_firstname` varchar(100)
,`U2_from_lastname` varchar(100)
,`U2_from_email` varchar(255)
,`U2_from_username` varchar(255)
,`U2_from_enabled` tinyint(1)
,`msg_type` varchar(25)
,`msg_activity` varchar(25)
,`msg_timestamp` timestamp
,`msg_subject` varchar(50)
,`msg_body` mediumtext
,`msg_read_status` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pins`
--
CREATE TABLE `view_pins` (
`id` int(11)
,`event` varchar(255)
,`comment` varchar(255)
,`rating_set` int(11)
,`pinner` int(11)
,`pinner_firstname` varchar(100)
,`pinner_lastname` varchar(100)
,`pinner_email` varchar(255)
,`pinner_employee_id` varchar(12)
,`pinner_enabled` tinyint(1)
,`pinner_last_login_date` datetime
,`pinner_profile_image` varchar(255)
,`pinner_department` varchar(45)
,`pinner_location` varchar(45)
,`pinnee` int(11)
,`pinnee_firstname` varchar(100)
,`pinnee_lastname` varchar(100)
,`pinnee_email` varchar(255)
,`pinnee_employee_id` varchar(12)
,`pinnee_enabled` tinyint(1)
,`pinnee_last_login_date` datetime
,`pinnee_profile_image` varchar(255)
,`pinnee_department` varchar(45)
,`pinnee_location` varchar(45)
,`pinnee_relationship` int(11)
,`relation_role` varchar(25)
,`relation_role_description` varchar(75)
,`pin_tag` varchar(255)
,`objective` int(11)
,`pin_objective_title` varchar(255)
,`pin_rating_val_id` int(11)
,`pin_rating_value` int(11)
,`pin_rating_title` varchar(30)
,`pin_date` datetime
,`created_date` datetime
,`pin_quarter` int(1)
,`pin_year` int(4)
,`pin_year_month` int(6)
,`pin_status` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_relation_possibilities`
--
CREATE TABLE `view_relation_possibilities` (
`rp_id` int(11)
,`U1_rel_role_id` int(11)
,`U1_rel_role` varchar(25)
,`U2_rel_role_id` int(11)
,`U2_rel_role` varchar(25)
,`is_relation_possible` varchar(12)
,`is_edit_relation_possible` varchar(12)
,`is_rating_possible` tinyint(1)
,`notify_pinner` tinyint(1)
,`notify_pinnee` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_user_relationships`
--
CREATE TABLE `view_user_relationships` (
`user_rel_id` int(11)
,`User1` int(11)
,`User1_firstname` varchar(100)
,`User1_lastname` varchar(100)
,`User1_rel_role_id` int(11)
,`User1_rel_role` varchar(25)
,`User1_rel_role_description` varchar(75)
,`User1_tags` text
,`User1_enabled` tinyint(1)
,`User2` int(11)
,`User2_firstname` varchar(100)
,`User2_lastname` varchar(100)
,`User2_rel_role_id` int(11)
,`User2_rel_role` varchar(25)
,`User2_rel_role_description` varchar(75)
,`User2_tags` text
,`User2_enabled` tinyint(1)
,`created_date` datetime
,`last_modified_date` timestamp
);

-- --------------------------------------------------------

--
-- Structure for view `view_notifications`
--
DROP TABLE IF EXISTS `view_notifications`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_notifications` AS select `notify`.`id` AS `notify_id`,`notify`.`user_id` AS `U1_to_id`,`U1`.`firstname` AS `U1_to_firstname`,`U1`.`lastname` AS `U1_to_lastname`,`U1`.`email` AS `U1_to_email`,`U1`.`username` AS `U1_to_username`,`U1`.`enabled` AS `U1_to_enabled`,`notify`.`msg_from` AS `U2_from_id`,`U2`.`firstname` AS `U2_from_firstname`,`U2`.`lastname` AS `U2_from_lastname`,`U2`.`email` AS `U2_from_email`,`U2`.`username` AS `U2_from_username`,`U2`.`enabled` AS `U2_from_enabled`,`notify`.`msg_type` AS `msg_type`,`notify`.`msg_activity` AS `msg_activity`,`notify`.`msg_timestamp` AS `msg_timestamp`,`notify`.`msg_subject` AS `msg_subject`,`notify`.`msg_body` AS `msg_body`,`notify`.`msg_read_status` AS `msg_read_status` from ((`notifications` `notify` join `users` `U1` on((`U1`.`id` = `notify`.`user_id`))) join `users` `U2` on((`U2`.`id` = `notify`.`msg_from`)));

-- --------------------------------------------------------

--
-- Structure for view `view_pins`
--
DROP TABLE IF EXISTS `view_pins`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pins` AS select `p`.`id` AS `id`,`p`.`event` AS `event`,`p`.`comment` AS `comment`,`p`.`rating_set` AS `rating_set`,`p`.`pinner` AS `pinner`,`pinner`.`firstname` AS `pinner_firstname`,`pinner`.`lastname` AS `pinner_lastname`,`pinner`.`email` AS `pinner_email`,`pinner`.`employee_id` AS `pinner_employee_id`,`pinner`.`enabled` AS `pinner_enabled`,`pinner`.`last_login_date` AS `pinner_last_login_date`,`pinner`.`profile_image` AS `pinner_profile_image`,`pinner`.`department` AS `pinner_department`,`pinner`.`location` AS `pinner_location`,`p`.`pinnee` AS `pinnee`,`pinnee`.`firstname` AS `pinnee_firstname`,`pinnee`.`lastname` AS `pinnee_lastname`,`pinnee`.`email` AS `pinnee_email`,`pinnee`.`employee_id` AS `pinnee_employee_id`,`pinnee`.`enabled` AS `pinnee_enabled`,`pinnee`.`last_login_date` AS `pinnee_last_login_date`,`pinnee`.`profile_image` AS `pinnee_profile_image`,`pinnee`.`department` AS `pinnee_department`,`pinnee`.`location` AS `pinnee_location`,`p`.`pinnee_relationship` AS `pinnee_relationship`,`pin_relation`.`rel_role` AS `relation_role`,`pin_relation`.`description` AS `relation_role_description`,`p`.`tag` AS `pin_tag`,`p`.`objective` AS `objective`,`pin_objective`.`title` AS `pin_objective_title`,`p`.`rating` AS `pin_rating_val_id`,`pin_rating`.`rating_value` AS `pin_rating_value`,`pin_rating`.`rating_title` AS `pin_rating_title`,`p`.`pin_date` AS `pin_date`,`p`.`created_date` AS `created_date`,quarter(`p`.`pin_date`) AS `pin_quarter`,year(`p`.`pin_date`) AS `pin_year`,extract(year_month from `p`.`pin_date`) AS `pin_year_month`,`p`.`status` AS `pin_status` from (((((`pins` `p` join `users` `pinner` on((`p`.`pinner` = `pinner`.`id`))) join `users` `pinnee` on((`p`.`pinnee` = `pinnee`.`id`))) join `relationships` `pin_relation` on((`p`.`pinnee_relationship` = `pin_relation`.`id`))) join `objectives` `pin_objective` on((`p`.`objective` = `pin_objective`.`id`))) join `ratings` `pin_rating` on((`p`.`rating` = `pin_rating`.`rating_val_id`)));

-- --------------------------------------------------------

--
-- Structure for view `view_relation_possibilities`
--
DROP TABLE IF EXISTS `view_relation_possibilities`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_relation_possibilities` AS select `rp`.`id` AS `rp_id`,`rp`.`U1_rel_role` AS `U1_rel_role_id`,`r1`.`rel_role` AS `U1_rel_role`,`rp`.`U2_rel_role` AS `U2_rel_role_id`,`r2`.`rel_role` AS `U2_rel_role`,`rp`.`is_relation_possible` AS `is_relation_possible`,`rp`.`is_edit_relation_possible` AS `is_edit_relation_possible`,`rp`.`is_rating_possible` AS `is_rating_possible`,`rp`.`notify_pinner` AS `notify_pinner`,`rp`.`notify_pinnee` AS `notify_pinnee` from ((`relation_possibilities` `rp` join `relationships` `r1` on((`rp`.`U1_rel_role` = `r1`.`id`))) join `relationships` `r2` on((`rp`.`U2_rel_role` = `r2`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `view_user_relationships`
--
DROP TABLE IF EXISTS `view_user_relationships`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_user_relationships` AS select `ur`.`id` AS `user_rel_id`,`ur`.`user1_id` AS `User1`,`u1`.`firstname` AS `User1_firstname`,`u1`.`lastname` AS `User1_lastname`,`ur`.`user1_rel_role` AS `User1_rel_role_id`,`r1`.`rel_role` AS `User1_rel_role`,`r1`.`description` AS `User1_rel_role_description`,`ur`.`user1_tags` AS `User1_tags`,`u1`.`enabled` AS `User1_enabled`,`ur`.`user2_id` AS `User2`,`u2`.`firstname` AS `User2_firstname`,`u2`.`lastname` AS `User2_lastname`,`ur`.`user2_rel_role` AS `User2_rel_role_id`,`r2`.`rel_role` AS `User2_rel_role`,`r2`.`description` AS `User2_rel_role_description`,`ur`.`user2_tags` AS `User2_tags`,`u2`.`enabled` AS `User2_enabled`,`ur`.`created_date` AS `created_date`,`ur`.`last_modified_date` AS `last_modified_date` from ((((`user_relationships` `ur` join `users` `u1` on((`u1`.`id` = `ur`.`user1_id`))) join `users` `u2` on((`u2`.`id` = `ur`.`user2_id`))) join `relationships` `r1` on((`r1`.`id` = `ur`.`user1_rel_role`))) join `relationships` `r2` on((`r2`.`id` = `ur`.`user2_rel_role`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objectives`
--
ALTER TABLE `objectives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objective_tag`
--
ALTER TABLE `objective_tag`
  ADD PRIMARY KEY (`obj_tag_id`);

--
-- Indexes for table `pins`
--
ALTER TABLE `pins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`rating_val_id`);

--
-- Indexes for table `rating_sets`
--
ALTER TABLE `rating_sets`
  ADD PRIMARY KEY (`rating_set_id`);

--
-- Indexes for table `relationships`
--
ALTER TABLE `relationships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relation_possibilities`
--
ALTER TABLE `relation_possibilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `spring_session`
--
ALTER TABLE `spring_session`
  ADD PRIMARY KEY (`SESSION_ID`),
  ADD KEY `SPRING_SESSION_IX1` (`LAST_ACCESS_TIME`);

--
-- Indexes for table `spring_session_attributes`
--
ALTER TABLE `spring_session_attributes`
  ADD PRIMARY KEY (`SESSION_ID`,`ATTRIBUTE_NAME`),
  ADD KEY `SPRING_SESSION_ATTRIBUTES_IX1` (`SESSION_ID`);

--
-- Indexes for table `system_configuration`
--
ALTER TABLE `system_configuration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `config_param` (`config_param`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `uid_UNIQUE` (`id`),
  ADD UNIQUE KEY `session_id_UNIQUE` (`activation_fpwd_key`);

--
-- Indexes for table `user_relationships`
--
ALTER TABLE `user_relationships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_role_id`),
  ADD UNIQUE KEY `uni_username_role` (`role`,`userid`),
  ADD KEY `fk_userid` (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `objectives`
--
ALTER TABLE `objectives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `objective_tag`
--
ALTER TABLE `objective_tag`
  MODIFY `obj_tag_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pins`
--
ALTER TABLE `pins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `rating_val_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `rating_sets`
--
ALTER TABLE `rating_sets`
  MODIFY `rating_set_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `relationships`
--
ALTER TABLE `relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `relation_possibilities`
--
ALTER TABLE `relation_possibilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `system_configuration`
--
ALTER TABLE `system_configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` smallint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'uid',AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `user_relationships`
--
ALTER TABLE `user_relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `spring_session_attributes`
--
ALTER TABLE `spring_session_attributes`
  ADD CONSTRAINT `SPRING_SESSION_ATTRIBUTES_FK` FOREIGN KEY (`SESSION_ID`) REFERENCES `spring_session` (`SESSION_ID`) ON DELETE CASCADE;
