package com.springbootapp.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.springbootapp.model.AppErrorStatus;

/**
 * Override the default tomcat error page , tomcat should not send the html page
 * 
 * @author lrm_dev
 *
 */
@RestController
public class IndexController implements ErrorController {

	private static final String PATH = "/error";

	/*@Value("${debug}")
	private boolean debug;*/

	@Autowired
	AppErrorStatus appErrorStatus;

	@Autowired
	private ErrorAttributes errorAttributes;

	@RequestMapping(value = PATH)
	public AppErrorStatus error(HttpServletRequest request, HttpServletResponse response) {
		appErrorStatus.setStatus(response.getStatus());

		Map<String, Object> errorAttributes = getErrorAttributes(request, getTraceParameter(request));
		appErrorStatus.setError((String) errorAttributes.get("error"));
		appErrorStatus.setMessage((String) errorAttributes.get("message"));
		;
		appErrorStatus.setTimeStamp(errorAttributes.get("timestamp").toString());
		appErrorStatus.setTrace((String) errorAttributes.get("trace"));
		return appErrorStatus;
	}
	
	 private boolean getTraceParameter(HttpServletRequest request) {
		    String parameter = request.getParameter("trace");
		    if (parameter == null) {
		        return false;
		    }
		    return !"false".equals(parameter.toLowerCase());
		  }


	private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(request);
		return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
	}

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return PATH;
	}

}
