package com.springbootapp.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springbootapp.config.GlobalConfigs;
import com.springbootapp.datacontrollers.UserDataController;

import com.springbootapp.model.Response;

import com.springbootapp.model.User;
import com.springbootapp.utils.AppAuthCodes;
import com.springbootapp.utils.AppCommons;
import com.springbootapp.utils.AppError;

// TODO: Auto-generated Javadoc
/**
 * The Class StubbedAPIController.
 */
@RestController
@RequestMapping("/stubbed")
public class StubbedAPIController {
	
	/*
	*//** The User data controller. *//*
	@Autowired
	UserDataController UserDataController;

	*//** The user. *//*
	@Autowired
	User user;
	
	*//** The pin data controller. *//*
	@Autowired
	PinDataController pinDataController;
	
	*//** The pin. *//*
	@Autowired
	Pin pin;
	
	*//** The objective. *//*
	@Autowired
	Objective objective;

	*//** The Constant ACK. *//*
	private static final String ACK = AppCommons.ACK;
	
	*//** The Constant NACK. *//*
	private static final String NACK = AppCommons.NACK;


	
	 * @RequestMapping("/login") public Response greeting(@RequestParam(value =
	 * "name", defaultValue = "World") String name) { return new
	 * Greeting(counter.incrementAndGet(), String.format(template, name)); }
	 

	*//**
	 * Greeting.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 *//*
	@RequestMapping("/login")
	public Response<User> greeting(Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();
		session.setAttribute("username", principal.getName().toString());
		//1 month session
	//	session.setMaxInactiveInterval(2592000);
		
		//1 hr session
		//session.setMaxInactiveInterval(3600);
		
		//5 hr session
				session.setMaxInactiveInterval(18000);
		
		
		if(session != null)
		{
		user = (User) GlobalConfigs.getInstance().getBean("userBean");
		
		user.setEmployee_id("009");
		user.setFirstname("John");
		user.setLastname("Doe");
		user.setUsername("johndoe");
		user.setRole("ROLE_USER");
		user.setEmail("john@gmail.com");
		user.setLocation("Fremont USA");
		user.setDepartment("IT");
		//user.setIs_active(true);
		user.setAuthstring(session.getId());
	
		response.setStatus(ACK);
		response.setMsg("Success");
		response.setData(user);
		
		}
		
		return response;
	}

	*//**
	 * This will return the username of the currently logged in user.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 *//*
	@RequestMapping("/whoami")
	public Response<String> whoAmI(Principal principal, HttpSession session) {
		Response<String> response = new Response<String>();
		response.setStatus(ACK);
		response.setData(principal.getName().toString());
		return response;
	}

	*//**
	 * API to invalidate/logout the given session by the user.
	 *
	 * @param session the session
	 *//*
	 @ResponseStatus(HttpStatus.NO_CONTENT) 
	@RequestMapping("/logout")
	public void logout(HttpSession session) {
		session.invalidate();
	}

	*//**
	 * TODO : Add the Principal object to verify sessions against the user
	 * names.
	 *
	 * @param user the user
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 *//*
	@RequestMapping(value = "/updateuserprofile", method = RequestMethod.POST, produces = "application/json")
	public Response<User> updateUserProfiel(@RequestBody User user, Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();

		user.setUsername(principal.getName());
		user = UserDataController.updateUserProfile(user);

		if (user != null) {
			response.setStatus(ACK);
			response.setData(user);
		} else {
			response.setStatus(NACK);
			response.setData(user);
		}
		return response;
	}

	*//**
	 * TODO : Add the Principal object to verify sessions against the user name
	 * Update the user profile image once the user is verified.
	 *
	 * @param user the user
	 * @return the response
	 *//*
	@RequestMapping(value = "/saveprofileimage", method = RequestMethod.POST, produces = "application/json")
	public Response<User> updateUserProfileImage(@RequestBody User user) {
		Response<User> response = new Response<User>();

		user = UserDataController.updateUserProfileImage(user);

		if (user != null) {
			response.setStatus(ACK);
			response.setData(user);
		} else {
			response.setStatus(NACK);
			response.setData(user);
		}
		return response;
	}

	*//**
	 * Stubbed API.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return the all users
	 *//*
	@RequestMapping(value = "/getallusers", method = RequestMethod.GET, produces = "application/json")
	public Response<User> getAllUsers(Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();
		// List<User> mUsersList = UserDataController.getAllUsers(user);
		
		if (AppCommons.verifySession(session) == true) {
		if(session != null)
		{
	
		String username = session.getAttribute("username").toString();
	
		if (username != null) {
				
			List<User> mUsersList = new ArrayList<User>();
			User userBean1 = (User) GlobalConfigs.getInstance().getBean("userBean");
			userBean1.setEmployee_id("001");
			userBean1.setFirstname("John");
			userBean1.setLastname("Doe");
			userBean1.setEmail("john@gmail.com");
			userBean1.setLocation("Fremont USA");
			//userBean1.setIs_active(true);
			User userBean2 = (User) GlobalConfigs.getInstance().getBean("userBean");
			userBean2.setEmployee_id("002");
			userBean2.setFirstname("John");
			userBean2.setLastname("Doe");
			userBean1.setEmail("john@gmail.com");
			userBean1.setLocation("Fremont USA");
			//userBean1.setIs_active(true);
			User userBean3 = (User) GlobalConfigs.getInstance().getBean("userBean");
			userBean3.setEmployee_id("003");
			userBean3.setFirstname("John");
			userBean3.setLastname("Doe");
			userBean1.setEmail("john@gmail.com");
			userBean1.setLocation("Fremont USA");
			//userBean1.setIs_active(true);
			mUsersList.add(userBean1);
			mUsersList.add(userBean2);
			mUsersList.add(userBean3);
			response.setStatus(ACK);
			response.setDataList(mUsersList);			
	
							  }
		}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		return response;
	}

	*//**
	 * Stubbed API.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return the count for notification
	 *//*
	
	
	@RequestMapping(value = "/getcountfornotification", method = RequestMethod.GET, produces = "application/json")
	public Response<User> getCountForNotification(Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();
		
		if (AppCommons.verifySession(session) == true) {
		User userBean1 = (User) GlobalConfigs.getInstance().getBean("userBean");
		// userBean1.setNotificationCount(100);
		response.setStatus(ACK);
		response.setData(userBean1);
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		
		return response;
	}

	*//**
	 * Stubbed API.
	 *
	 * @param notificationListIncoming the notification list incoming
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 *//*
	@RequestMapping(value = "/makenotificationsread", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Response<Notification> makeNotificationsRead(@RequestBody List<Notification> notificationListIncoming,
			Principal principal, HttpSession session) {
		Response<Notification> response = new Response<Notification>();
		response.setStatus(ACK);
		response.setMsg("All notification are made as mark");
		List<Notification> notificationList = new ArrayList<Notification>();
		Notification notif1 = (Notification) GlobalConfigs.getInstance().getBean("notificationBean");
		notif1.setId(1);
		notif1.setReadStatus(0);
		//notif1.setNotificationType("Type1");
		notificationList.add(notif1);
		Notification notif2 = (Notification) GlobalConfigs.getInstance().getBean("notificationBean");
		notif2.setId(1);
		notif2.setReadStatus(0);
		//notif2.setNotificationType("Type1");
		notificationList.add(notif2);
		response.setData(notif1);
		response.setDataList(notificationList);
		return response;
	}
	
	
	*//**
	 * Stubbed API.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return saurabh
	 *//*
	
	
	//if you use consume s you will have to send content/type from client (consumes = "application/json")
	@RequestMapping(value = "/getfavourites", method = RequestMethod.GET, produces = "application/json")
	public Response<User> getFavourites(Principal principal, HttpSession session)
	{
		Response<User> response = new Response<User>();
		// List<User> mUsersList = UserDataController.getAllUsers(user);
		
		if (AppCommons.verifySession(session) == true) {
		if(session != null)
		{
	
		String username = session.getAttribute("username").toString();
	
		if (username != null) {
				
		List<User> mUsersList = new ArrayList<User>();
		User userBean1 = (User) GlobalConfigs.getInstance().getBean("userBean");
		userBean1.setEmployee_id("001");
		userBean1.setFirstname("John");
		userBean1.setLastname("Doe");
		userBean1.setEmail("john@gmail.com");
		userBean1.setLocation("Fremont USA");
		//userBean1.setIs_active(true);
		User userBean2 = (User) GlobalConfigs.getInstance().getBean("userBean");
		userBean2.setEmployee_id("002");
		userBean2.setFirstname("John");
		userBean2.setLastname("Doe");
		userBean1.setEmail("john@gmail.com");
		userBean1.setLocation("Fremont USA");
		//userBean1.setIs_active(true);
		User userBean3 = (User) GlobalConfigs.getInstance().getBean("userBean");
		userBean3.setEmployee_id("003");
		userBean3.setFirstname("John");
		userBean3.setLastname("Doe");
		userBean1.setEmail("john@gmail.com");
		userBean1.setLocation("Fremont USA");
		//userBean1.setIs_active(true);
		mUsersList.add(userBean1);
		mUsersList.add(userBean2);
		mUsersList.add(userBean3);
		response.setStatus(ACK);
		response.setDataList(mUsersList);
							  }
		}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		
		return response;
		
	}
	
	


	*//**
	 * pin stubbed API's
	 * Save the pin.
	 *
	 * @param pin the pin
	 * @param session the session
	 * @return the response
	 *//*
	@RequestMapping(value = "/addpin", method = RequestMethod.POST, produces = "application/json",consumes="application/json")
	public Response<Pin> updateUserProfiel(@RequestBody Pin pin, HttpSession session) {
		Response<Pin> response = new Response<Pin>();
		
		String username = session.getAttribute("username").toString();
		user.setUsername(username);
		if (username != null)
			pin = pinDataController.savePin(pin);

		if (pin != null) {
			response.setStatus(ACK);
			response.setData(pin);
		} else {
			response.setStatus(NACK);
			response.setData(pin);
		}
		return response;
	}
	
	@RequestMapping(value = "/getfrom", method = RequestMethod.GET, produces = "application/json")
	public Response<Pin> getPinFrom(HttpSession session) {
		Response<Pin> response = new Response<Pin>();
		List<Pin> pinList = new ArrayList<Pin>();
		String username = session.getAttribute("username").toString();
		user.setUsername(username);
		if (username != null)
			pinList = pinDataController.getPinFrom(pin, user);

		if (pinList != null) {
			response.setStatus(ACK);
			response.setDataList(pinList);
		} else {
			response.setStatus(NACK);
			//response.setData(pin);
		}
		return response;
	}
	
	*//**
	 * Gets the pin to.
	 *
	 * @param pin the pin
	 * @param session the session
	 * @return the pin to
	 *//*
	@RequestMapping(value = "/getto", method = RequestMethod.GET, produces = "application/json")
	public Response<Pin> getPinTo(@RequestBody Pin pin, HttpSession session) {
		Response<Pin> response = new Response<Pin>();

		String username = session.getAttribute("username").toString();
		user.setUsername(username);
		if (username != null)
			pin = pinDataController.savePin(pin);

		if (pin != null) {
			response.setStatus(ACK);
			response.setData(pin);
		} else {
			response.setStatus(NACK);			
			response.setMsg(AppError.getInstance().getLastErrorMessage());
		}
		return response;
	}
	
	
	
	*//**
	 * Stubbed API.
	 *
	 * @param user the user
	 * @return the response
	 *//*
	
	@RequestMapping(value = "/getpinbyme", method = RequestMethod.GET, produces = "application/json")
	public Response<Pin> getPinByMe(HttpSession session) {
		Response<Pin> response = new Response<Pin>();
		List<Pin> pinList = new ArrayList<Pin>();
		String username = session.getAttribute("username").toString();
		user.setUsername(username);
		if (username != null)
			pinList = pinDataController.getPinFrom(pin, user);

		if (pinList != null) {
			response.setStatus(ACK);
			response.setDataList(pinList);
		} else {
			response.setStatus(NACK);
			//response.setData(pin);
		}
		return response;
	}
	
	
	*//**
	 * Stubbed API
	 * @param session
	 * @return
	 *//*
	
	@RequestMapping(value = "/getpintome", method = RequestMethod.GET, produces = "application/json")
	public Response<Pin> getPinToMe(HttpSession session) {
		Response<Pin> response = new Response<Pin>();
		List<Pin> pinList = new ArrayList<Pin>();
		String username = session.getAttribute("username").toString();
		user.setUsername(username);
		if (username != null)
			pinList = pinDataController.getPinFrom(pin, user);

		if (pinList != null) {
			response.setStatus(ACK);
			response.setDataList(pinList);
		} else {
			response.setStatus(NACK);
			//response.setData(pin);
		}
		return response;
	}


	
	
	
	*//**
	 * returns true if user created successfully, false when user not created successfully
	 * @param user
	 * @return
	 *//*
	@RequestMapping(value = "/registeruser", method = RequestMethod.POST, produces = "application/json")
	public Response<User> updateUserProfiel(@RequestBody User user) {
		Response<User> response = new Response<User>();		

		user = UserDataController.registerUser(user);
		if (user != null) {
			response.setStatus(ACK);
			response.setMsg("Register Success");
			response.setData(user);
		} else {
			response.setStatus(NACK);
			response.setMsg("User already exists");
			response.setData(user);
		}
		return response;
	}
*/	
	
	
	


}
