package com.springbootapp.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.springbootapp.datacontrollers.SampleDataController;
import com.springbootapp.model.Response;
import com.springbootapp.model.Sample;

// TODO: Auto-generated Javadoc
/**
 * The Class SampleController.
 */
@RestController
@RequestMapping("/sample")
public class SampleController {

	/** The sample. */
	@Autowired
	Sample sample;

	/** The sample data controller. */
	@Autowired
	SampleDataController sampleDataController;
	
	/**
	 * Gets the project object.
	 *
	 * @return the project object
	 */
	@RequestMapping(value = "/getallsamples", method = RequestMethod.GET, produces = "application/json")
	public Response<Sample> getProjectObject() {
		Response<Sample> response = new Response<Sample>();
		
		List<Sample> listSamples = new ArrayList<Sample>();
		
		listSamples = sampleDataController.getAllSamples();
		
		response.setDataList(listSamples);
		
		return response;

	}

}
