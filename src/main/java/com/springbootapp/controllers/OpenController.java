package com.springbootapp.controllers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springbootapp.data.JdbcCustomUserDAO;
import com.springbootapp.datacontrollers.UserDataController;
import com.springbootapp.datacontrollers.CompanyDataController;
import com.springbootapp.model.Company;
import com.springbootapp.model.Response;
import com.springbootapp.model.User;
import com.springbootapp.utils.AppAuthCodes;
import com.springbootapp.utils.AppCommons;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenController.
 */
@RestController
@RequestMapping("/openservices")
public class OpenController {

	/** The user data controller. */
	@Autowired
	UserDataController userDataController;
	
	@Autowired
	CompanyDataController companyDataController;
	
	/** The user. */
	@Autowired
	User user;	
	
	
	/** The jdbc custom user DAO. */
	@Autowired
	JdbcCustomUserDAO jdbcCustomUserDAO;
	
	// The following three Needs to be moved into DataController.	

	/** The Constant ACK. */
	private static final String ACK = AppCommons.ACK;
	
	/** The Constant NACK. */
	private static final String NACK = AppCommons.NACK;
	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(OpenController.class);
		
	/**
	 * Step 1 : Admin calls this web service for the activation mail
	 * Expected Output , user id specified in the notification object should get the mail with the link
	 * @param notification
	 * @param session
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST, produces = "application/json")
	public Response<User> forgotPassword(@RequestBody User user1,
			HttpServletRequest request) {
		Response<User> response = new Response<User>();

		if (user1 != null) {

			Map<String, String> data = new HashMap<String, String>();

			user = userDataController.getUserProfileByEmail(user1.getEmail());
			log.info("this is user"+user);
			if (user.getEmail() != null) {
			

				String token = UUID.randomUUID().toString();
				
				// Update the user token in the database as well
				String appUrl = "http://" + request.getServerName() + ":"
						+ request.getServerPort() + request.getContextPath();
				data.put("login", user.getUsername());
				String link = "<a href=" + appUrl + " target=\"_blank\">Click here to Reset the password</a>";
				data.put("link", appUrl);
				data.put("token", token);
				// WE WILL KEEP 0 as system user.

				response.setStatus(ACK);
				response.setMsg("Added a notification!");

			}
		} else {
			response.setStatus(NACK);
			response.setMsg("Failed to add notification!");

		}

		return response;
	}
	
	/**
	 * Step 2 : User clicks on the link in the mail and gets redirected to the page where he can change the password
	 * This method redirects the user to updatePassword page if the email and tokens are valid.
	 *
	 * @param locale the locale
	 * @param model the model
	 * @param email the email
	 * @param token the token
	 * @param request the request
	 * @return the model and view
	 */
	@RequestMapping(value = "/user/changepassword", method = RequestMethod.GET)
	public ModelAndView showChangePasswordPage(
	  Locale locale, Model model, @RequestParam("email") String email, @RequestParam("token") String token,HttpServletRequest request) {		
		// Verify the email and token , also verify it against the time
		log.info("!!!!!In the openservices to reset the password.");
		ModelAndView modelAndView = null;
		String appUrl="";
		appUrl = "http://" + request.getServerName(); /*+ ":" + request.getServerPort()*/
		//		+ request.getContextPath();		
	//	String appUrl = AppCommons.APP_BASE_URL
		if(userDataController.verifyToken(email,token))
		{
			// Token is valid , forward the user to update password page			
			//String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
			//+ request.getContextPath();
			
			//modelAndView = new ModelAndView("redirect:http://10.10.10.81:8085/phrooglecalc21jan/#/resetpassword?email="+email+"&token="+token);
			log.info(appUrl);
			//http://10.10.10.137/PIN/pinWeb/#/resetpassword
			log.info("!!!!Redirecting the user");			
			log.info("IP address : "+appUrl);
			modelAndView = new ModelAndView("redirect:"+appUrl+"/#/resetpassword?email="+email+"&token="+token);							
		}	
		else
		{
			modelAndView = new ModelAndView("redirect:"+appUrl+"/#/error");
			log.warn("Invalid password request for :"+email);
		}	
		log.info("Redirecting the user");
        return modelAndView;
	}
	
	/**
	 * Step 3: Once user send the password in user object with password , password gets reset for the user
	 * Description : This method updates the password of the user
	 * Note : You must specify the email address , passwordToken , password (i.e. new password)
	 *
	 * @param userIn the user in
	 * @param request the request
	 * @return true/false
	 */
	@RequestMapping(value = "/updatepasswordusingtoken", method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	public Response<User> updatePassword(@RequestBody User userIn, HttpServletRequest request) {
		Response<User> response = new Response<User>();
		//userIn.setPassword(PhroogleUtils.getInstance().createPasswordHash(userIn.getPassword()));
		if(userDataController.updatePasswordUsingToken(userIn))
		{
			//Password update was successful
			response.setStatus(ACK);			
		}
		else
		{
			response.setStatus(NACK);
		}
		return response;
	}	
	
	/**
	 * Get Company base url by com_login_domain.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	
	 */
	
	@RequestMapping(value = "/identifybaseurl ", method = RequestMethod.POST, produces = "application/json",consumes="application/json")
	public Response<Company> identifyBaseUrl(@RequestBody Company company, Principal principal) {
		Response<Company> response = new Response<Company>();
					
				
				Company companydata = companyDataController.identifyBaseUrl(company);
						
				if (companydata != null) {
					response.setStatus(ACK);
					response.setMsg("company profile !");
					response.setData(companydata);
				} else {
					response.setStatus(NACK);
					response.setMsg("com_login_domain does not exist !");
					response.setData(companydata);
				}
			
				
		return response;

	}
	
	
}
