package com.springbootapp.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springbootapp.config.GlobalConfigs;
import com.springbootapp.datacontrollers.SystemConfigurationDataController;
import com.springbootapp.datacontrollers.UserDataController;
import com.springbootapp.model.MessageByLocaleService;

import com.springbootapp.model.Response;
import com.springbootapp.model.User;
import com.springbootapp.utils.AppCommons;

/**
 * Login Controller : Provides the user related functionalities like login,logout 
 * @author lrm_dev
 *
 */
@RestController
@RequestMapping("/auth")
public class LoginController {

	@Autowired
	UserDataController userDataController;
	
	@Autowired
	SystemConfigurationDataController systemConfigurationDataController;

	@Autowired
	 MessageByLocaleService messageByLocaleService;
	
	@Autowired
	User user;

	private static final String ACK = AppCommons.ACK;
	private static final String NACK = AppCommons.NACK;

	/*
	 * @RequestMapping("/login") public Response greeting(@RequestParam(value =
	 * "name", defaultValue = "World") String name) { return new
	 * Greeting(counter.incrementAndGet(), String.format(template, name)); }
	 */

	@CrossOrigin
	//	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
	@RequestMapping("/login")
	public Response<User> greeting(Principal principal, HttpSession session,Device device,HttpServletRequest request) {
		Response<User> response = new Response<User>();
		
		if(AppCommons.configMap.isEmpty())
		{
			//Load the configuration map in the memory
			systemConfigurationDataController.loadConfigMap();
			
		}
		//session.setAttribute("username", principal.getName().toString());
		
		//1 month session
		//session.setMaxInactiveInterval(2592000);
		
		if(session != null)
		{
			List<User> usersList = new ArrayList<User>();
			//fetch user object from database.
			
			user = userDataController.getMyProfile(principal.getName().toString());
			//set user_id in the session	
			
			if(user == null)
			{
				response.setStatus(NACK);
				response.setMsg(messageByLocaleService.getMessage("login.signin.failure"));
				response.setData((user));
				
				
			}
			else
			{
				AppCommons.printDeviceInfoUsingRequestAndDevice(request, device, AppCommons.TAG_LOGIN);
				//session.setAttribute("session_user_id", user.getId());
				user.setAuthstring(session.getId());
				usersList.add(user);
				response.setStatus(ACK);
				response.setMsg(messageByLocaleService.getMessage("login.signin.success"));
				response.setData((user));
			}
		
		}
		
		return response;
	}

	/**
	 * This will return the username of the currently logged in user
	 * 
	 * @param principal
	 * @param session
	 * @return
	 */
	@RequestMapping("/whoami")
	public Response<String> whoAmI(Principal principal, HttpSession session) {
		Response<String> response = new Response<String>();
		response.setStatus(ACK);
		//response.setDataList(principal.getName().toString());
		return response;
	}

	/**
	 * API to invalidate/logout the given session by the user
	 * 
	 * @param session
	 */
	/* @ResponseStatus(HttpStatus.NO_CONTENT) */	
	@RequestMapping(value="/logout", produces = "application/json")
	public Response<User> logout(Principal principal,HttpSession session) {
		session.invalidate();
		Response<User> response = new Response<User>();
		user = userDataController.getMyProfile(principal.getName().toString());
		response.setData(user);
		response.setStatus(ACK);
		response.setMsg("Log out success !");
		return response;
	}

	

		

	
	
}
