package com.springbootapp.controllers;
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.springbootapp.model.Response;
import com.springbootapp.model.Response2;
import com.springbootapp.model.User;
import com.springbootapp.datacontrollers.CompanyDataController;
import com.springbootapp.model.Company;
import com.springbootapp.utils.AppAuthCodes;
import com.springbootapp.utils.AppCommons;
import com.springbootapp.utils.AppException;

//TODO: Auto-generated Javadoc

/**
* The Class CompanyAPIController.
*/
@RestController
@RequestMapping("/company")

public class CompanyAPIController {
	
	/** The Constant ACK. */
	private static final String ACK = AppCommons.ACK;
	
	/** The Constant NACK. */
	private static final String NACK = AppCommons.NACK;

	@Autowired
	CompanyDataController companyDataController;

	/** The user. */
	@Autowired
	Company company;
	
	/** The Constant log. */
	//private static final Logger log = LoggerFactory.getLogger(CompanyAPIController.class);

	/**
	 * Adding New Company Details.
	 *
	 * @param company
	 * @return the response
	 * @throws AppException 
	 */
	
	@CrossOrigin
	@RequestMapping(value = "/addcompany", method = RequestMethod.POST, produces = "application/json")
	public Response<Company> addCompany(@RequestBody Company company,HttpSession session,HttpServletRequest request) throws AppException {
		
		Response<Company> response = new Response<Company>();
		
		Company resp = companyDataController.addCompany(company);
		
		if(resp!= null)
			{	
				response.setStatus(ACK);
				response.setMsg("Inserted Successfully ! Need to activate the company from the admin panel");
				response.setData(resp);
				
			} else {
				response.setStatus(NACK);
				response.setMsg("company data not valid!");
				response.setData(resp);
			}
		
		return response;
	}
	
	/**
	 * get the list of companies
	 *
	 * @param company
	 * @return the response
	 */	
	
	@CrossOrigin
	@RequestMapping(value = "/getallcompanies", method = RequestMethod.GET, produces = "application/json")
	
	public Response2<Company> getAllCompanies(Principal principal, HttpSession session) {
		
		Response2<Company> response = new Response2<Company>();
			
		if (AppCommons.verifySession(session) == true) {
			
			if(session != null)
				{
			
				Integer user_id = (Integer)session.getAttribute("session_user_id");
			
				if (user_id != null) {
					
						List<Company> listSamples = new ArrayList<Company>();						
						listSamples = companyDataController.getAllCompanies();			
						response.setStatus(ACK);
						response.setMsg("Successfully got "+listSamples.size()+" companies !");
						response.setData(listSamples);
						return response;
					}
				}
		} else {
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		return response;
	}
		
	
	/**
	 * get the list of company domains
	 *
	 * @param company
	 * @return the response
	 */
	
	
	@CrossOrigin
	@RequestMapping(value = "/getcomdomains", method = RequestMethod.POST, produces = "application/json")
	
	public Response2<Company> getComdomains(@RequestBody Company company,HttpSession session,HttpServletRequest request) {
		
		Response2<Company> response = new Response2<Company>();
			
		if (AppCommons.verifySession(session) == true) {
			
			if(session != null)
				{
			
				Integer user_id = (Integer)session.getAttribute("session_user_id");
			
				if (user_id != null) {
					
						List<Company> listSamples = new ArrayList<Company>();
						//listSamples = companyDataController.getAllCompanies();		
						listSamples = companyDataController.getComdomains(company);		
						response.setStatus(ACK);
						response.setMsg("Successfully got "+listSamples.size()+" companies !");
						response.setData(listSamples);
						return response;
					}
				}
		} else {
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		return response;
	}
	
	/**
	 * Get Company profile by com_id.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	
	 */
	@RequestMapping(value = "/getcompanyprofile", method = RequestMethod.POST, produces = "application/json",consumes="application/json")
	public Response<Company> uploadProfileImage(@RequestBody Company company, Principal principal, HttpSession session) {
		Response<Company> response = new Response<Company>();
		//log.info("Base64string : "+user.getBase64String());		
		if (AppCommons.verifySession(session) == true) {
			
			String username = session.getAttribute("username").toString();
			company.setUsername(username);
			
			if (username != null){
			
				Company companydata = companyDataController.getCompanyProfileById(company);
										
				if (companydata != null) {
					response.setStatus(ACK);
					response.setMsg("company profile !");
					response.setData(companydata);
				} else {
					response.setStatus(NACK);
					response.setMsg("com_id does not exist !");
					response.setData(companydata);
				}
			} else {
				
				response.setStatus(NACK);
				response.setMsg("Can't fetch data from company !");
				//response.setData(companydata);
			}
			
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS.toString());	
		}		
		return response;

	}
	
	
	
	/**
	 * TODO : Add the Principal object to verify sessions against the company
	 * names.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 * @throws AppException 
	 */
	@CrossOrigin
	@RequestMapping(value = "/updatecompanyprofile", method = RequestMethod.POST, produces = "application/json")
	public Response<Company> updateCompanyProfile(@RequestBody Company company, Principal principal, HttpSession session) throws AppException {
		Response<Company> response = new Response<Company>();

		company = companyDataController.updateCompanyProfile(company);
		
		if (company != null) {
			Company companydata = companyDataController.getCompanyProfileById(company);
			response.setStatus(ACK);
			response.setMsg(" company profile update successful !");
			response.setData(companydata);
		} else {
			response.setStatus(NACK);
			response.setMsg(" company profile update failed !");
			response.setData(company);
		}
		return response;
	}
	
	
	
	
	/**
	 * TODO : Add the Principal object to verify sessions against the company
	 * names.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 * @throws AppException 
	 */
	@CrossOrigin
	@RequestMapping(value = "/deletecompanyprofile", method = RequestMethod.POST, produces = "application/json")
	public Response<Company> deleteCompanyProfile(@RequestBody Company company, Principal principal, HttpSession session) throws AppException {
		Response<Company> response = new Response<Company>();

	//	user.setUsername(principal.getName());
		//company = companyDataController.updateCompanyProfile(company);
		company = companyDataController.deleteCompanyProfile(company);
		
		if (company != null) {
			response.setStatus(ACK);
			response.setMsg(" company profile delete successfully !");
			//response.setData(company);
		} else {
			response.setStatus(NACK);
			response.setMsg(" unable to delete companuy profile !");
			//response.setData(company);
		}
		return response;
	}
	
	
	/**
	 * Activate deactivate company profile.
	 *
	 * @param com_id the com_id
	 * @param action the action
	 * @return true, if successful
	 */
	
	public boolean activateDeactivateCompany(Company company,String action)
	{
		boolean resp = false;
		
		if(company!=null && action !=null )
		{
			if(action.equalsIgnoreCase("activate"))
			{
				companyDataController.activateCompany(company);
				
			}
			else 
			{
				companyDataController.deactivateCompany(company);
			}
		}
		
		return resp;
		
		
	}
	
	/**
	 *  Activate Company Profile.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	
	@CrossOrigin
	@RequestMapping(value = "/activatecompany", method = RequestMethod.POST, produces = "application/json")
	public Response<Company> activateCompany(@RequestBody Company company, Principal principal, HttpSession session) {
		Response<Company> response = new Response<Company>();

		boolean check;
		//user.setUsername(principal.getName());
		check = companyDataController.activateCompany(company);
		Company companydata = companyDataController.getCompanyProfileById(company);
		if (check != false) {
			response.setStatus(ACK);
			response.setMsg(" Company Profile Activated !");
			response.setData(companydata);
		} else {
			response.setStatus(NACK);
			response.setMsg(" Failed  !");
		//	response.setData(user);
		}
		return response;
	}
	
	

	/**
	 *  Deactivate Company Profile.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	
	@CrossOrigin
	@RequestMapping(value = "/deactivatecompany", method = RequestMethod.POST, produces = "application/json")
	public Response<Company> deactivateCompany(@RequestBody Company company, Principal principal, HttpSession session) {
		Response<Company> response = new Response<Company>();

		boolean check;
		//user.setUsername(principal.getName());
		check = companyDataController.deactivateCompany(company);
		Company companydata = companyDataController.getCompanyProfileById(company);
		if (check != false) {
			response.setStatus(ACK);
			response.setMsg(" User Deactivated !");
			response.setData(companydata);
		} else {
			response.setStatus(NACK);
			response.setMsg(" Failed  !");
		//	response.setData(user);
		}
		return response;
	}
	
	
	/**
	 *  Update Company Domains.
	 *
	 * @param company the company
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 * @throws AppException 
	 */
	
	@CrossOrigin
	@RequestMapping(value = "/updatecomdomains", method = RequestMethod.POST, produces = "application/json")
	public Response<Company> updateCompanydomains(@RequestBody Company company, Principal principal, HttpSession session) throws AppException {
		Response<Company> response = new Response<Company>();
			
		Company resp = companyDataController.updateCompanydomains(company);
			
		if(resp!=null)
		{
			
			response.setStatus(ACK);
			response.setMsg("updated Successfully !");
			response.setData(resp);
		}
		else {
			response.setStatus(NACK);
			response.setMsg("Company domains not updated !");
			response.setData(resp);
		}
		
					
		return response;
	}
	
	
}