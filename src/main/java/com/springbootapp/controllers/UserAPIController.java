package com.springbootapp.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springbootapp.config.GlobalConfigs;
import com.springbootapp.datacontrollers.UserDataController;
import com.springbootapp.model.Response;
import com.springbootapp.model.Response2;
import com.springbootapp.model.Sample;
import com.springbootapp.model.User;
import com.springbootapp.utils.AppAuthCodes;
import com.springbootapp.utils.AppCommons;

// TODO: Auto-generated Javadoc
/**
 * The Class UserAPIController.
 */
@RestController
@RequestMapping("/user")
public class UserAPIController {	

	
	/** The Constant ACK. */
	private static final String ACK = AppCommons.ACK;
	
	/** The Constant NACK. */
	private static final String NACK = AppCommons.NACK;
	
	/** The user data controller. */
	@Autowired
	UserDataController userDataController;
	
	/** The user. */
	@Autowired
	User user;
	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(UserAPIController.class);

	/**
	 * Stubbed API.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return the all users
	 */
	
	@CrossOrigin
	@RequestMapping(value = "/getallusers", method = RequestMethod.GET, produces = "application/json")
	public Response2<User> getAllUsers(@RequestParam(value="skipMe",required=false) Boolean skipMe,@RequestParam(value="skipDisabled",required=false) Boolean skipDisabled,Principal principal, HttpSession session) {
		Response2<User> response = new Response2<User>();
		// List<User> mUsersList = UserDataController.getAllUsers(user);
	//	List<Pin> listOfPins = new ArrayList<Pin>();
		if (AppCommons.verifySession(session) == true) {
		if(session != null)
		{
	
		Integer  user_id = (Integer)session.getAttribute("session_user_id");
	
		if (user_id != null) {
				
				
			List<User> listSamples = new ArrayList<User>();
			
			skipMe = (skipMe!=null)?skipMe:false;
			skipDisabled = (skipDisabled!=null)? skipDisabled:false;
			listSamples = userDataController.getAllUsers(user_id,skipMe,skipDisabled);
			
	 			
			response.setStatus(ACK);
			response.setMsg("Successfully got "+listSamples.size()+" users !");
			
			response.setData(listSamples);
			
			return response;
							  }
		}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		return response;
	}
	
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/getusercount", method = RequestMethod.GET, produces = "application/json")
	public Response<User> getUserCount(Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();
		// List<User> mUsersList = UserDataController.getAllUsers(user);
	//	List<Pin> listOfPins = new ArrayList<Pin>();
		if (AppCommons.verifySession(session) == true) {
		if(session != null)
		{
	
		Integer  user_id = (Integer)session.getAttribute("session_user_id");
	
		if (user_id != null) {
				
				
			User userdata = new User();
			
			userdata = userDataController.getSystemUserStats();
			
			response.setData(userdata);
			response.setStatus(ACK);
			response.setMsg("Successfully got user stat !");
			
		//	response.setData(listSamples);
			
			return response;
							  }
		}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		return response;
	}
	
	
	
	
	/**
	 * Gets the favourites.
	 *
	 * @param principal the principal
	 * @param session the session
	 * @return the favourites
	 */
	@CrossOrigin
	@RequestMapping(value = "/getfavourites", method = RequestMethod.GET, produces = "application/json")
	public Response2<User> getFavourites(Principal principal, HttpSession session) {
		Response2<User> response = new Response2<User>();
		// List<User> mUsersList = UserDataController.getAllUsers(user);
		
		if (AppCommons.verifySession(session) == true) {
		if(session != null)
		{
	
			Integer  user_id = (Integer)session.getAttribute("session_user_id");
			
			if (user_id != null) {
				
			List<User> listSamples = new ArrayList<User>();
			
			listSamples = userDataController.getFavourites(user_id);
			
			response.setStatus(ACK);
			response.setMsg("Successfully got all favourite users !");
			
			response.setData(listSamples);
			
			return response;
							  }
		}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS+"");
		}
		return response;
	}
	
	
	/**
	 * Update user profiel.
	 *
	 * @param user the user
	 * @return the response
	 */
	@CrossOrigin
	@RequestMapping(value = "/addemployee", method = RequestMethod.POST, produces = "application/json")
	public Response<User> updateUserProfiel(@RequestBody User user) {
		Response<User> response = new Response<User>();	
		
		if (user != null) 
		{
			
				user.setPassword(AppCommons.defaultPassword);
				user = userDataController.registerUser(user);
			if(user!=null)
			{
				
				response.setStatus(ACK);
				response.setMsg("Register Success ! Need to activate the user from the admin panel");
				response.setData(user);
			}
			else {
				response.setStatus(NACK);
				response.setMsg("User not added!");
				response.setData(user);
			}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg("No user data found!");
			response.setData(null);
			
		}
				
			
			
			
		return response;
	}
	
	/**
	 * Activate deactivate user.
	 *
	 * @param id the id
	 * @param action the action
	 * @return true, if successful
	 */
	public boolean activateDeactivateUser(Integer id,String action)
	{
		boolean resp = false;
		
		if(id!=null && action !=null )
		{
			if(action.equalsIgnoreCase("activate"))
			{
				userDataController.activateUser(id);
				
			}
			else 
			{
				userDataController.deactivateUser(id);
			}
		}
		
		return resp;
		
		
	}
	
	
	/**
	 * TODO : Add the Principal object to verify sessions against the user
	 * names.
	 *
	 * @param user the user
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	@CrossOrigin
	@RequestMapping(value = "/updateuserprofile", method = RequestMethod.POST, produces = "application/json")
	public Response<User> updateUserProfile(@RequestBody User user, Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();

	//	user.setUsername(principal.getName());
		user = userDataController.updateUserProfile(user);

		if (user != null) {
			response.setStatus(ACK);
			response.setMsg(" user profile update successful !");
			//response.setData(user);
		} else {
			response.setStatus(NACK);
			response.setMsg(" user profile update failed !");
		//	response.setData(user);
		}
		return response;
	}

	
	
	
	/**
	 *  Deactivate Employee.
	 *
	 * @param user the user
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	@CrossOrigin
	@RequestMapping(value = "/deactivateemployee", method = RequestMethod.POST, produces = "application/json")
	public Response<User> deactivateEmployee(@RequestBody User user, Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();

		boolean check;
	//	user.setUsername(principal.getName());
		check = userDataController.deactivateUser(user.getId());

		if (check) {
			response.setStatus(ACK);
			response.setMsg(" User Deactivated !");
			//response.setData(user);
		} else {
			response.setStatus(NACK);
			response.setMsg(" Failed  !");
		//	response.setData(user);
		}
		return response;
	}
	
	
	/**
	 *  Activate Employee.
	 *
	 * @param user the user
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	@CrossOrigin
	@RequestMapping(value = "/activateemployee", method = RequestMethod.POST, produces = "application/json")
	public Response<User> activateEmployee(@RequestBody User user, Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();

		boolean check;
	//	user.setUsername(principal.getName());
		check = userDataController.activateUser(user.getId());

		if (check) {
			response.setStatus(ACK);
			response.setMsg(" User Activated !");
			//response.setData(user);
		} else {
			response.setStatus(NACK);
			response.setMsg(" Failed  !");
		//	response.setData(user);
		}
		return response;
	}
	
	/**
	 * Upload profile image.
	 *
	 * @param user the user
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	@RequestMapping(value = "/uploadprofileimage", method = RequestMethod.POST, produces = "application/json",consumes="application/json")
	public Response<User> uploadProfileImage(@RequestBody User user, Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();
		//log.info("Base64string : "+user.getBase64String());		
		if (AppCommons.verifySession(session) == true) {
			String username = session.getAttribute("username").toString();
			user.setUsername(username);
			if (username != null)				
				user = userDataController.uploadProfileImage(user);				
			if (user != null) {
				String userName = userDataController.getUserProfileById(user.getId()).getUsername();
				user.setUsername(userName);
				user = userDataController.updateUserProfileImage(user);
				response.setStatus(ACK);
				response.setData(user);;
				response.setMsg("Profile image uploaded successfully");
			} else {
				response.setStatus(NACK);
				response.setMsg("Profile image upload was unsuccessful");
				// response.setData(pin);
			}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS.toString());	
		}		
		return response;

	}
	
	/**
	 * Update user password.
	 *
	 * @param user the user
	 * @param principal the principal
	 * @param session the session
	 * @return the response
	 */
	@RequestMapping(value = "/updateuserpassword", method = RequestMethod.POST, produces = "application/json",consumes="application/json")
	public Response<User> updateUserPassword(@RequestBody User user, Principal principal, HttpSession session) {
		Response<User> response = new Response<User>();
		//log.info("Base64string : "+user.getBase64String());		
		if (AppCommons.verifySession(session) == true) {
			String username = session.getAttribute("username").toString();
			user.setUsername(username);
			if (username != null)
				user = userDataController.updatePassword(user);				
			if (user != null) {
				//user = userDataController.updateUserProfileImage(user);
				response.setStatus(ACK);
				response.setData(user);;
				response.setMsg("Password changed successfully");
			} else {
				response.setStatus(NACK);
				response.setMsg("Password change was unsuccessful");
				// response.setData(pin);
			}
		}
		else
		{
			response.setStatus(NACK);
			response.setMsg(AppAuthCodes.UNAUTHORIZED_ACCESS.toString());	
		}		
		return response;

	}



}
