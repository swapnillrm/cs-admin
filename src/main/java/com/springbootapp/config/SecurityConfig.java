package com.springbootapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

@Configuration
@EnableWebSecurity
/*@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)*/
public class SecurityConfig extends WebSecurityConfigurerAdapter {	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
    /*@Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.inMemoryAuthentication()
            .withUser("user").password("password").roles("USER");
    	JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");
    	  builder.jdbcAuthentication().dataSource(jdbcTemplate.getDataSource())
  		.usersByUsernameQuery(
  			"select username,password, enabled from users where username=?")
  		.authoritiesByUsernameQuery(
  			"select userid, role from user_roles where userid in (select id from users where username=?)");    	
    }*/

	
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
    	/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
  	  builder.jdbcAuthentication().dataSource(jdbcTemplate.getDataSource())
		.usersByUsernameQuery(
			"select username, password, enabled from users where username=?")	
		.passwordEncoder(passwordEncoder)
		.authoritiesByUsernameQuery(
			"select userid, role from user_roles where userid in (select id from users where username=?)");
        /*auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");*/
    }
    
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http     
        .addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class)        
            .authorizeRequests()
            .antMatchers("/openservices/**/**").permitAll()
            .antMatchers("/error").permitAll()
            .antMatchers("/auth/**").permitAll()                        
            .antMatchers("/sample/**").access("hasRole('ROLE_ADMIN')")
            .anyRequest().authenticated()
            .and()
            .requestCache()
            .requestCache(new NullRequestCache())
            .and()
            .httpBasic();
        
        http.csrf().disable();
        //.csrfTokenRepository(csrfTokenRepository());
        
        
        
        
    }
	
    @Bean
    public HttpSessionStrategy httpSessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }    
	
    private CsrfTokenRepository csrfTokenRepository() 
    { 
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository(); 
        repository.setSessionAttributeName("_csrf");
        return repository; 
    }
}
