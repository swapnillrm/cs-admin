package com.springbootapp.config;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class GlobalConfigs {
	private static final Logger log = LoggerFactory.getLogger(GlobalConfigs.class);

	private static ApplicationContext ctx = null;

	protected GlobalConfigs() {

	}

	public static ApplicationContext getInstance() {
		if (ctx == null) {
			ctx = new AnnotationConfigApplicationContext(BeanConfiguration.class);
			log.info("Creating the AnnotationConfigApplicationContext");
		}
		return ctx;
	}
}