package com.springbootapp.config;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project")
public class UserController {

    @RequestMapping("/users")
    public String authorized() {
        return "Hello Secured World";
    }
}
