package com.springbootapp.config;

import java.util.Locale;
import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.springbootapp.model.Company;

import com.springbootapp.model.TableInfo;

import com.springbootapp.model.User;

@Configuration
public class BeanConfiguration {
	
	@Bean(name = "userBean")
	@Scope("prototype")
	public User getUser() {
		return new User();
	}


	@Bean(name = "companyBean")
	@Scope("prototype")
	public Company getCompany() {
		return new Company();
	}

	@Bean(name = "emailerBean")
	@Scope("prototype")
	public JavaMailSender javaMailSender() {
		   JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		   Properties mailProperties = new Properties();
		   mailProperties.put("mail.smtp.auth", true);
		   mailProperties.put("mail.smtp.starttls.enable", true);
		   mailProperties.put("mail.smtp.starttls.required", true);
		   mailSender.setJavaMailProperties(mailProperties);
		   mailSender.setHost("smtp.gmail.com");
		   mailSender.setPort(587);
		   mailSender.setProtocol("smtp");
		   mailSender.setUsername("aptamogh@gmail.com");	
		   mailSender.setPassword("Apte@1234");
		  
		   return mailSender;
		}
	
	@Bean
	@Scope("prototype")
	public LocaleResolver localeResolver() {
	    SessionLocaleResolver slr = new SessionLocaleResolver();
	    slr.setDefaultLocale(Locale.US);
	    return slr;
	}
	
	@Bean
	@Scope("prototype")
	public LocaleChangeInterceptor localeChangeInterceptor() {
	    LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
	    lci.setParamName("lang");
	    return lci;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
	@Bean(name = "tableInfoBean")
	@Scope("prototype")
	public TableInfo getTableInfo() {
		return new TableInfo();
	}
}
