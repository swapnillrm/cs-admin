package com.springbootapp.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.transaction.PlatformTransactionManager;

@EnableJdbcHttpSession(maxInactiveIntervalInSeconds=10000)
public class HttpSessionConfig {
	
	@Autowired
	DataSource dataSource;
	
	/*@Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .addScript("org/springframework/session/jdbc/schema-mysql.sql")
                .build();
    }
*/
    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {    	
        return new DataSourceTransactionManager(dataSource);
    }

}
