package com.springbootapp.config;
/*
import java.io.IOException;

import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

import org.springframework.session.ExpiringSession;*/

import org.springframework.context.annotation.EnableAspectJAutoProxy;


/*@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 2400)*/

public class EmbeddedRedisConfiguration {
	
    /*private static RedisServer redisServer;*/
    
    //C:\Users\lrm_dev\Downloads\redis-2.8.17
    
	/** NOTE : How to add the session timeout in the redis server
	 * Adding the session timeout in the redis server by using , the timeout is defined in seconds
	 * @EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60)
	 * @return
	 */
	
   /* @Bean
    public static ConfigureRedisAction configureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }

    @Bean
    public JedisConnectionFactory connectionFactory() throws IOException {
        redisServer = new RedisServer(Protocol.DEFAULT_PORT);            
        redisServer.start();
        //return new JedisConnectionFactory();
    	JedisConnectionFactory connection = new JedisConnectionFactory();
        connection.setPort(6379);
        connection.setHostName("localhost");
        connection.setPassword("password");
        return connection;
    }

    @PreDestroy
    public void destroy() {
        redisServer.stop();
    }
*/}
