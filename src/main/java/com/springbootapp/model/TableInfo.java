package com.springbootapp.model;

import java.math.BigInteger;

import org.springframework.stereotype.Component;

@Component
public class TableInfo {
	
	/**
	 * select column_name, data_type, character_maximum_length from
	 * information_schema.columns where table_name = 'myTable'
	 */
	private String columnName;
	private String dataType;
	private BigInteger characterMaximumLength;
	private String tableName;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}


	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public BigInteger getCharacterMaximumLength() {
		return characterMaximumLength;
	}

	public void setCharacterMaximumLength(BigInteger characterMaximumLength) {
		this.characterMaximumLength = characterMaximumLength;
	}

}
