package com.springbootapp.model;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.springbootapp.datacontrollers.UserDataController;

@Component
public class AppLoginListener implements ApplicationListener<AuthenticationSuccessEvent> {

	private static final Logger log = LoggerFactory.getLogger(AppLoginListener.class);

	@Autowired
	HttpSession httpSession;
	

	@Autowired
	UserDataController userDataController;
	
	@Autowired
	User user;
	

	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		// TODO Auto-generated method stub		
		
		String userName = ((UserDetails)event.getAuthentication().getPrincipal()).getUsername();	
		log.info("AppLoginListener : onApplicationEvent : !!!!!!Auth successful!!!!!!!");
		//httpSession.setMaxInactiveInterval(100);
		log.info("My session id : " + httpSession.getId());
		httpSession.setAttribute("username", userName);
		
		user = userDataController.getMyProfile(userName);
		if(user!=null)
		{
			httpSession.setAttribute("session_user_id", user.getId());
		}

	}

}
