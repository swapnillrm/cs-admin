package com.springbootapp.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Configuration {

	private int id;
	private boolean multipleManagerAllowed;

	private String configParam;
	private String description;
	private String configValue;
	private String createdDate;

	public String getConfigParam() {
		return configParam;
	}

	public void setConfigParam(String configParam) {
		this.configParam = configParam;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isMultipleManagerAllowed() {
		return multipleManagerAllowed;
	}

	public void setMultipleManagerAllowed(boolean multipleManagerAllowed) {
		this.multipleManagerAllowed = multipleManagerAllowed;
	}

}
