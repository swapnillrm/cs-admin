package com.springbootapp.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * System Configuration and Settings to set configuration and settings details
 * @author leftrightmind
 *
 */

@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SystemConfiguration {

	private String global_emails;
	private String global_notifications;
	
	/**
	 * @return the global_emails
	 */
	public String getGlobal_emails() {
		return global_emails;
	}
	/**
	 * @param global_emails the global_emails to set
	 */
	public void setGlobal_emails(String global_emails) {
		this.global_emails = global_emails;
	}
	/**
	 * @return the global_notifications
	 */
	public String getGlobal_notifications() {
		return global_notifications;
	}
	/**
	 * @param global_notifications the global_notifications to set
	 */
	public void setGlobal_notifications(String global_notifications) {
		this.global_notifications = global_notifications;
	}
	
	
	
	

}
