package com.springbootapp.model;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class Company {
	private int com_id;
	private String com_name;
	private String email;
	private String phone;
	private String com_logo;
	private String contact_person;
	private String alt_email;
	private String location;
	private String com_baseurl;
	
	private String status;
	private String msg;
	
	private ArrayList<String> com_domains;
	private String username;
	private String com_login_domain;
	public Object companyDataController;
	public String com_domains_list;
	public String com_website;
	
	
	private String license_key;
	private String activation_key;
	private String token;
	public String doa;
	public String dod;
	
	/**
	 * @return the com_id
	 */
	public Integer getCom_id() {
		return com_id;
	}

	/**
	 * @param com_id 
	 */
	
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCom_name() {
		return com_name;
	}
	
	public void setCom_name(String com_name) {
		this.com_name = com_name;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setCom_logo(String com_logo) {
		this.com_logo = com_logo;
	}
	
	public String getCom_logo() {
		return com_logo;
	}	
	
	public void setCom_website(String com_website) {
		this.com_website = com_website;
	}
	
	public String getCom_website() {
		return com_website;
	}	
	
	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}
	
	public String getContact_person() {
		return contact_person;
	}	
	
	
	public void setAlt_email(String alt_email) {
		this.alt_email = alt_email;
	}
	
	public String getAlt_email() {
		return alt_email;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getLocation() {
		return location;
	}
	
	
	public void setCom_baseurl(String com_baseurl) {
		this.com_baseurl = com_baseurl;
	}
	
	public String getCom_baseurl() {
		return com_baseurl;
	}
	
	public void setCom_login_domain(String com_login_domain) {
		this.com_login_domain = com_login_domain;
	}
	
	public String getCom_login_domain() {
		return com_login_domain;
	}
		
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public ArrayList<String> getCom_domains() {
		return com_domains;
	}

	public void setCom_domains(ArrayList<String> com_domains) {
		this.com_domains = com_domains;
	}
	
	public String getCom_domains_list() {
		return com_domains_list;
	}

	public void setCom_domains_list(String com_domains_list) {
		this.com_domains_list = com_domains_list;
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getLicense_key() {
		return license_key;
	}
	
	public void setLicense_key(String license_key) {
		this.license_key = license_key;
	}
	
	public String getActivation_key() {
		return activation_key;
	}
	
	public void setActivation_key(String activation_key) {
		this.activation_key = activation_key;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	
	/**
	 * @return the Date of Activation
	 */
	
	public String getDoa() {
		return doa;
	}
	
	public void setDoa(String doa) {
		this.doa = doa;
	}
	
	
	/**
	 * @return the Date of DeActivation
	 */
	
	public String getDod() {
		return dod;
	}
	
	public void setDod(String dod) {
		this.dod = dod;
	}

	@Override
	public String toString() {
		return "Company [com_id=" + com_id + ", com_name=" + com_name + ", email=" + email + ", status=" + status
				+ ", msg=" + msg + ", com_domains=" + com_domains + "]";
	}
	
	

}
