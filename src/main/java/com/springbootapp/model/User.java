package com.springbootapp.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

	private int id;
	private String username;
	private String email;
	private String profile_image;
	private String authstring;
	private String employee_id;
	private String firstname;
	private String lastname;
	private String location;
	private String department;
	private String role;
	private boolean enabled;
	private Integer role_id;
	
	private String oldPassword;
	private String rel_role;
	private String rel_role_description;
	private String password;

	/**
	 * Added for profileImage upload to aws
	 */
	private String filePath;
	private String fileType;
	private String base64String;
	private Boolean status;
	private String msg;

	private String fileName;
	
	private String activationFpwdKey;
	
	
	private Integer activeUserCount;
	private Integer totalUserCount;
	private Integer deactiveUserCount;
	

	/**
	 * @return the activeUserCount
	 */
	public Integer getActiveUserCount() {
		return activeUserCount;
	}

	/**
	 * @param activeUserCount the activeUserCount to set
	 */
	public void setActiveUserCount(Integer activeUserCount) {
		this.activeUserCount = activeUserCount;
	}

	/**
	 * @return the totalUserCount
	 */
	public Integer getTotalUserCount() {
		return totalUserCount;
	}

	/**
	 * @param totalUserCount the totalUserCount to set
	 */
	public void setTotalUserCount(Integer totalUserCount) {
		this.totalUserCount = totalUserCount;
	}

	/**
	 * @return the deactiveUserCount
	 */
	public Integer getDeactiveUserCount() {
		return deactiveUserCount;
	}

	/**
	 * @param deactiveUserCount the deactiveUserCount to set
	 */
	public void setDeactiveUserCount(Integer deactiveUserCount) {
		this.deactiveUserCount = deactiveUserCount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getBase64String() {
		return base64String;
	}

	public void setBase64String(String base64String) {
		this.base64String = base64String;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfile_image() {
		return profile_image;
	}

	public void setProfile_image(String profile_image) {
		this.profile_image = profile_image;
	}

	public String getAuthstring() {
		return authstring;
	}

	public void setAuthstring(String authstring) {
		this.authstring = authstring;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	/*public boolean isIs_active() {
		return enabled;
	}

	public void setIs_active(boolean is_active) {
		this.enabled = is_active;
	}*/

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRel_role() {
		return rel_role;
	}

	public void setRel_role(String user_relation) {
		this.rel_role = user_relation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the role_id
	 */
	public Integer getRole_id() {
		return role_id;
	}

	/**
	 * @param role_id the role_id to set
	 */
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	/**
	 * @return the rel_role_description
	 */
	public String getRel_role_description() {
		return rel_role_description;
	}

	/**
	 * @param rel_role_description the rel_role_description to set
	 */
	public void setRel_role_description(String rel_role_description) {
		this.rel_role_description = rel_role_description;
	}

	public String getActivationFpwdKey() {
		return activationFpwdKey;
	}

	public void setActivationFpwdKey(String activationFpwdKey) {
		this.activationFpwdKey = activationFpwdKey;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

}
