package com.springbootapp.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement(name = "response")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> implements Serializable {	

	private static final long serialVersionUID = 1L;
	private String status = "";
	private String msg = "";
	private String session = null;
	private T data;
	private List<T> dataList;
	
	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@XmlElement(name = "msg")
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	@XmlElement(name = "data")
	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}

	@XmlElement(name = "session")
	public String getSession() {
		return session;
	}
	
	public void setSession(String session) {
		this.session = session;
	}

	public List<T> getDataList() {
		return dataList;
	}
	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}	
}
