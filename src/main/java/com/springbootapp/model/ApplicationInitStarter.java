package com.springbootapp.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.springbootapp.datacontrollers.SystemConfigurationDataController;

/**
 * This Model class can be used to initialize the variables when the application starts
 * 
 *
 */
@Component
public class ApplicationInitStarter implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger log = LoggerFactory.getLogger(ApplicationInitStarter.class);

	@Autowired
	SystemConfigurationDataController systemConfigurationDataController;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		// TODO Auto-generated method stub
		log.info("!!!Congrats!!!You app started successfully!!!");
		systemConfigurationDataController.loadConfigMap();
		log.info("*******Your Config map loaded successfully********");

	}

}
