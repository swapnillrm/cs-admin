package com.springbootapp.model;

public interface MessageByLocaleService {

    public String getMessage(String id);
}