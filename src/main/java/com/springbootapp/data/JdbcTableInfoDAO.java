package com.springbootapp.data;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springbootapp.config.GlobalConfigs;
import com.springbootapp.model.Sample;
import com.springbootapp.model.TableInfo;

@Component
public class JdbcTableInfoDAO {
	/**
	 * select column_name, data_type, character_maximum_length from
	 * information_schema.columns where table_name = 'myTable'
	 */
	
	@Autowired
	TableInfo tableInfo;
	
	@Autowired
	CommonJdbc commonJdbc;
	
	
	private static final Logger log = LoggerFactory.getLogger(JdbcTableInfoDAO.class);
	
	public Map<String,TableInfo> getTableInfoByTableName(String tableName)
	{
		String selectTableDataQuery = String.format("select column_name, data_type, character_maximum_length from information_schema.columns where table_name = '%s'",tableName);
 		log.info(selectTableDataQuery);
 		List<Map<String, Object>> totalMap = commonJdbc.queryForList(selectTableDataQuery); 		
 		Map<String,TableInfo> listMap = new HashMap<String,TableInfo>();
 		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			String columnName = (String) map.get("column_name");
			String dataType = (String) map.get("data_type");
			BigInteger charMaxLimit = (BigInteger) map.get("character_maximum_length");
			tableInfo = (TableInfo) GlobalConfigs.getInstance().getBean("tableInfoBean");
			tableInfo.setColumnName(columnName);
			tableInfo.setDataType(dataType);
			tableInfo.setCharacterMaximumLength(charMaxLimit);
			listMap.put(columnName, tableInfo);
			//list.add(tableInfo);
		}
 		return listMap;
	}

}
