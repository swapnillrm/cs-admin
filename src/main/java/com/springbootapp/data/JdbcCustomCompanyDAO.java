package com.springbootapp.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springbootapp.config.GlobalConfigs;

import com.springbootapp.model.Company;
import com.springbootapp.model.User;
import com.springbootapp.utils.AppException;

// TODO: Auto-generated Javadoc
/**
 * The Class JdbcCustomUserDAO.
 */
@Component
public class JdbcCustomCompanyDAO implements CustomCompanyDAO {

	@Autowired
	CommonJdbc commnsJdbc;
	
	/** The user. */
	@Autowired
	Company company;
	
	public Company addCompany(Company company) throws AppException {
		// TODO Auto-generated method stub
		String selectUserId = String.format("select count(*) from company where com_name like binary '%s'", company.getCom_name());
		
		Company company_data = new Company();
		
		if (commnsJdbc.selectInteger(selectUserId) == 0) {
			
			String query = null;
			Integer lastinsertedid = null;
			
			//log.info("company size is " + company.toString());
			//log.info("list of computer domains " + company.getCom_domains().toString());
					
			query = String.format(
					"insert into company(com_name, com_logo, phone, email, com_website, contact_person, alt_email, location, com_baseurl) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
					company.getCom_name(), company.getCom_logo(), company.getPhone(), company.getEmail(), company.getCom_website(), company.getContact_person(), company.getAlt_email(), company.getLocation(), company.getCom_baseurl());
			
			
				commnsJdbc.update(query);
				
				lastinsertedid = commnsJdbc.selectInteger("SELECT LAST_INSERT_ID()");
				
				String insert_domains = String.format("insert into company_domains(com_id, com_login_domain) values ('%s','%s')", lastinsertedid,company.getCom_domains_list());
					
				commnsJdbc.update(insert_domains);
					
					String get_result = "SELECT * FROM company INNER JOIN company_domains ON company.com_id = company_domains.com_id WHERE company.com_id = " + lastinsertedid;			
					
					
					
					List<Map<String, Object>> totalMap = commnsJdbc.queryForList(get_result);
					
					for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
						Map<String, Object> map = iterator.next();
						// user = (User) GlobalConfigs.getInstance().getBean("userBean");
						company_data = (Company) GlobalConfigs.getInstance().getBean("companyBean");
						company_data.setCom_id((int) map.get("com_id"));
						company_data.setCom_name((String) map.get("com_name"));
						company_data.setCom_logo((String) map.get("com_logo"));
						company_data.setPhone((String) map.get("phone"));
						company_data.setEmail((String) map.get("email"));
						company_data.setCom_website((String) map.get("com_website"));
						company_data.setContact_person((String) map.get("contact_person"));
						company_data.setAlt_email((String) map.get("alt_email"));
						company_data.setLocation((String) map.get("location"));
						company_data.setCom_baseurl((String) map.get("com_baseurl"));
						company_data.setCom_domains_list((String) map.get("com_login_domain"));
						company_data.setLicense_key((String) map.get("license_key"));
						company_data.setActivation_key((String) map.get("activation_key"));
						company_data.setToken((String) map.get("token"));
						company_data.setStatus((String) map.get("status"));
			
						}
					
					
			} else {
				
				company_data = null;
			}

		return company_data;
		
	}


	public List<Company> getAllCompanies() {
		// TODO Auto-generated method stub
				//String query = "select * from company";
				//String query = "SELECT company.com_name, company.com_logo, company_domains.com_login_domain FROM company INNER JOIN company_domains ON company.com_id = company_domains.com_id ORDER BY company.com_name";
				String query = "SELECT * FROM company INNER JOIN company_domains ON company.com_id = company_domains.com_id";
				List<Company> list = new ArrayList<Company>();
				Company company_instance = new Company();
				List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
				for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
					Map<String, Object> map = iterator.next();
					company_instance = (Company) GlobalConfigs.getInstance().getBean("companyBean");
					company_instance.setCom_id((int) map.get("com_id"));
					company_instance.setCom_name((String) map.get("com_name"));
					company_instance.setCom_logo((String) map.get("com_logo"));
					company_instance.setPhone((String) map.get("phone"));
					company_instance.setEmail((String) map.get("email"));
					company_instance.setContact_person((String) map.get("contact_person"));
					company_instance.setCom_website((String) map.get("com_website"));
					company_instance.setAlt_email((String) map.get("alt_email"));
					company_instance.setLocation((String) map.get("location"));
					company_instance.setCom_baseurl((String) map.get("com_baseurl"));
					company_instance.setCom_domains_list((String) map.get("com_login_domain"));
					company_instance.setLicense_key((String) map.get("license_key"));
					company_instance.setActivation_key((String) map.get("activation_key"));
					company_instance.setToken((String) map.get("token"));
					company_instance.setStatus((String) map.get("status"));
					list.add(company_instance);
				}
				
		return list;
	}

	
	public List<Company> getComdomains(Company company) {
		// TODO Auto-generated method stub
			String query = "select com_id,com_login_domain from company_domains where com_id ="+ company.getCom_id();				
				List<Company> list = new ArrayList<Company>();
				Company company_instance = new Company();
				List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
				for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
					Map<String, Object> map = iterator.next();
					company_instance = (Company) GlobalConfigs.getInstance().getBean("companyBean");
					company_instance.setCom_id((int) map.get("com_id"));
					company_instance.setCom_login_domain((String) map.get("com_login_domain"));
					list.add(company_instance);
				}
				
		return list;
	}
	

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomCompanyDAO#updateCompanyProfile(com.springbootapp.model.Company)
	 */
	@Override
	public Integer updateCompanyProfile(Company company) throws AppException {
		
		// TODO Auto-generated method stub
		/*
		 * String updateProjectRatingsQuery = String.format(
		 * "update company set email='%s' where com_name like binary '%s'",
		 * company.getEmail(), company.getCom_name()); if
		 * (commnsJdbc.update(updateProjectRatingsQuery) == 1) return true;
		 * return false;
		 */

		String query = "update company set com_name = '" + company.getCom_name() + "', com_logo = '" + company.getCom_logo() + "', email = '" + company.getEmail() + "', phone = '" + company.getPhone() + "', com_website = '" + company.getCom_website() + "', contact_person = '" + company.getContact_person() + "', alt_email = '" + company.getAlt_email() + "', location = '" + company.getLocation() + "', com_baseurl = '" + company.getCom_baseurl() + "' where com_id = '" + company.getCom_id() + "'";
		Integer lastinsertedid = null;
		if (commnsJdbc.update(query) == 1) {
			lastinsertedid = commnsJdbc.selectInteger("SELECT LAST_INSERT_ID()");
			//String update_domains = "update company_domains set com_login_domain = '" + company.getCom_domains_list() + "' where com_id = '" + company.getCom_id() + "'";
			return lastinsertedid;
		} else {
			return lastinsertedid;
		}

	}
	
	
	public Company updateCompanydomains(Company company) {
		// TODO Auto-generated method stub
		String query = "update company_domains set com_login_domain = '" + company.getCom_domains_list() + "' where com_id = '" + company.getCom_id() + "'";	
		
		if (commnsJdbc.update(query) == 1) {			
			return company;
		} else {
			return null;
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomCompanyDAO#deleteCompanyProfile(com.springbootapp.model.Company)
	 */
	@Override
	public boolean deleteCompanyProfile(Company company) throws AppException {
		
		// TODO Auto-generated method stub
		/*
		 * String updateProjectRatingsQuery = String.format(
		 * "update company set email='%s' where com_name like binary '%s'",
		 * company.getEmail(), company.getCom_name()); if
		 * (commnsJdbc.update(updateProjectRatingsQuery) == 1) return true;
		 * return false;
		 */

		String query = "delete from company where com_id = '" + company.getCom_id() + "'";

		if (commnsJdbc.update(query) == 1) {
			return true;
		} else {
			return false;
		}

	}


	public boolean activateCompany(Company company) {
		// TODO Auto-generated method stub
		//String query = "UPDATE 'company' SET `status` = 'Active' WHERE com_id = " + com_id;
		String license_key = company.getLicense_key();
		String activation_key = company.getActivation_key();
		String token = company.getToken();
		String doa = company.getDoa();	
		
		String query = "UPDATE `company` SET `status`= 'Activate', `license_key`= '"+license_key+"',`activation_key`= '"+activation_key+"', `token`= '"+token+"', `date_of_activation`= '"+doa+"' WHERE `com_id` =" + company.getCom_id();
		int check = commnsJdbc.update(query);
		if (check == 1)
			return true;
		else
			return false;
	}
	
	public boolean deactivateCompany(Company company) {
		
		// TODO Auto-generated method stub
		String license_key = company.getLicense_key();
		String activation_key = company.getActivation_key();
		String token = company.getToken();		
		String dod = company.getDod();
		
		String query = "UPDATE `company` SET `status`= 'Deactivate', `license_key`= '"+license_key+"',`activation_key`= '"+activation_key+"', `token`= '"+token+"', `date_of_deactivation`= '"+dod+"' WHERE `com_id` =" + company.getCom_id();
		int check = commnsJdbc.update(query);
		if (check == 1)
			return true;
		else
			return false;
	}



	@Override
	public Company getCompanyProfileById(Company company) {
			
			//String query = "select * from company where com_id = " + company.getCom_id();
		
		String query = "SELECT * FROM company INNER JOIN company_domains ON company.com_id = company_domains.com_id WHERE company.com_id = " + company.getCom_id();
			
			String selectUserId = String.format("select count(*) from company where com_id like binary '%s'",
					company.getCom_id());	
			
			//commnsJdbc.update(query);
			Company company_data = new Company();
			
			if (commnsJdbc.selectInteger(selectUserId) == 1) {
				
				List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);	
				for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
					Map<String, Object> map = iterator.next();
					// user = (User) GlobalConfigs.getInstance().getBean("userBean");
					company_data = (Company) GlobalConfigs.getInstance().getBean("companyBean");
					company_data.setCom_id((int) map.get("com_id"));
					company_data.setCom_name((String) map.get("com_name"));
					company_data.setCom_logo((String) map.get("com_logo"));
					company_data.setPhone((String) map.get("phone"));
					company_data.setEmail((String) map.get("email"));
					company_data.setCom_website((String) map.get("com_website"));
					company_data.setContact_person((String) map.get("contact_person"));
					company_data.setAlt_email((String) map.get("alt_email"));
					company_data.setLocation((String) map.get("location"));
					company_data.setCom_baseurl((String) map.get("com_baseurl"));
					company_data.setCom_domains_list((String) map.get("com_login_domain"));
					company_data.setLicense_key((String) map.get("license_key"));
					company_data.setActivation_key((String) map.get("activation_key"));
					company_data.setToken((String) map.get("token"));
					company_data.setStatus((String) map.get("status"));
		
				}
				
				
			} else {
				company_data = null;
				
			}	
	
		return company_data;
	}
	
	
	@Override
	public Company identifyBaseUrl(Company company) {
		
		String finddomain = company.getCom_login_domain();
		String[] tokens = finddomain.split("@"); 
		String getdomain = tokens[1];		
		
		
			String query = "SELECT company_domains.com_id, company.com_baseurl, company.com_name FROM company_domains RIGHT JOIN company ON company_domains.com_id = company.com_id where company_domains.com_login_domain LIKE '%" + getdomain+"%'";
			
			String selectUserId = String.format("select count(*) from company_domains where com_login_domain like binary '%s'",
					"%"+getdomain+"%");	
			
			//commnsJdbc.update(query);
			Company company_data = new Company();
			
			if (commnsJdbc.selectInteger(selectUserId) != 0) {
				
				List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);	
				for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
					Map<String, Object> map = iterator.next();
					// user = (User) GlobalConfigs.getInstance().getBean("userBean");
					company_data = (Company) GlobalConfigs.getInstance().getBean("companyBean");
					company_data.setCom_id((int) map.get("com_id"));
					company_data.setCom_name((String) map.get("com_name"));
					company_data.setCom_baseurl((String) map.get("com_baseurl"));					
					company_data.setStatus((String) map.get("status"));
		
				}
				
				
			} else {
				company_data = null;
				
			}	
	
		return company_data;
	}



}
