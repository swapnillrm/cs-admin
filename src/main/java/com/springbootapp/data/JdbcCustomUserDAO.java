package com.springbootapp.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.springbootapp.config.ConfigParams;
import com.springbootapp.config.GlobalConfigs;

import com.springbootapp.model.User;
import com.springbootapp.utils.AppCommons;
import com.springbootapp.utils.AppException;

// TODO: Auto-generated Javadoc
/**
 * The Class JdbcCustomUserDAO.
 */
@Component
public class JdbcCustomUserDAO implements CustomUserDAO {

	/** The commns jdbc. */
	@Autowired
	CommonJdbc commnsJdbc;

	/** The user. */
	@Autowired
	User user;


	/** The password encoder. */
	@Autowired
	PasswordEncoder passwordEncoder;


	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(JdbcCustomUserDAO.class);

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#updateUserProfile(com.springbootapp.model.User)
	 */
	@Override
	public boolean updateUserProfile(User user) throws AppException {
		// TODO Auto-generated method stub
		/*
		 * String updateProjectRatingsQuery = String.format(
		 * "update users set location='%s' where username like binary '%s'",
		 * user.getLocation(), user.getUsername()); if
		 * (commnsJdbc.update(updateProjectRatingsQuery) == 1) return true;
		 * return false;
		 */

		String query = "update users set location= '" + user.getLocation() + "' , firstname='" + user.getFirstname()
				+ "' , lastname='" + user.getLastname() + "' , department='" + user.getDepartment()
				+ "' where username = '" + user.getUsername() + "'";

		if (commnsJdbc.update(query) == 1) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * This method creates the user role.
	 *
	 * @param user the user
	 * @return the integer
	 */

	@Override
	public Integer registerUser(User user) throws AppException {
		// TODO Auto-generated method stub
		String selectUserId = String.format("select count(*) from users where username like binary '%s'",
				user.getUsername());
		Integer ret = 0;
		/**
		 * Encode the password using passwordEncoder
		 */
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		if (commnsJdbc.selectInteger(selectUserId) == 0) {
			String createUser = null;
			if (user.getProfile_image() == null) {
				createUser = String.format(
						"insert into users(username, firstname, lastname, password, email, department, location, employee_id,created_date) values ('%s','%s','%s','%s','%s','%s','%s','%s',CURRENT_TIMESTAMP)",
						user.getUsername(), user.getFirstname(), user.getLastname(), user.getPassword(),
						user.getEmail(), user.getDepartment(), user.getLocation(), user.getEmployee_id());
			} else {
				createUser = String.format(
						"insert into users(username, firstname, lastname, password, email, department, location, employee_id,profile_image,created_date) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s',CURRENT_TIMESTAMP)",
						user.getUsername(), user.getFirstname(), user.getLastname(), user.getPassword(),
						user.getEmail(), user.getDepartment(), user.getLocation(), user.getEmployee_id(),
						user.getProfile_image());
			}

			// username does not exist , create the new user with requested
			// parameters
			if (commnsJdbc.update(createUser) == 1) { // create the roles table
														// entry as well
				selectUserId = String.format("select id from users where username like binary '%s'",
						user.getUsername());
				Integer intSelectUserId = commnsJdbc.selectInteger(selectUserId);
				//Disable the user once he is registered
				deactivateUser(intSelectUserId);
				//Disable the user once he is registered
				createUser = String.format("insert into user_roles(userid, role) values ('%s', '%s')", intSelectUserId,
						ConfigParams.ROLE_USER);
				if (commnsJdbc.update(createUser) == 1)
					ret = intSelectUserId;
				else
					ret = 0;
			} else {
				ret = 0;
			}

		}
		return ret;

	}

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#saveProfileImage(com.springbootapp.model.User)
	 */
	@Override
	public boolean saveProfileImage(User user) {
		// TODO Auto-generated method stub
		/*
		 * PreparedStatement pstmt = null; String str = user.getProfileImage();
		 * Blob blob = null; try { blob=
		 * commnsJdbc.jdbcTemplate.getDataSource().getConnection().createBlob();
		 * blob.setBytes(1, str.getBytes()); pstmt =
		 * commnsJdbc.jdbcTemplate.getDataSource().getConnection().
		 * prepareStatement(
		 * "update users set profile_image=? where username like binary ?");
		 * pstmt.setBlob(1, blob); pstmt.setString(2, user.getUsername());
		 * 
		 * int i = pstmt.executeUpdate();
		 * 
		 * if(i>0) { return true; } else { return false; } } catch (Exception e)
		 * { e.printStackTrace(); //throw e; }
		 */
		return false;
	}

	/**
	 * get all suers method saurabh.
	 *
	 * @param whoAmI the who am I
	 * @return the all users
	 */

	@Override
	public List<User> getAllUsers(Integer whoAmI,Boolean skipMe,Boolean skipDisabled) {
		// TODO Auto-generated method stub

		// ARJUN: making modifications in this. I am particularly preferring to
		// use this layer to get DAO
		String query = "select * from users";
		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// String name = (String) map.get("name");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("id"));
			if(!(skipMe && whoAmI.equals((int) map.get("id"))))
			{
				if(!(skipDisabled && ((boolean) map.get("enabled") ==false )))
				{
					
					
						user.setEmail((String) map.get("email"));
					user.setFirstname((String) map.get("firstname"));
					user.setLastname((String) map.get("lastname"));
					user.setLocation((String) map.get("location"));
					user.setDepartment((String) map.get("department"));
					user.setRole((String) map.get("role"));
							
					
					user.setEnabled((boolean) map.get("enabled"));
					user.setEmployee_id((String) map.get("employee_id"));
					user.setProfile_image((String) map.get("profile_image"));
					// user.setSampleString(name);
					list.add(user);
				}
			}
		}
		return list;
	}



	@Override
	public User getSystemUserStats() {
		// TODO Auto-generated method stub

		// ARJUN: making modifications in this. I am particularly preferring to
		// use this layer to get DAO
		String query = "select * from view_system_user_stats";
		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// String name = (String) map.get("name");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			
			user.setActiveUserCount(Integer.parseInt(""+map.get("active_count")));
			user.setDeactiveUserCount(Integer.parseInt(""+map.get("deactive_count")));
			user.setTotalUserCount(Integer.parseInt(""+ map.get("total_count")));
			// user.setSampleString(name);
			list.add(user);
		}
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#getFavourites(java.lang.Integer)
	 */
	public List<User> getFavourites(Integer user_id) {
		// TODO Auto-generated method stub

		// user = getMyProfile(username);

		// RelationShipMapping.

		// String query = "SELECT * FROM `users` INNER JOIN user_relationships
		// ON users.id = user_relationships.user2_id AND
		// user_relationships.user1_id ="+ user.getId() +" INNER JOIN
		// relationships ON relationships.id =
		// user_relationships.user1_rel_role";
		String query = "SELECT * from view_pins  WHERE pinner = " + user_id + " GROUP BY pinnee";
		// List<User> distinct_users = new ArrayList<User>();

		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// String name = (String) map.get("name");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("pinnee"));
			// user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("pinnee_firstname"));
			user.setEmployee_id((String) map.get("pinnee_employee_id"));
			user.setEmail((String) map.get("pinnee_email"));
			user.setLastname((String) map.get("pinnee_lastname"));
			user.setLocation((String) map.get("pinnee_location"));
			user.setDepartment((String) map.get("pinnee_department"));
			// user.setRole((String) map.get("role"));
			user.setEnabled((boolean) map.get("pinnee_enabled"));
			// user.setEmployee_id((String) map.get("employee_id"));
			user.setProfile_image((String) map.get("pinnee_profile_image"));
			
			// user.setSampleString(name);
			list.add(user);
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#getMyProfile(java.lang.String)
	 */
	@Override
	public User getMyProfile(String username) {
		// TODO Auto-generated method stub
		// String query = "select * from users where username = '"+username+"'";

		String query = "select * from users INNER JOIN user_roles ON user_roles.userid = users.id  where users.username = '"
				+ username + "' ";

		// SqlRowSet rowSet = commnsJdbc.queryForMyProfile(query);
		// user.setFirstname(rowSet.getString(2));

		List<Map<String, Object>> userMap = commnsJdbc.queryForList(query);

		for (Iterator<Map<String, Object>> iterator = userMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// user = (User) GlobalConfigs.getInstance().getBean("userBean");
			log.info("I am here");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("id"));
			user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("firstname"));
			user.setLastname((String) map.get("lastname"));
			user.setLocation((String) map.get("location"));
			user.setDepartment((String) map.get("department"));
			user.setRole((String) map.get("role"));
			user.setEnabled((boolean) map.get("enabled"));
			user.setProfile_image((String) map.get("profile_image"));
			user.setEmployee_id((String) map.get("employee_id"));

		}

		return user;
	}

	/**
	 * Checks if is user active.
	 *
	 * @param Id the id
	 * @return true, if is user active
	 */
	public boolean isUserActive(Integer Id) {
		// TODO Auto-generated method stub
		// String query = "select * from users where username = '"+username+"'";

		String query = "select enabled from users INNER JOIN user_roles ON user_roles.userid = users.id  where users.id = '"
				+ Id + "' ";
		boolean resp = false;
		// SqlRowSet rowSet = commnsJdbc.queryForMyProfile(query);
		// user.setFirstname(rowSet.getString(2));

		List<Map<String, Object>> userMap = commnsJdbc.queryForList(query);

		for (Iterator<Map<String, Object>> iterator = userMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// user = (User) GlobalConfigs.getInstance().getBean("userBean");
			resp = ((boolean) map.get("enabled"));

		}

		return resp;
	}

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#getUserProfileById(java.lang.Integer)
	 */
	@Override
	public User getUserProfileById(Integer user_id) {
		// TODO Auto-generated method stub
		// String query = "select * from users where username = '"+username+"'";

		String query = "select * from users INNER JOIN user_roles ON user_roles.userid = users.id  where users.id = '"
				+ user_id + "' ";

		// SqlRowSet rowSet = commnsJdbc.queryForMyProfile(query);
		// user.setFirstname(rowSet.getString(2));

		List<Map<String, Object>> userMap = commnsJdbc.queryForList(query);

		for (Iterator<Map<String, Object>> iterator = userMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("id"));
			user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("firstname"));
			user.setLastname((String) map.get("lastname"));
			user.setLocation((String) map.get("location"));
			user.setDepartment((String) map.get("department"));
			user.setRole((String) map.get("role"));
			
			user.setEnabled((boolean) map.get("enabled"));
			user.setEmployee_id((String) map.get("employee_id"));
			user.setProfile_image((String) map.get("profile_image"));

		}

		return user;
	}

	// Arjun : Trying to see if we can get user by the session id , so that web view can be activated.
	// Update : Cannot get user by session id , as session tables do not provide string data of user id instead they are blobs 
	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#isSessionValid(java.lang.String)
	 */
	// instead , we will validate all the session as invalid or valid.
	@Override
	public Boolean isSessionValid(String sessionToken) {
		// TODO Auto-generated method stub
		// String query = "select * from users where username = '"+username+"'";

		String query = "select * from spring_session_attributes INNER JOIN spring_session ON spring_session.SESSION_ID = spring_session_attributes.SESSION_ID  where spring_session_attributes.SESSION_ID = '"
				+ sessionToken + "' ";

		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		// SqlRowSet rowSet = commnsJdbc.queryForMyProfile(query);
		// user.setFirstname(rowSet.getString(2));

		int count =0;
		 for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			count++;
		};
		
		if(count >0)
			return true;
		else
			return false;
		
	}

	
	
	
	/**
	 * Activate user.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean activateUser(Integer id) {
		// TODO Auto-generated method stub
		String query = "update users set enabled = 1 where id = " + id;
		int check = commnsJdbc.update(query);
		if (check == 1)
			return true;
		else
			return false;
	}

	/**
	 * Deactivate user.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean deactivateUser(Integer id) {
		// TODO Auto-generated method stub
		String query = "update users set enabled = 0 where id = " + id;
		int check = commnsJdbc.update(query);
		if (check == 1)
			return true;
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#getAllActiveUsers()
	 */
	@Override
	public List<User> getAllActiveUsers() {
		// TODO Auto-generated method stub
		String query = "select * from users WHERE enabled = 1";
		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// String name = (String) map.get("name");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("id"));
			user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("firstname"));
			user.setLastname((String) map.get("lastname"));
			user.setLocation((String) map.get("location"));
			user.setDepartment((String) map.get("department"));
			user.setRole((String) map.get("role"));
			user.setEnabled((boolean) map.get("enabled"));
			user.setEmployee_id((String) map.get("employee_id"));
			user.setProfile_image((String) map.get("profile_image"));
			// user.setSampleString(name);
			list.add(user);
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.springbootapp.data.CustomUserDAO#getAllInactiveUsers()
	 */
	@Override
	public List<User> getAllInactiveUsers() {
		// TODO Auto-generated method stub
		String query = "select * from users WHERE enabled = 0";
		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// String name = (String) map.get("name");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("id"));
			user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("firstname"));
			user.setLastname((String) map.get("lastname"));
			user.setLocation((String) map.get("location"));
			user.setDepartment((String) map.get("department"));
			user.setRole((String) map.get("role"));
			user.setEnabled((boolean) map.get("enabled"));
			user.setEmployee_id((String) map.get("employee_id"));
			user.setProfile_image((String) map.get("profile_image"));
			// user.setSampleString(name);
			list.add(user);
		}
		return list;
	}

	/**
	 * Update user profile image.
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	public boolean updateUserProfileImage(User user) throws AppException {
		// TODO Auto-generated method stub

		String query = "update users set profile_image= '" + user.getFilePath() + "' where username = '"
				+ user.getUsername() + "'";

		if (commnsJdbc.update(query) == 1) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Update token.
	 *
	 * @param updateUserLoginQuery the update user login query
	 * @return true, if successful
	 */
	public boolean updateToken(String updateUserLoginQuery) {
		// TODO Auto-generated method stub
		int check = commnsJdbc.update(updateUserLoginQuery);
		if (check == 1)
			return true;
		else
			return false;
	}

	/**
	 * Verify token.
	 *
	 * @param email the email
	 * @param token the token
	 * @return true, if successful
	 */
	public boolean verifyToken(String email, String token) {
		// TODO Auto-generated method stub
		int recordExists = 0;
		String selectRecordQuery = String.format(
				"select COALESCE((select id from users where email='%s' and activation_fpwd_key='%s'),0)", email,
				token);
		recordExists = commnsJdbc.selectInteger(selectRecordQuery);
		if (recordExists > 0) {
			// Compare the time as well
			String currentDate = AppCommons.getCurrentDate();
			String selectPasswordDateTimeQuery = String.format(
					"select COALESCE((select token_time from users where email='%s' and activation_fpwd_key='%s'),'NA')",
					email, token);
			String tokenTime = commnsJdbc.selectString(selectPasswordDateTimeQuery);
			if(AppCommons.compareDateInDays(currentDate, tokenTime)>AppCommons.TOKEN_EXPIRY_DAYS)
			{
				//Token is expired , lets not reset the password
				log.info("verifyToken : Token expired , can't reset the password");
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Update the password using password token.
	 *
	 * @param userIn the user in
	 * @return true, if successful
	 * @throws AppException the app exception
	 */
	public boolean updatePasswordUsingToken(User userIn) throws AppException {
		// TODO Auto-generated method stub
		int recordExists = 0;
		Boolean isActivationFlow = false;
		if(userIn.getActivationFpwdKey()!=null)
		{
			if(!userIn.getActivationFpwdKey().equalsIgnoreCase(""))
			{
				if(userIn.getActivationFpwdKey().startsWith(AppCommons.ACT_ACTIVATE_USER))
				{
					isActivationFlow = true;
				}
			}
		}
		String selectRecordQuery = String.format(
				"select COALESCE((select id from users where email='%s' and activation_fpwd_key='%s'),0)",
				userIn.getEmail(), userIn.getActivationFpwdKey());
		recordExists = commnsJdbc.selectInteger(selectRecordQuery);
		userIn.setPassword(passwordEncoder.encode(userIn.getPassword()));
		if (recordExists > 0) {
			// Valid email and token associated with the mail
			// Verify the token validity
			String currentDate = AppCommons.getCurrentDate();
			String selectPasswordDateTimeQuery = String.format(
					"select COALESCE((select token_time from users where email='%s' and activation_fpwd_key='%s'),'NA')",
					userIn.getEmail(), userIn.getActivationFpwdKey());
			String tokenTime = commnsJdbc.selectString(selectPasswordDateTimeQuery);
			Integer compare = AppCommons.compareStringDate(currentDate, tokenTime);
			
			if(AppCommons.compareDateInDays(currentDate, tokenTime)>AppCommons.TOKEN_EXPIRY_DAYS)
			{
				//Token is expired , lets not reset the password
				log.info("updatePasswordUsingToken : Token expired , can't reset the password");
				return false;
			}
			// End verify token
			String updateUserLoginQuery = String.format("update users set password='%s' where email='%s'",
					userIn.getPassword(), userIn.getEmail());
			int resultUpdatePassword = commnsJdbc.update(updateUserLoginQuery);
			// userIn.setPasswordToken(null);
			// userIn.setTokenTime(null);
			userIn.setActivationFpwdKey(null);
			// userIn.setTok
			String resetTokenQuery = String.format(
					"update users set activation_fpwd_key=null,token_time=null where email='%s'", userIn.getEmail());
			commnsJdbc.update(resetTokenQuery);
			log.info("Password changed successfully : "+resultUpdatePassword);
			if (resultUpdatePassword > 0)
			{
				if(isActivationFlow)
				{
					//Activation flow activate the user here
					log.info("Activating the user");
					activateUser(recordExists);
					log.info("User is activated , try logging in using new password");
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Update password of all users.
	 *
	 * @return true, if successful
	 */
	public boolean updatePasswordOfAllUsers() {
		// TODO Auto-generated method stub
		String query = "select * from users";
		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> totalMap = commnsJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setPassword((String) map.get("password"));
			user.setId((int) map.get("id"));
			user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("firstname"));
			user.setLastname((String) map.get("lastname"));
			user.setLocation((String) map.get("location"));
			user.setDepartment((String) map.get("department"));
			user.setRole((String) map.get("role"));
			user.setEnabled((boolean) map.get("enabled"));
			user.setEmployee_id((String) map.get("employee_id"));
			user.setProfile_image((String) map.get("profile_image"));
			// user.setSampleString(name);
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			String updateUserLoginQuery = String.format("update users set password='%s' where email='%s'",
					user.getPassword(), user.getEmail());
			log.info("password updated");
			int resultUpdatePassword = commnsJdbc.update(updateUserLoginQuery);
			// list.add(user);
		}

		return true;
	}

	/**
	 * Update the password using email.
	 *
	 * @param userIn the user in
	 * @return true, if successful
	 * @throws AppException the app exception
	 */
	public boolean updatePassword(User userIn) throws AppException {
		// TODO Auto-generated method stub
		// End verify token
		String selectEncodedPasswordQuery = String
				.format("select COALESCE((select password from users where username='%s'),'NA')", userIn.getUsername());
		String encodedPassword = commnsJdbc.selectString(selectEncodedPasswordQuery);
		if (passwordEncoder.matches(userIn.getOldPassword(), encodedPassword)) {
			userIn.setPassword(passwordEncoder.encode(userIn.getPassword()));
			String updateUserLoginQuery = String.format("update users set password='%s' where username='%s'",
					userIn.getPassword(), userIn.getUsername());
			int resultUpdatePassword = commnsJdbc.update(updateUserLoginQuery);
			if (resultUpdatePassword > 0)
				return true;
		}
		return false;
	}

	/**
	 * Gets the user profile by email.
	 *
	 * @param email the email
	 * @return the user profile by email
	 */
	public User getUserProfileByEmail(String email) {
		// TODO Auto-generated method stub
		String query = "select * from users where users.email = '"
				+ email + "' ";

		// SqlRowSet rowSet = commnsJdbc.queryForMyProfile(query);
		// user.setFirstname(rowSet.getString(2));

		List<Map<String, Object>> userMap = commnsJdbc.queryForList(query);

		for (Iterator<Map<String, Object>> iterator = userMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			// user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user = (User) GlobalConfigs.getInstance().getBean("userBean");
			user.setUsername((String) map.get("username"));
			user.setId((int) map.get("id"));
			user.setEmail((String) map.get("email"));
			user.setFirstname((String) map.get("firstname"));
			user.setLastname((String) map.get("lastname"));
		}

		return user;

	}

}
