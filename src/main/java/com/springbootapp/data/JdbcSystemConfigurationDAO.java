package com.springbootapp.data;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springbootapp.model.SystemConfiguration;
import com.springbootapp.utils.AppCommons;


// TODO: Auto-generated Javadoc
/**
 * The Class JdbcSystemConfigurationDAO.
 */
@Component
public class JdbcSystemConfigurationDAO implements SystemConfigurationDAO {
	
	/** The common jdbc. */
	@Autowired
	CommonJdbc commonJdbc;
	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(JdbcSystemConfigurationDAO.class);

	/* (non-Javadoc)
	 * @see com.springbootapp.data.SystemConfigurationDAO#readConfigMap()
	 * ARJUN : Looks like this is  used but with wrong return type. FUTURE CHECK.
	 */
	@Override
	public Map<String, String> readConfigMap() {
		// TODO Auto-generated method stub
		String query = "select id,config_param,config_value from system_configuration where config_type='GLOBALS'";
		
		List<Map<String, Object>> totalMap = commonJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			Integer id = (Integer) map.get("id");
			String configParam = (String)map.get("config_param");
			String configValue = (String)map.get("config_value");
			AppCommons.configMap.put(configParam, configValue);
			log.info("Settings id : "+id +  " " + configParam + " " + configValue);	
		}
		return null;
	}
	
	/**
	 * This is same as above except that it updates setitings maps. Settings is subset of config . But handled in data controller layer.
	 */
	@Override
	public Map<String, String> readSysSettingsMap() {
		// TODO Auto-generated method stub
		String query = "select id,config_param,config_value from system_configuration where config_type='GLOBALS'";
		
		List<Map<String, Object>> totalMap = commonJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			Integer id = (Integer) map.get("id");
			String configParam = (String)map.get("config_param");
			String configValue = (String)map.get("config_value");
			AppCommons.settingsMap.put(configParam, configValue);
			log.info("Settings id : "+id +  " " + configParam + " " + configValue);			
		}
		return AppCommons.settingsMap;
	}

	public Boolean updateSettings(SystemConfiguration systemConfiguration) {
		// TODO Auto-generated method stub
		Boolean resp = false;
		String global_emails = (systemConfiguration.getGlobal_emails().matches("YES")||systemConfiguration.getGlobal_emails().matches("NO"))?systemConfiguration.getGlobal_emails():"YES";
		String global_notifications = (systemConfiguration.getGlobal_notifications().matches("YES")||systemConfiguration.getGlobal_notifications().matches("NO"))?systemConfiguration.getGlobal_notifications():"YES";
		
		String query = "UPDATE `system_configuration` SET `config_value`='"+global_emails+"' WHERE `config_param` = 'GLOBAL_EMAILS_ON';";
		//UPDATE `springbootapp1`.`system_configuration` SET `description` = 'Notify on update relationship to user whose relationship is changed' WHERE `system_configuration`.`id` = 15;";
		log.info(query);
		int checksettingsupdate = commonJdbc.insert(query);
		query =" UPDATE `system_configuration` SET `config_value`='"+global_notifications+"' WHERE `config_param` = 'GLOBAL_NOTIFICATIONS_ON';";
		//UPDATE `springbootapp1`.`system_configuration` SET `description` = 'Notify on update relationship to user whose relationship is changed' WHERE `system_configuration`.`id` = 15;";
		log.info(query);
		 checksettingsupdate += commonJdbc.insert(query);
		 log.info("total changes " + checksettingsupdate);
		if(checksettingsupdate !=0)
		{
			return true;
		}
		else
		{
			return false;
		}
		//return null;
	}

}
