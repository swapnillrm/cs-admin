package com.springbootapp.data;

import java.util.List;

import com.springbootapp.model.User;
import com.springbootapp.utils.AppException;

// TODO: Auto-generated Javadoc
/**
 * The Interface CustomUserDAO.
 */
public interface CustomUserDAO {

	/**
	 * Update user profile.
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	public boolean updateUserProfile(User user) throws AppException;

	/**
	 * Register user.
	 *
	 * @param user the user
	 * @return the integer
	 */
	public Integer registerUser(User user) throws AppException;

	/**
	 * Save profile image.
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	public boolean saveProfileImage(User user) throws AppException;


	/**
	 * Gets the all users.
	 *
	 * @param whoAmI the who am I
	 * @param skipMe Skip me skips current User
	 * @return the all users
	 */
	List<User> getAllUsers(Integer whoAmI, Boolean skipMe,Boolean skipDisabled);
	
	/**
	 * Gets the my profile.
	 *
	 * @param username the username
	 * @return the my profile
	 */
	public User getMyProfile(String username);

	/**
	 * Gets the favourites.
	 *
	 * @param user_id the user id
	 * @return the favourites
	 */
	List<User> getFavourites(Integer user_id);

	/**
	 * Gets the all active users.
	 *
	 * @return the all active users
	 */
	List<User> getAllActiveUsers();

	/**
	 * Gets the all inactive users.
	 *
	 * @return the all inactive users
	 */
	List<User> getAllInactiveUsers();

	/**
	 * Gets the user profile by id.
	 *
	 * @param user_id the user id
	 * @return the user profile by id
	 */
	User getUserProfileById(Integer user_id);

	//User getUserProfileBySessionId(String sessionToken);

	/**
	 * Checks if is session valid.
	 *
	 * @param sessionToken the session token
	 * @return the boolean
	 */
	Boolean isSessionValid(String sessionToken);

	/**
	 * Gets the system user stats.
	 *
	 * @return the system user stats
	 */
	User getSystemUserStats();

	



//<User> getFavourites(Integer user_id);

	

}
