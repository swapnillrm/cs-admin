package com.springbootapp.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.springbootapp.config.ConfigParams;
import com.springbootapp.config.GlobalConfigs;
import com.springbootapp.utils.AppCommons;

// TODO: Auto-generated Javadoc
/**
 * The Class CommonJdbc.
 */
@Component
public class CommonJdbc {

	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(CommonJdbc.class);
	
	/** The jdbc template. */
	@Autowired
	JdbcTemplate jdbcTemplate;
	/*
	 * @Autowired JdbcTemplate jdbcTemplate;
	 */

	/**
	 * Query for list.
	 *
	 * @param query the query
	 * @return the list
	 */
	public List<Map<String, Object>> queryForList(String query) {
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		log.info("************ SELECT LIST **********");
		log.info(query);
		
		return jdbcTemplate.queryForList(query);
	}

	/**
	 * Update.
	 *
	 * @param query the query
	 * @return the int
	 */
	public int update(String query) {
		log.info("************ INSERT OR UPDATE  **********");
		log.info(query);
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		return jdbcTemplate.update(query);
	}

	/**
	 * Insert.
	 *
	 * @param query the query
	 * @return the int
	 */
	public int insert(String query) {
		log.info("************ INSERT   **********");
		log.info(query);
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		return jdbcTemplate.update(query);
	}
	
	/**
	 * Delete.
	 *
	 * @param query the query
	 * @param params the params
	 * @param types the types
	 * @return the int
	 */
	public int delete(String query, Object[] params, int[] types) {
		log.info("************ DELETE  **********");
		log.info(query);
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		return jdbcTemplate.update(query, params, types);
	}
	
	/**
	 * Delete.
	 *
	 * @param query the query
	 * @return the int
	 */
	public int delete(String query) {
		log.info("************ DELETE QUERY  **********");
		log.info(query);
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		return jdbcTemplate.update(query);
	}

	/**
	 * returns null if query returns null value.
	 *
	 * @param query the query
	 * @return the string
	 */
	public String selectString(String query) {
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		log.info("************ SELECT STRING **********");
		log.info(query);
		return (jdbcTemplate.queryForObject(query, String.class)).toString();
	}

	/**
	 * returns null if query returns null value.
	 *
	 * @param query the query
	 * @return the integer
	 */
	public Integer selectInteger(String query) {
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		log.info("************ SELECT INTEGER  **********");
		log.info(query);
		return (jdbcTemplate.queryForObject(query, Integer.class)).intValue();
	}

	/**
	 * Batch update.
	 *
	 * @param query the query
	 * @return the int[]
	 */
	/*
	 * 
	 */
	public int[] batchUpdate(String... query) {
		/*JdbcTemplate jdbc = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		log.info("************ Batch update  **********");
		log.info(""+query);
		return jdbcTemplate.batchUpdate(query);
	}
	
	/**
	 * DB test live method
	 *
	 * @param query the query
	 * @return the list
	 */
	public List<Map<String, Object>> dbTestForList(String query) {
		/*JdbcTemplate jdbcTemplate = (JdbcTemplate) GlobalConfigs.getInstance().getBean("jdbcTemplateBean");*/
		//log.info("DB test for list");
		//log.info(query);		
		return jdbcTemplate.queryForList(query);
	}

	/**
	 * Use this method only for inserting the logs into log table
	 * @param logType
	 * @param logTimeStamp
	 * @param logMessage
	 * @param dbLogTrace
	 * @param ack
	 * @param user_id
	 */
	public void insertLog(String logType, Date logTimeStamp, String logMessage, String dbLogTrace,
			String ack, Integer user_id) {
		// TODO Auto-generated method stub
		//new Object[]		
		SimpleDateFormat sdf = new SimpleDateFormat(AppCommons.appDateFormat);
		String currentTime = sdf.format(logTimeStamp);
		
		String insertLog = String.format("insert into logs(log_type,log_timestamp,log_message,log_trace,log_status_code,user_id) values ('%s','%s','%s','%s','%s','%s')", logType,
				currentTime,logMessage,dbLogTrace,ack,user_id);
		jdbcTemplate.update(insertLog);		
	}

}
