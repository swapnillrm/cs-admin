package com.springbootapp.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springbootapp.config.GlobalConfigs;
import com.springbootapp.model.Sample;

// TODO: Auto-generated Javadoc
/**
 * The Class JdbcSampleDAO.
 */
@Component
public class JdbcSampleDAO implements SampleDAO {

	/** The sample. */
	@Autowired
	Sample sample;

	/** The common jdbc. */
	@Autowired
	CommonJdbc commonJdbc;

	/* (non-Javadoc)
	 * @see com.springbootapp.data.SampleDAO#getAllSamples()
	 */
	@Override
	public List<Sample> getAllSamples() {
		// TODO Auto-generated method stub
		String query = "select name from samples";
		List<Sample> list = new ArrayList<Sample>();
		List<Map<String, Object>> totalMap = commonJdbc.queryForList(query);
		for (Iterator<Map<String, Object>> iterator = totalMap.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			String name = (String) map.get("name");
			sample = (Sample) GlobalConfigs.getInstance().getBean("sampleBean");
			sample.setSampleString(name);
			list.add(sample);
		}
		return list;
	}

}
