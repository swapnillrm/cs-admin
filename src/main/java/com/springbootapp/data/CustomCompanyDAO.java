package com.springbootapp.data;

import java.util.List;

import com.springbootapp.model.Company;
import com.springbootapp.model.User;
import com.springbootapp.utils.AppException;

//TODO: Auto-generated Javadoc
/**
* The Interface CustomCompanyDAO.
*/

public interface CustomCompanyDAO {
	
	/**
	 * Adding Company.
	 *
	 * @param user the company
	 * @return the integer
	 */
	public Object addCompany(Company company) throws AppException;
	
	
	/**
	 * Gets the all companies.
	 *
	 */
	
	public List<Company> getAllCompanies();
	
	public List<Company> getComdomains(Company com_id);


	public Integer updateCompanyProfile(Company company) throws AppException;


	boolean deleteCompanyProfile(Company company) throws AppException;

	Company getCompanyProfileById(Company com_id);

	
	Company identifyBaseUrl(Company company);
}

