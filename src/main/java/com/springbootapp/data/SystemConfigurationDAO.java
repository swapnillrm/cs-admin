package com.springbootapp.data;

import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Interface SystemConfigurationDAO.
 */
public interface SystemConfigurationDAO {
	
	/**
	 * Read config map.
	 *
	 * @return the map
	 */
	public Map<String,String> readConfigMap();

	Map<String, String> readSysSettingsMap();

	
//	public Map<String, String> readLangMap();

}
