package com.springbootapp.data;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JdbcDBLogsDAO implements DBLogsDAO {
	
	@Autowired
	CommonJdbc commonJdbc;

	public void insertLog(String logNotice, Date currentDateInDateFormat, String dbActPinSavePin, String dbLogTrace,
			String ack, Integer user_id) {
		// TODO Auto-generated method stub
		commonJdbc.insertLog(logNotice,currentDateInDateFormat,dbActPinSavePin,dbLogTrace,ack,user_id);		
	}

}
