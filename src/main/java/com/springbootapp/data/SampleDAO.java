package com.springbootapp.data;

import java.util.List;

import com.springbootapp.model.Sample;



// TODO: Auto-generated Javadoc
/**
 * The Interface SampleDAO.
 */
public interface SampleDAO {

	/**
	 * Gets the all samples.
	 *
	 * @return the all samples
	 */
	public List<Sample> getAllSamples() ;
}
