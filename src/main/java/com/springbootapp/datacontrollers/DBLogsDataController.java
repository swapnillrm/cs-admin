package com.springbootapp.datacontrollers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.springbootapp.data.JdbcDBLogsDAO;

@Controller
public class DBLogsDataController {
	
	@Autowired
	JdbcDBLogsDAO jdbcDBLogsDAO;

	/**
	 * Inser the db logs about the operations 
	 * @param logNotice
	 * @param currentDateInDateFormat
	 * @param dbActPinSavePin
	 * @param dbLogTrace
	 * @param ack
	 * @param user_id
	 */
	public void insertLog(String logNotice, Date currentDateInDateFormat, String dbActPinSavePin, String dbLogTrace,
			String ack, Integer user_id) {
		// TODO Auto-generated method stub
		jdbcDBLogsDAO.insertLog(logNotice,currentDateInDateFormat,dbActPinSavePin,dbLogTrace,ack,user_id);		
	}
	
	

}
