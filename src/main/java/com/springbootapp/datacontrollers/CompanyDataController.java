package com.springbootapp.datacontrollers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import com.springbootapp.model.Company;
import com.springbootapp.model.User;
import com.springbootapp.data.JdbcCustomCompanyDAO;
import com.springbootapp.utils.AppException;


@Controller
public class CompanyDataController {
	
	@Autowired
	Company company;
	
	@Autowired
	JdbcCustomCompanyDAO jdbcCustomCompanyDAO;

	/**
	 * 
	 * @param email,com_name
	 * @return
	 */
	
	@Transactional
	public Company addCompany(Company company) throws AppException {
		// TODO Auto-generated method stub
		Company updateFlag = null;
		updateFlag = jdbcCustomCompanyDAO.addCompany(company);
		
		if (updateFlag == null)
			return null;
		return updateFlag;
		
	}
	
	
	/**
	 * get all companies API Swapnil
	 */

	public List<Company> getAllCompanies() {
		// TODO Auto-generated method stub
		return jdbcCustomCompanyDAO.getAllCompanies();
	}

	/**
	 * get all companies API Swapnil
	 * @param company 
	 */

	public List<Company> getComdomains(Company company) {
		// TODO Auto-generated method stub
		return jdbcCustomCompanyDAO.getComdomains(company);
	}

	public Company updateCompanyProfile(Company company) throws AppException {
		// TODO Auto-generated method stub
		Integer updateFlag = null;
		updateFlag = jdbcCustomCompanyDAO.updateCompanyProfile(company);
		jdbcCustomCompanyDAO.updateCompanydomains(company);
		
		if (updateFlag == null)
			return null;
		return company;
	}
	
	public Company updateCompanydomains(Company company) throws AppException {
		// TODO Auto-generated method stub
		Company updateFlag = null;
		updateFlag = jdbcCustomCompanyDAO.updateCompanydomains(company);
		if (updateFlag == null)
			return null;
		return company;
	}
	
	public Company deleteCompanyProfile(Company company) throws AppException {
		// TODO Auto-generated method stub
		Boolean updateFlag=false;
		updateFlag = jdbcCustomCompanyDAO.deleteCompanyProfile(company);
		if (updateFlag == false)
			return null;
		return company;
	}
	
	
	public boolean activateCompany(Company company) {
		// TODO Auto-generated method stub
		return jdbcCustomCompanyDAO.activateCompany(company);
		// return true;
	}
	
	public boolean deactivateCompany(Company company) {
		// TODO Auto-generated method stub
		return jdbcCustomCompanyDAO.deactivateCompany(company);
		// return true;
	}
	
	
	public Company getCompanyProfileById(Company company) {
		// TODO Auto-generated method stub
		Company updateFlag = null;
		updateFlag = jdbcCustomCompanyDAO.getCompanyProfileById(company);
		if (updateFlag == null)
			return null;
		return updateFlag;
	}
	
	public Company identifyBaseUrl(Company company) {
		// TODO Auto-generated method stub
		Company updateFlag = null;
		updateFlag = jdbcCustomCompanyDAO.identifyBaseUrl(company);
		if (updateFlag == null)
			return null;
		return updateFlag;
	}


	
}
