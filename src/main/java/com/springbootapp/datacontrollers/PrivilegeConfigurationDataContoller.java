/**
 * 
 */
package com.springbootapp.datacontrollers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;


/**
 * Privilege Configuration Data controller provides hooks to check and validate  privileges of user based on privileges configuration
 * @author leftrightmind
 * @since   2016-06-17 
 */
public class PrivilegeConfigurationDataContoller {
	
	/**
	 * This method checks whether a given Activity is allowed for a particular user or not 
	 * @author leftrightmind
	 * @since   2016-06-17 
	 */
	public boolean isActivityAllowed(String Activity,Integer U1, Integer U2)
	{
		boolean resp =true;
		
		// Currently we do not have concrete logic on most Activities , however, we can define this as we go ahead.
		
		
		return resp;
		
		
	}

}
