package com.springbootapp.datacontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.springbootapp.data.JdbcCustomUserDAO;
import com.springbootapp.model.User;
import com.springbootapp.utils.AppCommons;
import com.springbootapp.utils.AppException;
import com.springbootapp.utils.PinFileUploader;

@Controller
public class UserDataController {

	@Autowired
	User user;

	@Autowired
	JdbcCustomUserDAO jdbcCustomUserDAO;

	@Autowired
	PinFileUploader pinFileUploader;

	public User updateUserProfile(User user) {
		// TODO Auto-generated method stub
		Boolean updateFlag=false;
		try {
			updateFlag = jdbcCustomUserDAO.updateUserProfile(user);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			updateFlag = false;
			e.printStackTrace();
		}
		if (updateFlag == false)
			return null;
		return user;
	}

	/**
	 * This method is transactional there are two tables user and user_roles
	 * 
	 * @param user
	 * @return
	 */
	@Transactional
	public User registerUser(User user) {
		// TODO Auto-generated method stub
		Integer updateFlag = 0;

		// username and email should match, email should be email , firstname
		// last name , employeeid should not be null ,

		try {
			updateFlag = jdbcCustomUserDAO.registerUser(user);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			updateFlag=0;
			e.printStackTrace();
		}

		if (updateFlag.equals(0))
			return null;
		else {
			user = jdbcCustomUserDAO.getUserProfileById(updateFlag);
			return user;
		}

		// return user;
	}

	@Transactional
	public User updateUserProfileImage(User user) {
		// TODO Auto-generated method stub
		Boolean updateFlag=false;
		try {
			updateFlag = jdbcCustomUserDAO.updateUserProfileImage(user);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			updateFlag=false;
			e.printStackTrace();
		}
		if (updateFlag == false)
			return null;
		return user;
	}

	/**
	 * get all users API saurabh
	 */

	public List<User> getAllUsers(Integer whoAmI,Boolean skipMe,Boolean skipDisabled) {
		// TODO Auto-generated method stub
		return jdbcCustomUserDAO.getAllUsers(whoAmI,skipMe,skipDisabled);
	}
	
	public User getSystemUserStats() {
		// TODO Auto-generated method stub
		return jdbcCustomUserDAO.getSystemUserStats();
	}

	public List<User> getFavourites(Integer user_id) {
		// TODO Auto-generated method stub
		return jdbcCustomUserDAO.getFavourites(user_id);
	}

	public User getMyProfile(String username) {
		// TODO Auto-generated method stub
		User user = jdbcCustomUserDAO.getMyProfile(username);
		if (user == null)
			return null;
		return user;
	}

	public User getUserProfileById(Integer user_id) {
		// TODO Auto-generated method stub
		User user = jdbcCustomUserDAO.getUserProfileById(user_id);
		if (user == null)
			return null;
		return user;
	}
	
	public User getUserProfileByEmail(String email) {
		// TODO Auto-generated method stub
		User user = jdbcCustomUserDAO.getUserProfileByEmail(email);
		if (user == null)
			return null;
		return user;
	}

	public boolean activateUser(Integer id) {
		// TODO Auto-generated method stub
		return jdbcCustomUserDAO.activateUser(id);
		// return true;
	}

	public boolean deactivateUser(Integer id) {
		// TODO Auto-generated method stub
		return jdbcCustomUserDAO.deactivateUser(id);
		// return true;
	}

	public User uploadProfileImage(User user2) {
		// TODO Auto-generated method stub
		User user = pinFileUploader.uploadProfileFile(user2);
		if (user.getStatus() == false)
			return null;
		return user;
	}

	public String generatePassword(String hash) {

		String resp = null;

		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
		// String pwd = RandomStringUtils.random( 14, 0, 0, false, false,
		// characters.toCharArray(), new SecureRandom() );
		// for now . since the generation is not working

		String pwd = hash;
		return pwd;
	}

	public String encryptPassword(String hash) {
		String resp = null;

		return resp;
	}

	public boolean updateToken(User user) {
		// TODO Auto-generated method stub
		String updateUserLoginQuery = String.format(
				"update users set activation_fpwd_key='%s',token_time='%s' where username='%s'",
				user.getActivationFpwdKey(), AppCommons.getCurrentDate(), user.getUsername());
		boolean result = jdbcCustomUserDAO.updateToken(updateUserLoginQuery);
		return result;
	}

	public boolean verifyToken(String email, String token) {
		// TODO Auto-generated method stub
		boolean result = jdbcCustomUserDAO.verifyToken(email, token);
		return result;
	}

	public boolean updatePasswordUsingToken(User userIn) {
		// TODO Auto-generated method stub
		try {
			jdbcCustomUserDAO.updatePasswordUsingToken(userIn);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;

	}

	/*public boolean updatePasswordOfAllUsers() {
		return jdbcCustomUserDAO.updatePasswordOfAllUsers();
	}*/

	public boolean isUserActive(Integer Id) {
		return jdbcCustomUserDAO.isUserActive(Id);
	}

	public User updatePassword(User user) {
		// TODO Auto-generated method stub
		try {
			if (jdbcCustomUserDAO.updatePassword(user) == true)
				return user;
			else
				return null;
		} catch (AppException e) {
			// TODO Auto-generated catch block
			user=null;
			e.printStackTrace();
		}
		return user;
	}

}
