package com.springbootapp.datacontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.springbootapp.data.JdbcSampleDAO;
import com.springbootapp.model.Sample;

@Controller
public class SampleDataController {

	@Autowired
	Sample sample;
	
	@Autowired
	JdbcSampleDAO jdbcSampleDAO;

	public List<Sample> getAllSamples() {
		// TODO Auto-generated msethod stub
		return jdbcSampleDAO.getAllSamples();
	}

}
