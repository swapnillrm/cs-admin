package com.springbootapp.datacontrollers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.springbootapp.data.JdbcSystemConfigurationDAO;
import com.springbootapp.model.SystemConfiguration;

@Controller
public class SystemConfigurationDataController {
	
	@Autowired
	JdbcSystemConfigurationDAO jdbcSystemConfigurationDAO;

	public void loadConfigMap() {
		// TODO Auto-generated method stub
		jdbcSystemConfigurationDAO.readConfigMap();
		
		
	}
	/*
	public void loadLangMap() {
		// TODO Auto-generated method stub
		jdbcSystemConfigurationDAO.readConfigMap();
		
		
	}
	*/
	public Boolean updateSysSettings(SystemConfiguration systemConfiguration)
	{
		return jdbcSystemConfigurationDAO.updateSettings(systemConfiguration);
	}
	
	/**
	 * THis is same as config map ,EXCEPT That we will ONLY show what WE NEED TO SHOW to the outside world.
	 * @param systemConfiguration
	 * @return
	 */
	public SystemConfiguration getSysSettings(SystemConfiguration systemConfiguration)
	{
		Map<String,String> settings = new HashMap<String,String>();
		settings = jdbcSystemConfigurationDAO.readSysSettingsMap();
		
		systemConfiguration.setGlobal_emails(settings.get("GLOBAL_EMAILS_ON"));
		systemConfiguration.setGlobal_notifications(settings.get("GLOBAL_NOTIFICATIONS_ON"));
		
		
		return systemConfiguration;
	}
	
	
	/** 
	 *  THIS MAY NOT BE USED
	 * @return
	 */
	
	/*
	public boolean refreshAppConfigMap()
	{
		 jdbcSystemConfigurationDAO.readConfigMap();
		 return true;
		
	}*/

}
