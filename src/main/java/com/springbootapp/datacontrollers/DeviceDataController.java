package com.springbootapp.datacontrollers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DevicePlatform;
import org.springframework.stereotype.Controller;

@Controller
public class DeviceDataController {

	private static final Logger log = LoggerFactory.getLogger(DeviceDataController.class);

	public void printDeviceInfo(Device device) {
		String deviceType = "unknown";
		if (device.isNormal()) {
			DevicePlatform devPlatform = device.getDevicePlatform();			
			log.info("Device platform : "+devPlatform.name());			
			deviceType = "normal";			
		} else if (device.isMobile()) {
			DevicePlatform devPlatform = device.getDevicePlatform();			
			log.info("Device platform : "+devPlatform.name());			
			deviceType = "mobile";
		} else if (device.isTablet()) {
			DevicePlatform devPlatform = device.getDevicePlatform();			
			log.info("Device platform : "+devPlatform.name());
			deviceType = "tablet";
		}
		log.info("Device type : " + deviceType);
	}
}
