package com.springbootapp.utils;

public final class AppError {
	private static AppError appError;
	private int errorCode;
	private String errorMessage;

	private AppError() {
	}

	public static AppError getInstance() {
		if (appError == null) {
			appError = new AppError();
		}

		return appError;
	}

	public String getLastErrorMessage() {
		return errorMessage;
	}

	public int getLastErrorCode() {
		return errorCode;
	}

	public void setLastErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setLastErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
