package com.springbootapp.utils;

/**
 * This enum defines the constants for roles.
 * 
 * @author lrm_dev
 *
 */
public enum RelationShipMapping {

	MGR(1), PEER(2), DR(3), OTHER(4), NONE(5);

	int value;
	

	RelationShipMapping(int x) {
		this.value = x;
	}

	public Integer getValue() {
		return value;
	}
}
