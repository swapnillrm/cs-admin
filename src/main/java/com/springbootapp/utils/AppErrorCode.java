package com.springbootapp.utils;

public abstract interface AppErrorCode {

	public abstract String getCode();

	public abstract long getCodeNumber();

}
