package com.springbootapp.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.springbootapp.model.User;

@Component
public class PinFileUploader {

	@Autowired
	User user;
	
	private static final Logger log = LoggerFactory.getLogger(PinFileUploader.class);
	
	AWSCredentials credentials = new BasicAWSCredentials("AKIAJSXAQQIBPMZAOIYQ",
			"aIwPMuo95KoXdelYn68LwleGTiG7fbcaiPPsSLf/");
	AmazonS3 s3 = new AmazonS3Client(credentials);
	
	public User uploadProfileFile(User userObject/*String userFileName, String userFileType, String base64*/) {
		String bucketName = "project-attachments";
		
		user = userObject;
		try {
			/*
			 * Upload an object to your bucket - You can easily upload a file to
			 * S3, or upload directly an InputStream if you know the length of
			 * the data in the stream. You can also specify your own metadata
			 * when uploading to S3, which allows you set a variety of options
			 * like content-type and content-encoding, plus additional metadata
			 * specific to your applications.
			 */
			//System.out.println("Uploading a new object to S3 from a file\n");
			log.info("Uploading a new object to S3 from a file\n");

			// s3.putObject(new PutObjectRequest(bucketName, key,
			// createSampleFile()));

			String fileName = userObject.getFileName();// uploadParams.getJSONObject(i).getString("file_name");//"arjun_is_a_boy.png";
			String fileType = userObject.getFileType();//userFileType;// uploadParams.getJSONObject(i).getString("file_type");
			fileType.toLowerCase();

			String key = generateFileName(fileType);
			user.setFileName(key);
			String base64String = user.getBase64String();
			//log.info("Base64string in PinFileUploader"+base64String);
			byte[] bI = org.apache.commons.codec.binary.Base64.decodeBase64(base64String);

			InputStream fis = createSampleFile(bI);

			// giving access permission to view file
			AccessControlList permissionList = new AccessControlList();
			permissionList.grantPermission(GroupGrantee.AllUsers, Permission.Read);

			// metadata
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(bI.length);
			metadata.setContentType(fileType);
			metadata.setCacheControl("public, max-age=31536000");
			s3.putObject(new PutObjectRequest(bucketName, key, fis, metadata)
					.withCannedAcl(CannedAccessControlList.PublicRead));
			String url = "https://" + bucketName + ".s3.amazonaws.com/" + key;
			// responseObject.put("status", "true");
			// responseObject.put("msg", "file uploaded");
			// responseObject.put("fileName", key);
			// responseObject.put("fileType", fileType);
			// responseObject.put("file_path", url);
			// responseArray.put(responseObject);
			user.setStatus(true);
			user.setMsg("File uploaded");
			user.setFilePath(url);
			log.info("File uploaded successfully");
		} catch (AmazonServiceException ase) {			
			log.warn("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			//System.out.println("Error Message:    " + ase.getMessage());
			//System.out.println("HTTP Status Code: " + ase.getStatusCode());
			//System.out.println("AWS Error Code:   " + ase.getErrorCode());
			//System.out.println("Error Type:       " + ase.getErrorType());
			//System.out.println("Request ID:       " + ase.getRequestId());
			user.setStatus(false);
			user.setMsg(ase.getMessage());
			//user.setStatusCode(ase.getStatusCode());
			log.info("Upload failed with the following code : "+ase.getStatusCode());
			// responseObject.put("status", "false");
			// responseObject.put("msg", ase.getMessage());
			// responseObject.put("fileName","");
			// responseObject.put("file_path", "");
			// responseArray.put(responseObject);
		} catch (AmazonClientException ace) {
			user.setStatus(false);			
			log.warn("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			log.warn("Error Message: " + ace.getMessage());
						
		} catch (IOException e) {
			// TODO Auto-generated catch block
			user.setStatus(false);
			log.warn("Exception Message: " + e.getMessage());
			e.printStackTrace();
		}
		return user;
	}

	private static InputStream createSampleFile(byte[] bI) throws IOException {
		InputStream fis = new ByteArrayInputStream(bI);
		return fis;
	}

	public static String generateFileName(String fileType) {

		// break type at "/"

		String[] parts = fileType.split("/");
		String part2 = parts[1];
		part2.toLowerCase();

		// unique id
		String uniqueID = UUID.randomUUID().toString();

		// trim string to first 10 characters
		String upToNCharacters = uniqueID.substring(0, Math.min(uniqueID.length(), 10));

		// current date
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar cal = Calendar.getInstance();
		String formattedDate = dateFormat.format(cal.getTime());

		String finalString = formattedDate + upToNCharacters + "." + part2;

		return finalString;

	}

	
	
}
