package com.springbootapp.utils;

/**
 * Customized exception class
 * @author lrm_dev
 *
 */
public class AppException extends Exception implements AppCodeHolder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8099543543398493448L;
	private AppErrorCode code = AppCodeHolder.DEFAULT_CODE;

	public AppException() {
	}

	public AppException(Throwable cause) {
		super(cause.getMessage(), cause);
		setAppErrorCodeFrom(cause);
	}

	public AppException(String message) {

	}

	public AppException(String message, Throwable cause) {
		super(message, cause);
		setAppErrorCodeFrom(cause);
	}

	@Override
	public AppErrorCode getCode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCode(AppErrorCode errorCode) {
		// TODO Auto-generated method stub

	}

	private void setAppErrorCodeFrom(Throwable throwable) {
		if ((throwable instanceof AppCodeHolder)) {
			this.code = ((AppCodeHolder) throwable).getCode();
		} else {
			Throwable cause = throwable.getCause();
			if (cause != null) {
				setAppErrorCodeFrom(cause);
			}
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		if (this.code != null) {
			sb.append(". Result Code:");
			sb.append(this.code.getCode());
		}
		return sb.toString();
	}

}
