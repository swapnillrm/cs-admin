package com.springbootapp.utils;

public enum ErrorCode implements AppErrorCode {
	UNSPECIFIED_ERROR(0L), DEV_ERROR(1L), INSTALLATION_ERROR(2L), PLATFORM_NOT_READY(3L), NOT_IMPLEMENTED_YET(
			4L), SUCCESS(10L), PERSISTENCE_ERROR(20L), UNCOMPATIBLE_BEHAVIOUR(50L), PLUGIN_NOT_FOUND(
					90L), RESOURCE_NOT_EXISTS(91L), RESOURCE_ALREADY_EXISTS(92L), POLICY_NOT_EXISTS(
							93L), POLICY_ALREADY_EXISTS(94L), POLICY_STILL_USED(95L), REQUEST_NOT_VALID(
									100L), OPERATION_NOT_SUPPORTED(101L), MISSING_PARAMETER(110L), BAD_PARAMETER(
											120L), MESSAGE_NOT_ACCEPTED(130L), LOGICNAME_NOT_RECOGNIZED(
													142L), LOGICNAME_ALREADY_EXISTS(143L), LOGICNAME_NOT_ADMITTED(
															144L), LOGICNAME_OVERFLOW(145L), BAD_SOURCE_PLATFORM(
																	151L), BAD_TARGET_PLATFORM(152L), NO_FREE_PLATFORMS(
																			153L), NO_ACTION_NEEDED(
																					155L), CONTAINER_NOT_FOUND(
																							160L), NO_ACTIVE_PLATFORMS(
																									164L), WORKFLOW_ALREADY_EXISTS(
																											171L), WORKFLOW_ATTACH_DETACH_ERROR(
																													172L), WORKFLOW_SCALE_ERROR(
																															173L), WORKFLOW_NOT_EXISTS(
																																	174L), APPTYPE_NOT_RECOGNIZED(
																																			181L), RESOURCES_SHORTAGE(
																																					200L), MEMORY_SHORTAGE(
																																							201L), DISK_SHORTAGE(
																																									202L), THREAD_SHORTAGE(
																																											210L), FILE_SHORTAGE(
																																													220L), FILE_MOVE_ERROR(
																																															221L), BAD_ARCHIVE(
																																																	222L), TRANSACTION_ERROR(
																																																			223L), BAD_FS_PERMISSIONS(
																																																					230L), CONF_MISSING(
																																																							300L), CONF_BAD(
																																																									400L), CONF_EMPTY(
																																																											401L), CONF_PROPERTY_MISSING(
																																																													402L), CONF_PROPERTY_DUPLICATE(
																																																															403L), BAD_DNS(
																																																																	410L), LOAD_BALANCER_ERROR(
																																																																			450L), LOAD_BALANCER_INVALID_REQUEST(
																																																																					451L), LOAD_BALANCER_INSTALLATION_ERROR(
																																																																							452L), INVALID_OBJECT(
																																																																									500L), INVALID_CLASS_ERROR(
																																																																											510L), MALFORMED_SERIALIZATION_FORM_ERROR(
																																																																													545L), MALFORMED_JSON_ERROR(
																																																																															550L), MALFORMED_YAML_ERROR(
																																																																																	560L), CONNECT_ERROR(
																																																																																			600L), CONNECT_INTERRUPTED(
																																																																																					601L), FTP_ERROR(
																																																																																							650L), MONGO_NOT_READY(
																																																																																									710L), MONGO_NOT_AUTHORIZED(
																																																																																											720L), DB_NOT_AUTHORIZED(
																																																																																													740L), INVALID_OBSOLETE_DB_OBJECT(
																																																																																															750L), ID_OVERFLOW(
																																																																																																	760L), AFFINITY_ERROR(
																																																																																																			810L), TASK_NOT_FOUND(
																																																																																																					820L), WORKFLOW_ALREADY_COMPLETED(
																																																																																																							830L), BAD_STAGE(
																																																																																																									840L), WORKFLOW_LIMIT_REACHED(
																																																																																																											850L), TOKEN_ERROR(
																																																																																																													900L), CREDENTIALS_WRONG(
																																																																																																															901L), CREDENTIALS_LOCKED(
																																																																																																																	902L), IP_ERROR(
																																																																																																																			903L), BAD_AUTHENTICATION_DB_SCHEMA(
																																																																																																																					904L), USER_ALREADY_EXISTS(
																																																																																																																							905L), USER_NOT_EXISTS(
																																																																																																																									906L), ROLE_NOT_EXISTS(
																																																																																																																											907L), ROLE_ALREADY_EXISTS(
																																																																																																																													908L), OPERATION_NOT_ALLOWED(
																																																																																																																															910L), MONITOR_ITEM_NOT_EXIST(
																																																																																																																																	950L);

	private final long codeNumber;

	private ErrorCode(long codeNumber) {
		this.codeNumber = codeNumber;
	}

	public String getCode() {
		return name();
	}

	public long getCodeNumber() {
		return this.codeNumber;
	}
}