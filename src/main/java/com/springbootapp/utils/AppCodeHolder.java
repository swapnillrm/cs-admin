package com.springbootapp.utils;



public interface AppCodeHolder {
	
	 public static final AppErrorCode DEFAULT_CODE = ErrorCode.UNSPECIFIED_ERROR;
	  
	  public abstract AppErrorCode getCode();
	  
	  public abstract void setCode(AppErrorCode errorCode);

}
