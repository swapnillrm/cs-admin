package com.springbootapp.utils;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DevicePlatform;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class AppCommons {
	
	public final static String appDateFormat = "yyyy-MM-dd HH:mm:ss";
	public static final String ACK = "SUCCESS";
	public static final String NACK = "FAILURE";
	
	public static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	/**
	 * Application constants definition file
	 * 
	 */
	
	public final static String APP_VERSION = "1.0.1";
	public final static String APP_TITLE = "";
	public final static String APP_AUTHOR = "LeftRightMind";
	public final static String APP_LASTMODIFIED_DATE = "12July2016";
	public final static String APP_BASE_URL ="http://localhost:8181";
	
	
	

	public final static Integer MGR = 1;
	public final static Integer PEER = 2;
	public final static Integer DR = 3;
	public final static Integer OTHER = 4;
	
	public final static String YES="YES";
	public final static String NO="NO";
	
	public final static String defaultPassword = "lrm123";
	
	
	
	private static final Logger log = LoggerFactory.getLogger(AppCommons.class);
	
	public static final Map<String, String> configMap = new HashMap<String,String>();
	public static final Map<String, String> settingsMap = new HashMap<String,String>();
	//public static final Map<String, String> LangMap = new HashMap<String,String>();

	
	// ######################################        			ACTIVITY NAMES    			 ##############################################################################
	
	
	//Pin Activities
	public final static String ACT_ADD_PIN ="ADD_PIN";
	public final static String ACT_ADD_PIN_WITHOUT_RATING ="ADD_PIN_WITHOUT_RATING";
	public final static String ACT_UPDATE_RELATIONSHIP ="UPDATE_RELATIONSHIP";
	public final static String ACT_GET_PINS_SELF = "GET_PINS_SELF";
	public final static String ACT_EXPORT_PINS="ACT_EXPORT_PINS";
	public final static String ACT_FORGOT_PASSWORD = "ACT_FORGOT_PASSWORD";
	
	//Objective Activities
	public final static String ACT_ADD_OBJECTIVE = "ACT_ADD_OBJECTIVE";
	public final static String ACT_UPDATE_OBJECTIVE = "ACT_UPDATE_OBJECTIVE";
	
	//Tag Activities
	public final static String ACT_ADD_TAG = "ACT_ADD_TAG";
	public final static String ACT_UPDATE_TAG = "ACT_UPDATE_TAG";
	
	// User activities 
	
	public final static String ACT_ACTIVATE_USER="ACT_ACTIVATE_USER";
	
	
	// Email Activities
	public final static String ACT_SEND_ACTIVATION_MAIL = "ACT_SEND_ACTIVATION_MAIL";
	
	
	
	
	// ######################################        			DEFAULT CONFIGURATION    			 ######################################################################
	// Default Application level constants. These may be different from system configuration. e.g. DEFAULT_NOTIFICATION_TYPE=NOTIFICATION
	
	
	public final static String LANG_DEFAULT="en";
	public static final String DEFAULT_LANG = null;
	
	public final static Integer TOKEN_EXPIRY_DAYS = 2;
	
	// ###################################### EXPORT PINS ############################
//	public final static String EXPORT_PINS_HEADER_ROW ="id,event,comment,ratingSet,pinner,pinnerFirstName,pinnerLastName,pinnerEmail,pinnerEmployeeId,pinnerEnabled,pinnerLastLoginDate,pinnerProfileImage,"
//				+ "pinnerDepartment,pinnerLocation,pinnee,pinneeFirstName,pinneeLastName,pinneeEmail,pinneeEmployeeId,pinneeEnabled,pinneeLastLoginDate,"
//				+ "pinneeProfileImage,pinneeDepartment,pinneeLocation,pinneeRelationship,relationRole,pinTag,objective,pinObjectiveTitle,pinRatingValId,"
//				+ "pinRatingValue,pinRatingTitle,pinDate,createdDate,status";
	
// REQUIRED FIELDS
	

	
	
	

	/*
	 * full all needed version
	 * 
	 * */
	/*
	public final static String EXPORT_PINS_HEADER_ROW_STR ="id,event,comment,pinRatingValue,pinRatingTitle,pinDate,"
						+ "pinnerFirstName,pinnerLastName,pinnerEmail,pinnerEmployeeId,pinnerRelationship,"
						+ "pinneeFirstName,pinneeLastName,pinneeEmail,pinneeEmployeeId,pinneeRelationship,"
						+ "pinObjectiveTitle,"
						+ "createdDate,status";

	*/
	
	/** desired fields 
	 * 
	 */
	
	public final static String EXPORT_PINS_HEADER_ROW_STR ="S No,Event,Comment,Pin Objective,Rating,Rating Title,Pin Date,"
						+ "Pinner First Name,Pinner Last Name,Pinner Employee ID,"
						+ "Pinnee First Name,Pinnee Last Name,Pinnee Employee ID,Pinnee Relationship,"
						+ "Pin Created Date,Pin Status";
	
	//  ######################################        			LANG CONFIGURATION    			 ######################################################################
	
	// public final static String LANG_ADD_PIN_PINNEE_="";
	
	
	// #######################################################################################################################################
	
	//DB Logs Constants
	public final static String LOG_ERROR = "LOG_ERROR";
	public final static String LOG_WARN = "LOG_WARN";
	public final static String LOG_NOTICE = "LOG_NOTICE";
	public final static String LOG_TRANS = "LOG_TRANS";
	public final static String LOG_EMAIL = "LOG_EMAIL";	
	
	public final static String DB_LOG_TRACE = "DB_LOG_TRACE";
	public final static String DB_ACT_PIN_SAVE_PIN="DB_ACT_PIN_SAVE_PIN";
	public final static String DB_ACT_EXPORT_PINS="DB_ACT_EXPORT_PINS";
	//DB Logs Constants
	
	//DB table constants
	public final static String DB_PINS_TABLE = "pins";
	public final static String DB_OBJECTIVES_TABLE = "objectives";	
	public final static String DB_TAGS_TABLE = "tags";
	//DB table constant
	

	// ############################################################ App Common methods #############################################################

	/**
	 * Verify the session attributes
	 * @param session
	 * @return
	 */
	public static boolean verifySession(HttpSession session)
	{
		if(session==null)
		{
		return false;
		}
		if(session.getAttribute("username")==null)
		{
		return false;
		}
		return true;
	}
	
	public static String getCurrentDate() {
		String strDate = null;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date now = new Date();
			strDate = sdfDate.format(now);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.warn("Exception : " + e.getMessage());
		}
		return strDate;
	}
	
	/**
	 * Use the procedure to compare two dates
	 * 
	 * @param startDate
	 *            String
	 * @param endDate
	 *            String
	 * @return 0 if dates are equal , returns -1 if startDate < endDate ,
	 *         returns 1 if startDate > endDate
	 */
	public static Integer compareStringDate(String startDate, String endDate) {
		String strDate = null;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date fromDate = sdfDate.parse(startDate);
			Date toDate = sdfDate.parse(endDate);
			return fromDate.compareTo(toDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.warn("Exception : " + e.getMessage());
		}
		return null;
	}
	
	public static Integer compareDateInDays(String startDate,String endDate)
	{
		Integer diffInt = 0;
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDate = sdfDate.parse(startDate);
			Date toDate = sdfDate.parse(endDate);
			long diff = fromDate.getTime() - toDate.getTime();
		    log.info("compareDateInDays : Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
			diffInt = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			return diffInt;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.warn("Exception : " + e.getMessage());
		}
		return null;
		
	}
	
	public final static String TAG_LOGIN="TAG_LOGIN";
	/**
	 * Print connected device data
	 * @param request
	 * @param device
	 * @param TAG
	 */
	public static void printDeviceInfoUsingRequestAndDevice(HttpServletRequest request,Device device,String TAG)
	{
		String deviceType = "unknown";        
        if (device.isNormal()) {
            deviceType = "normal";
        } else if (device.isMobile()) {
            deviceType = "mobile";
        } else if (device.isTablet()) {
            deviceType = "tablet";
        }
        DevicePlatform devPlatform = device.getDevicePlatform();        
		log.info(TAG + "Request from device type : " + deviceType);
		log.info(TAG + "Request from device platform : " + devPlatform.name());
		log.info(TAG + "Request from IP : " + request.getRemoteAddr());		
	}
	
	public static Date getCurrentDateInDateFormat() {
		return new Date();
	}
	
	/**
	 * Validate the email address 
	 * @param email
	 * @return
	 */
	public static boolean isEmailValid(final String email)
	{
		if(email==null) return false;
		if(email.equalsIgnoreCase("")) return false;
		
		Pattern pattern;
		Matcher matcher;
		
		pattern = Pattern.compile(EMAIL_PATTERN);
		
		matcher = pattern.matcher(email);
		return matcher.matches();		
	}
	
	
	
}
