var app = angular.module('pinReportCtrl.controllers', []);


//normal controller
app.controller('reportCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$modal','$log',
  function($scope,userService, $state, $rootScope,localStorageService,$modal,$log) {
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $scope.req = {};
  //$scope.u_role= $rootScope.user.role;


  console.log('We are in reports');
  removeHref(); //to remove href attr from <a>
  panelHeight(); //for window height
  scrollPageToTop(); //scrolling page to top 

   $scope.registerUserSuccess = function(resData) {
           
           if(resData.status == "SUCCESS")
            {
              //console.log(JSON.stringify(responce.data));
              resetAlert();
              alertify.alert('User registered successfully.');
              $scope.employeeList1 = resData.data;
            }
            else
            {
              resetAlert();
              alertify.alert('Registration failed.');
              $state.go('home',{'reload':true});
              //$scope.employeeList1 = [];
            }
            
        }
        $scope.registerUser = function() {
            //closeAddModal();

            var reqData = {}
            reqData.firstname = $scope.register.firstname;
            reqData.lastname = $scope.register.lastname;
            reqData.department = $scope.register.department;
            reqData.location = $scope.register.location;
            reqData.employee_id = $scope.register.employee_id;
            reqData.email = $scope.register.email;
            reqData.username = $scope.register.email;
            console.log('reg data' + JSON.stringify(reqData) );

            userService.registerNewUser(reqData, $scope.registerUserSuccess)
        }
     
        $scope.deactivate = function(){
          var reqData ={}
          reqData.employee_id = $rootScope.user.id;
          userService.deactivateUser(reqData, function(resData){
            if(resData.status=="SUCCESS"){
              $scope.getAllEmployee();
            }
            else{
              resetAlert();
              alertify.alert('Could not deactivate. Please try again.')
            }

          })
        }



         /*$scope.getAllEmployeeSuccess = function(resData) {
           
           if(resData.status == "SUCCESS")
            {
              $scope.empList = resData.data;
              localStorageService.set('employeeList', $scope.empList);
            }
            else
            {
              delete $rootScope.user;  
              localStorageService.clearAll();
              resetAlert();
              alertify.alert(resData.msg)
            }
            
        }
        $scope.getAllEmployee = function() {
            var reqData = {}
            
            userService.getAllEmployeeData(reqData, $scope.getAllEmployeeSuccess)
        }
        $scope.getAllEmployee();*/

 $scope.getAllmanagerSuccess = function(responce) {
           
           if(responce.status == "success")
            {
              

              $scope.managerList = responce.data;

            }
            else
            {
              $scope.managerList = [];
            }
            
        }
      


         $scope.addProjectSuccess = function(responce) {
           
           if(responce.status == "success")
            {
               
              alert('Add successfully');
            }
            else
            {
              alert('Try Again');
            }
            
        }
     
        $scope.addProject = function()
        {
          var res = {};
         // console.log($scope.start);
          $scope.duration = $('#duration').text();
          var a = moment($scope.req.start,'DD/MM/YYYY');
          var b = moment($scope.req.end,'DD/MM/YYYY');
          //var days = d2.diff(d1, 'days');
          //var a = moment('1/1/2012', 'DD/MM/YYYY');
          //var b = moment('1/1/2013', 'DD/MM/YYYY');
          var days = b.diff(a, 'months');
        //console.log(days);
        
          res.duration = $scope.duration;
          res.projectName = $scope.req.projectName;
          res.projectManagerId = $scope.req.managerId;
          userService.addProjecttoDb(res, $scope.addProjectSuccess)
        }




        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
