var app = angular.module('pinConsumerHomeCtrl.controllers', []);

app.controller('consumerHomeCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {

   //console.log('localStorageService '+JSON.stringify(localStorageService.get('userData')));
  //$scope.allEmpList = [];

  if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }


  
  $scope.activePart = 'step1';

  $scope.req = {};
  $scope.save = {};
  $scope.selDisplay = {};
  //$scope.slider1 = {};
  var newItem ={};
  $scope.ratingTextVal = {};
  
  $scope.item = [];
  $scope.userRelationship = [];
  $scope.editRelationsList = [];
  $scope.allEmpList = [];
  $scope.allTags = [];
  $scope.allRatings = [];
  $scope.checkedTag = [];
  $scope.pinHistory = [];
  

  $scope.hasRelation = false;
  $scope.selected = 0;
  $scope.editMode = false;
  $scope.havePinned = false;
  $scope.otherPinned = false;

// emp-list selected active class


$scope.date = new Date();
var today=new Date(); 
var mindate = new Date(today.setDate(today.getDate() - 7));

$scope.minDate = mindate;
  $scope.maxDate = new Date();
$scope.getDateFormat =function(timestamps) {
   
    return new Date(timestamps);
  }

    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

	
  panelHeight(); //for window height
  datePicker(); // for date picker
  scrollPageToTop(); //scroll page to top
  
// opening the wizard to add pin
$scope.openPinWizard= false;
 $scope.addingPinprocess = false;
  $scope.addPin= function(){
    $scope.openPinWizard= true;
   // $scope.save = {};
    $scope.activePart = 'step1';
    $scope.addingPinprocess = true;
    //$('input[type="checkbox"]:checked').removeAttr('checked');
     var d = $("input[type=radio][name='rad1']:checked").val();
     
   if(d != undefined)
   {
    $('#fstNxtbtn').removeAttr('disabled');
   }
   else
   {
    $('#fstNxtbtn').attr('disabled',true)
   }
  }
  $scope.cancelPin= function(){
    //$state.reload();
    //$state.forceReload();
     $scope.save = {};
    $scope.openPinWizard= false;
    $scope.addingPinprocess = false;
  }
  $scope.$watch('save.pinneeRelationship', function(value) {
       console.log(value);
        $('#fstNxtbtn').removeAttr('disabled');
 });

// taking to the next step
  $scope.showNextStep = function(step){
        $scope.activePart = step;
        $('.step-pane').addClass('active');

        console.log('selected data till now '+JSON.stringify($scope.save));
  }


/*********************** Google charts with trend lines *************************/
  
$scope.drawGoogleChart1 = function(response1){
  
        if(response1.status == 'SUCCESS')
        { 
         $('.tab-container .tab-left .chartTab').addClass('active');
          //console.log($scope.selectedId);
          if(response1.data != [])
          {
                   

               
                if(response1.data.chartsDataPoints.length == 0)
                {
                    
                     $("#chart_div1").html("")
                    $("#no_chart1").html("No charts to dispaly")
                }
                else
                {
                  if($rootScope.user != undefined){google.charts.load('current', {'packages':['corechart']});}
              
                google.charts.setOnLoadCallback();
                  $("#no_chart1").html("");

                      // console.log(JSON.stringify(response.data.chartsDataPoints));
                     // google.charts.setOnLoadCallback();

                      google.charts.setOnLoadCallback(drawCharts);
                     // google.charts.load('current', {'packages':['corechart']});
                     
                      function drawCharts() {
                        var data1 = google.visualization.arrayToDataTable(changeChartData(response1.data.chartsDataPoints));
                         var titles = response1.data.chartsDataPoints[0].xTitle;

                        var options = {
                          title:  titles.toUpperCase(),
                          hAxis: {title: 'Time'},
                          vAxis: {title: 'Rating'},
                          legend: { position: 'right' },
                          lineWidth: 2,
                           width:600,
                        height:300,
                          trendlines: { 0: {} }    // Draw a trendline for data series 0.
                        };

                        var chart1 = new google.visualization.ScatterChart(document.getElementById('chart_div1'));
                       
                        chart1.draw(data1, options);

                      }

                }

                
              }
              else
              {
                    $("#chart_div1").html("")
                    $("#no_chart1").html("No charts to dispaly")

              }
      }
       else  
      {
        if(response1.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(response1.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(response1.msg);
      }
  }
}



//end google charts render

//Chart2
$scope.drawGoogleChart2 = function(response2){
  
        if(response2.status == 'SUCCESS')
        {  

          if(response2.data != []){
           //console.log($scope.selectedId);
                      // google.charts.setOnLoadCallback();

             
              if(response2.data.chartsDataPoints.length == 0)
              {
                  
                   $("#chart_div2").html("")
                  $("#no_chart2").html("No charts to dispaly")
              }
              else
              {
                $("#no_chart2").html("");
                // if($rootScope.user != undefined){google.charts.load('current', {'packages':['corechart']});}
          
                    // console.log(JSON.stringify(response.data.chartsDataPoints));
                    google.charts.setOnLoadCallback();

                    google.charts.setOnLoadCallback(drawCharts2);
                    
                  // google.charts.load('current', {'packages':['corechart']});
                    function drawCharts2() {
                    
                     var data2 = google.visualization.arrayToDataTable(changeChartDatatoFloat(response2.data.chartsDataPoints[0].rows));
                    
                      var options = {
                        title:  'OBJECTIVE',
                        hAxis: {title: 'Time'},
                        vAxis: {title: 'Rating'},
                       
                        legend: {position: 'right', textStyle: {fontSize: 10} },
                        lineWidth: 2,
                        width:600,
                        height:300,
                        trendlines: { 0: {} }    // Draw a trendline for data series 0.
                      };

                      var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));
                     
                      chart2.draw(data2, options);

                    }

              }

              

          }else
          {
              $("#no_chart2").html("No charts to dispaly")
          }
        }
       else  
      {
        if(response2.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(response2.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(response2.msg);
      }
  }
}

//Chart 3
$scope.drawGoogleChart3 = function(response){
  
        if(response.status == 'SUCCESS')
        {
                  //console.log($scope.selectedId);
          if(response.data.length != 0){  
            // google.charts.setOnLoadCallback();

         
          if(response.data.chartsDataPoints.length == 0)
          {
              
               $("#chart_div3").html("")
              $("#no_chart3").html("No charts to dispaly")
          }
          else
          {
            $("#no_chart3").html("");
            //if($rootScope.user != undefined){google.charts.load('current', {'packages':['corechart']});}
         
                // console.log(JSON.stringify(response.data.chartsDataPoints));
                google.charts.setOnLoadCallback();

                google.charts.setOnLoadCallback(drawCharts3);
                //google.charts.load('current', {'packages':['corechart']});
               
                function drawCharts3() {
                  var data = google.visualization.arrayToDataTable(changeChartData(response.data.chartsDataPoints));
                   var titles = response.data.chartsDataPoints[0].xTitle;

                  console.log(changeChartData(response.data.chartsDataPoints));
                  var options = {
                    title:  titles.toUpperCase(),
                    hAxis: {title: 'Time'},
                    vAxis: {title: 'Rating'},
                    legend: { position: 'bottom' },
                    lineWidth: 2,
                     width:600,
                        height:300,
                    trendlines: { 0: {} }    // Draw a trendline for data series 0.
                  };

                  var chart3 = new google.visualization.ScatterChart(document.getElementById('chart_div3'));
                 
                  chart3.draw(data, options);

                }

          }

          }else
          {
               $("#chart_div3").html("")
              $("#no_chart3").html("No charts to dispaly")
          }

      }
       else  
      {
        if(response.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(response.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(response.msg);
      }
  }
}


$scope.avgchartRender = function(id)
{
        var reqData = {}
        reqData.pinnee = id;
        
        userService.getAvgChartData(reqData, $scope.drawGoogleChart1)
       
}
$scope.avgchartObjectRender = function(id)
{
        var reqData = {}
        reqData.pinnee = id;
      
        userService.getAvgObjectiveChartData(reqData, $scope.drawGoogleChart2)
       
}

$scope.avgchartObjectOnlyRender = function(id,obj)
{
        var reqData = {}
        reqData.pinnee = id;
        reqData.objective = obj;
        
        userService.getAvgPerObjectiveChartData(reqData, $scope.drawGoogleChart3)
       
}
 
$scope.ObjectiveSel = function(Objid)
{
   var objSelId = Objid;//$('option:selected', $('#selectedEmp')).val();
   var id = $('#someId').text();
  
   $scope.avgchartObjectOnlyRender(id,objSelId);
 
}

$scope.sendId = function(id,roleId)
{
  $('#someId').html(id);
  $('#roleIds').val(roleId);
 // $('.tab-container .tab-left .tab-pane #tab3-1').addClass('active');
  //alert( $('#roleIds').text());
  /*alert($('#roleIds').val())*/
  $scope.showDiv = false;
  if($('#roleIds').val() === '1')
  {
    $scope.showDiv = true;
  }
  else
  {
    $scope.showDiv = false;
  }
  
}

/****************** EXPORT HISTORY *********************/


$scope.exportHistorySuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
           
              //$state.reload();  
              resetAlert();
             alertify.alert('Exported PINS successfully check your mail', function (e) {
                    if (e) {


                    $.unblockUI();
                    //$state.go('chome', {},{reload:true});
                   // $scope.getByPins();
                    
                    }
                  });
              $scope.openPinWizard= false;
              $scope.getPinHistory($scope.selDisplay.id);
     
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          //$state.reload();
          resetAlert();
          alertify.alert('Try Again', function (e) {
                    if (e) {


                    $.unblockUI();
                    //$state.go('chome', {},{reload:true});
                   // $scope.getByPins();
                    
                    }
                  });
              $scope.openPinWizard= false;
              $scope.getPinHistory($scope.selDisplay.id);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}



$scope.exportHistory = function(id){
      $.blockUI({ message: '<h4> Exporting Pins...</h4>' });
      var reqData = {};
      reqData.pinnee = id;
     // reqData.whoAmI = localStorageService.get('userData').id;


      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
     
      userService.exportHistoryToDB(reqData, $scope.exportHistorySuccess);

}

$scope.objListforPinnee = [];
$scope.objectListforPinnee = function(id)
{
   var req = {};
   req.pinnee=parseInt(id);
   userService.getobjectivesParPinnee(req,function(resp){
      if(resp.data != [])
      {
        $scope.objListforPinnee = resp.data ;
      }
      else
      {
        $scope.objListforPinnee = [];
      }
   });
}


//All Charts Ended here///

  /*    GET ALL USERS   */


$scope.getAllUserSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
        $.unblockUI();
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            $scope.allEmpList = resData.data;
            //console.log('all users list '+JSON.stringify($scope.allEmpList))
              $scope.selDisplay = resData.data[0];
             
              
              $scope.getMagicAPI(resData.data[0].id)
            
              $scope.getPinHistory(resData.data[0].id);
              angular.forEach($scope.allEmpList, function(value, key){
              $scope.fullname = value.firstname+' '+value.lastname;
              $scope.empFinalList = value;
              //console.log(JSON.stringify($scope.empFinalList));
            });
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

         // $state.reload();
          resetAlert();
          alertify.alert(resData.msg,function (e) {
                    if (e) {


                    //$.unblockUI();
                    $state.go('chome', {},{reload:true});
                    }
                  });
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
} 

 $scope.getAllUsers = function() {
        $.blockUI({ message: '<h4> Loading...</h4>' });

      var reqData = {};
      reqData.skipMe = true;
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      
     
      userService.getUsersForConsumer(reqData,$scope.getAllUserSuccess);
  }

 $scope.selectedOneOnly = true;
  $scope.select= function(index,item) {
       $scope.selected = index; 
       $scope.selDisplay = item;
       $scope.editMode = false
        $scope.addingPinprocess = false;
       
        $scope.selectedOneOnly = false;
       $scope.openPinWizard= false;
       $('.tab-container').tabs();
       $('.tab-container li').removeClass('active');
       $('.tab-container .history').addClass('active');
       $('.tab-container').tabs({active:0});
       //$("#ul.tabs").tabs("div.panes > div", { history: true });
       console.log(JSON.stringify($scope.selDisplay))
       return $scope.selDisplay; 

  };
/*  GET ALL USERS  */

//Chart Call
$scope.chartAvail = function(id)
{    
    $('.no-chart').show();
    var empId= parseInt(id);
    $scope.avgchartRender(empId);
    //$scope.avgchartObjectRender(empId);
   // $scope.objectListforPinnee(empId);
}


//Ends here chart calling
 $scope.rating =function(){
    if($rootScope.allRatings != [] && localStorageService.get('ratings') != [] )
    {
     // console.log(JSON.stringify(localStorageService.get('ratings')));
  var stepsArray = changeRatingarray(localStorageService.get('ratings'));
  // console.log(JSON.stringify(stepsArray));
      $scope.slider = {
            value: 0,
            options: {
              showTicksValues: true,
              stepsArray: stepsArray
           } 
        };
    }
}


/*  GET META  */
 $scope.allTagsselected =[];
$scope.getMetaSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {

          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          if (resData.data.length != 0) {
            $scope.allTags = resData.data.tagsList;
            $scope.allObj = resData.data.objectiveList;
            $scope.allRatings = resData.data.ratingList;
            $scope.ratingLength = parseInt($scope.allRatings.length) - 1;
            $rootScope.allRatings = resData.data.ratingList;
            localStorageService.set('ratings',$rootScope.allRatings);

            angular.forEach($scope.allTags, function(value, key){
              $scope.objRelatedToTag = value.objectivesList;
              //console.log('objRelatedToTag '+JSON.stringify(value));
             if(value.objectivesList.length > 0)
             {

              $scope.allTagsselected.push(value)
             }
            });

          //  console.log(JSON.stringify($scope.allTagsselected))
          }
          else{
            resetAlert();
            alertify.alert('Tags not found.')
          }  
          $scope.getAllUsers();
      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          resetAlert();
          alertify.alert(resData.msg);
          $scope.getAllUsers();
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
} 

 $scope.getMeta = function() {
      
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.getMetaData(reqData,$scope.getMetaSuccess);
  }

 if(localStorageService.get('userData'))
 {
  $scope.getMeta();
 }
 else
 {
   $state.go('login', {},{'reload':true}); 
 }

/*  GET META  */

/****************** GET MAGIC API *********************/
$scope.chartShow = false;
  $scope.showChart = false;
$scope.showSlider = false;
$scope.showExport = false;
$scope.getMagicAPI = function(e_id){

      var reqData = {};
      reqData.pinner = localStorageService.get('userData').id;
      reqData.pinnee = e_id;

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      console.log('ftats '+JSON.stringify(reqData))
    
      userService.magicAPI(reqData, function(resData) {
        
      if(resData.status == "SUCCESS")
      {
         //console.log('sd'+JSON.stringify($scope.allTagsselected))
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            if(resData.data.userRelationship && resData.data.userRelationship != undefined)
            {
              $scope.hasRelation = true;
              //$scope.save.rad1 = resData.data.userRelationship.user2_rel_role_description;
            }
            else{
              $scope.hasRelation = false;
            }


          if(resData.data.length != 0)
          {
            $scope.userRelationship = (resData.data.userRelationship != undefined ?resData.data.userRelationship:[]);
            $scope.editRelationsList = resData.data.editRelationsList;
         console.log('userRelationship '+JSON.stringify($scope.userRelationship.user2));
           // console.log('editRelationsList '+JSON.stringify($scope.editRelationsList.id));
           if($scope.userRelationship != undefined ){
            //angular.forEach($scope.userRelationship, function(value, key){
             
             var strVale = $scope.userRelationship.user2_tags?$scope.userRelationship.user2_tags:null;
             if(strVale != undefined )
            { if(strVale.indexOf(',') != -1 ){
              var resultArray = [parseInt(strVale)];
             // var resultArray =strVale.split(",").map(Number);

              var allTagsselected = modifytags($scope.allTagsselected,resultArray);
           console.log("news"+JSON.stringify($scope.allTagsselected));
             
             }else{

             var resultArray = strVale.split(',').map(function(strVale){return Number(strVale);});
           
           var allTagsselected = modifytags($scope.allTagsselected,resultArray);
           console.log("new"+JSON.stringify($scope.allTagsselected));
           }
         }else
         {

          var allTagsselected = $scope.allTagsselected;
           /* var d = $("input[type=radio][name='rad1']:checked").val();
             if(d != undefined)
             {
              $('#fstNxtbtn').removeAttr('disabled');
             }
             else
             {
              $('#fstNxtbtn').attr('disabled',true)
             }*/
           
         }
          }else
          {
              console.log('userRelationship '+JSON.stringify($scope.userRelationship));
          }

            if(resData.data.isChartViewPossible == true)
            {
              $scope.showChart = true;
              $scope.chartAvail(e_id);
            }
            else
            {
             $scope.showChart = false;
              $('.showNochart').show();
              $('#showNochart').html('No charts available');
            }
            if(resData.data.isRatingPossible == true)
            {

               $scope.showSlider = true;
               $scope.rating();
            }else
            {
               $scope.showSlider = false;
            }

             if(resData.data.isExportPinsPossible == true)
            {

               $scope.showExport = true;
              
            }else
            {
               $scope.showExport = false;
            }

            if ($scope.userRelationship == undefined && $scope.editRelationsList == undefined) 
            {
              //$state.reload();  
              resetAlert();
              alertify.alert('You cannot pin yourself.',function (e) {
                    if (e) {


                    //$.unblockUI();
                    $state.go('chome', {},{reload:true});
                    }
                  });
            }
            
          }
          else{
            resetAlert();
            alertify.alert('You cannot pin yourself.')
          }
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          //$state.reload();
          resetAlert();
          alertify.alert(resData.msg,function (e) {
                    if (e) {


                    //$.unblockUI();
                    $state.go('chome', {},{reload:true});
                    }
                  });
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
});

}

$scope.checkforslider=function(){
$scope.save.pinneeRelationship =$("input[type=radio][name='rad1']:checked").val();

if($scope.save != null && $scope.save.pinneeRelationship == '3')
            {
               $scope.showSlider = true;
              $scope.rating();
            }
}


/*****************  Adding Pin ******************/

$scope.savePinSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
             // $state.reload();  
              resetAlert();
              alertify.alert('PIN saved successfully', function (e) {
                    if (e) {


                    $.unblockUI();
                    $state.reload(); 
                   //$state.go('chome', {},{reload:true});
                   // $scope.getByPins();
                    
                    }
                  });
              $scope.openPinWizard= false;
              $scope.addingPinprocess= false;

              $scope.getPinHistory($scope.selDisplay.id);
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          //$state.reload();
          resetAlert();
          alertify.alert(resData.msg,function (e) {
                    if (e) {

                     // $state.reload();
                    $.unblockUI();
                    //$scope.getByPins();
                    
                    }
                  });
          $scope.openPinWizard= false;
           $scope.getPinHistory($scope.selDisplay.id);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}


$scope.savePin = function() {
var ratevalue = $('#ratingTextValue').text();
  console.log(ratevalue)
  if(ratevalue == 'NaN')
  {
    var rating = 0;
   
  }
   else
   {
    $scope.save.rating = parseInt($('#ratingTextValue').text());
        if(localStorageService.get('ratings') != []){
         
                 var rateId = getSliderId($scope.save.rating,localStorageService.get('ratings'));
               
                 console.log(rateId);
                var rating = parseInt(rateId);
              }
             
    }
  newItem.pinner = localStorageService.get('userData').id;
  newItem.pinnee = $scope.selDisplay.id;
  newItem.pinneeRelationship= parseInt($scope.save.pinneeRelationship);
  newItem.event = $scope.save.event;
  newItem.comment = $scope.save.comment;
  newItem.ratingSet = 1; // for now this will always be 1, later we shall have to change it
  newItem.rating = rating;
  newItem.tag = $scope.save.tags;
  newItem.objective = parseInt($scope.save.objective);
  newItem.pinDate = $scope.save.pinDate;

  $scope.item.push(newItem);
 
  var reqData = {};
  reqData = $scope.item[0];

  $http.defaults.headers.common = { 'Authorization' : undefined};      
  $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
  $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
  console.log('ftats '+JSON.stringify(reqData))
  $.blockUI({ message: '<h4> Saving Pin...</h4>' });

  userService.savePinToDB(reqData, $scope.savePinSuccess);
  
};

//getting checked tags
$('#check-button').click(function() {
$scope.save.tags=($('.Checkbox:checked').map(function() {
    return this.value;
}).get().join(', '));

console.log('meow '+$('.Checkbox:checked').val())

});


$scope.tagsForDisplay = {};
$scope.objForDisplay = {};
$scope.relForDisplay = {};
$scope.showPreview = function(){

  $scope.relForDisplay = $('.Relation:checked').next().text();

  $scope.tagsForDisplay = ($('.Checkbox:checked').map(function() {
     return $(this).next("label").text().split(' ').join(',');
  }).get()).toString();

  $scope.objForDisplay = $('.objktive:checked').next().text();
  console.log( $scope.objForDisplay + ' lala '+ $scope.tagsForDisplay + 'lala' + $scope.relForDisplay)

}


/************** UPDATE RELATIONSHIP **************/
$scope.updateUserRelSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          //$state.reload();
          $scope.editMode = false;
          resetAlert();
          alertify.alert('Relationship updated.',function (e) {
                    if (e) {


                    $.unblockUI();
                    
                    }
                  });
                $scope.openPinWizard= false;
                $scope.getPinHistory($scope.selDisplay.id);
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          //$state.reload();
          resetAlert();
          alertify.alert('Relationship could not be updated.',function (e) {
                    if (e) {


                    $.unblockUI();
                    //$state.go('chome', {},{reload:true});
                    }
                  });
                $scope.openPinWizard= false;
                $scope.getPinHistory($scope.selDisplay.id);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}

$scope.updateUserRel = function(){
   $.blockUI({ message: '<h4> Updating...</h4>' });
      $scope.selDisplay.editedVal= $('#editedVal').val();
      var reqData = {};
      reqData.user1 = localStorageService.get('userData').id;
      reqData.user2 = $scope.selDisplay.id;
      reqData.user2_rel_role_id = parseInt($scope.selDisplay.editedVal);

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      console.log('ftats '+JSON.stringify(reqData))
     
      userService.updateRelFromDB(reqData, $scope.updateUserRelSuccess);

}



/****************  Pin History *****************/


$scope.getPinHistorySuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          if (resData.data ) {
            $.unblockUI();
              if(resData.data.pins != undefined && resData.data.pins.length != 0){
                $scope.pins = resData.data.pins;
                $scope.havePinned = true;
                 $scope.openPinWizard= false;
              }
              else{
                $scope.havePinned = false;
                 $scope.openPinWizard= false;

              }

              if(resData.data.otherPins != undefined && resData.data.otherPins.length != 0){
                $scope.otherPinned = true;
                $scope.otherPins = resData.data.otherPins;
                 $scope.openPinWizard= false;
              }
              else{
                $scope.otherPinned = false;
                 $scope.openPinWizard= false;
              }


             //$scope.pins = resData.data.pins;
             //$scope.otherPins = resData.data.otherPins;
             //console.log(''+JSON.stringify($scope.pinHistory));


            angular.forEach($scope.byList, function(value, key){
              //console.log(JSON.stringify($scope.byList));
            });
          }

          else{
            resetAlert();
            alertify.alert('No pins found.')
          }
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{

          resetAlert();
          alertify.alert(resData.msg);
      }
  }
} 

$scope.activeTab = function()
{
     $('.tab-container li').removeClass('active');
     $('.tab-container .performance').addClass('active');
}

  $scope.getPinHistory = function(emp_id) {
      var reqData = {};
      $.blockUI({ message: '<h4> Loading Pins...</h4>' });
      reqData.pinnee = emp_id;
      //reqData.pinner = localStorageService.get('userData').id;
      reqData.whoAmI = localStorageService.get('userData').id;
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      console.log('user data for history '+JSON.stringify(reqData))
  
      userService.getAllPins(reqData,$scope.getPinHistorySuccess);
  }



/********************* Get objectives by TAG ID *********************/

$scope.relatedObj = [];
$scope.foundObj = false;

$scope.getRelatedObjSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          if (resData.data.length != 0) {

              $scope.relatedObj = resData.data
              $scope.foundObj = true;

            angular.forEach($scope.relatedObj, function(value, key){
              //console.log(JSON.stringify($scope.byList));
            });

            console.log('ja '+JSON.stringify($scope.relatedObj))
            return $scope.relatedObj;
          }

          else
          {
              $scope.foundObj = false;
          }
      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          
          resetAlert();
          alertify.alert(resData.msg);
      }

      else
      {
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert(resData.msg);

          $state.go('login', {},{'reload':true}); 
      }
  }
} 


  $scope.getRelatedObj = function() {

      var reqData = {};
      reqData.tags = $scope.save.tags;
      
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      //console.log('getRelatedObj  '+JSON.stringify(reqData))
  
      userService.getObjByTag(reqData,$scope.getRelatedObjSuccess);
  }


        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
app.filter('moment', [
    function () {
      return function (date, method) {
        var momented = moment(date);
        return momented[method].apply(momented, Array.prototype.slice.call(arguments, 2));
      };
    }
  ])