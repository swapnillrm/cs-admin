'use strict';
var pin_app =  angular.module( 'pinApp', ['ui.router',
                                                    'pinLoginCtrl.controllers',
                                                    'LocalStorageModule',
                                                    'pinApp.services',
                                                    'pinApp.directives',
                                                    'lpinMenu.controllers',
                                                    'pinObjCtrl.controllers',
                                                    'pinHomeCtrl.controllers',
                                                    'pinRatingsCtrl.controllers',
                                                    'pinTagsCtrl.controllers', 
                                                    'pinSettingsCtrl.controllers',
                                                    'pinReportCtrl.controllers',
                                                    'pinConsumerProfileCtrl.controllers',
                                                    'pinConsumerHomeCtrl.controllers',
                                                    'pinConsumerByMePinsCtrl.controllers',
                                                    'pinConsumerForMePinsCtrl.controllers',
                                                    'pinConsumerExportHistoryCtrl.controllers',
                                                    'pinConsumerNotificationsCtrl.controllers',
                                                    'pinResetPassword.controllers',
                                                    'pinChartsCtrl.controllers',
                                                    'pinChartsObjCtrl.controllers',
                                                    'pinChartsperRatingCtrl.controllers',
                                                    'pinchangePwdCtrl.controllers',
                                                    'angularMoment',
                                                    'angular-slidezilla',
                                                    'ui.bootstrap',
                                                    'flow',
                                                    'base64',
                                                    'ngSanitize',
                                                    'ui.select',
                                                    'rzModule'
                                                    
                                                  
                                                    ])

pin_app.config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('ls');
        localStorageServiceProvider.setNotify(true, true);

    }]);



pin_app.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        singleFile: true
    };
}])

pin_app.config(function($provide) {
    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceReload = function() {
            return $delegate.go($delegate.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });
});
pin_app.config(function($stateProvider, $urlRouterProvider,$httpProvider,$locationProvider) {
  //  delete $httpProvider.defaults.headers.common['X-Requested-With'];
     /* $httpProvider.defaults.headers.post['Accept'] = 'application/json,text/javascript';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.post['Access-Control-Max-Age'] = '1728000';
    $httpProvider.defaults.headers.post['Access-Control-Allow-Origin'] = "*";
    $httpProvider.defaults.headers.post['Access-Control-Allow-Headers'] = "*";
    $httpProvider.defaults.headers.common['Accept'] = 'application/json,text/javascript';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
   $httpProvider.defaults.headers.common['Access-Control-Request-Headers'] = "X-Requested-With";
   */

     delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.post['Accept'] = 'application/json,text/javascript';

    
    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
   
    $httpProvider.defaults.useXDomain = true;
   
   
        $stateProvider
            .state('login', {
                url:'/login',
                templateUrl:'partials/login.html',
                controller: 'LoginCtrl',
                restricted: false,
                reload:true
              
                /* data: {
                        title: false,
                         breadcrumbs : false
                      }*/
               
             })
         
             .state('objective', {
                url:'/objective',
                templateUrl:'partials/objective.html',
                controller: 'objectiveCtrl',
                restricted: true
               /* reload:true*/

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
              .state('home', {
                url:'/home',
                templateUrl:'partials/home.html',
                controller: 'homeCtrl',
                restricted: true
                 /* reload:true*/

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
               .state('ratings', {
                url:'/ratings',
                templateUrl:'partials/ratings.html',
                controller: 'ratingsCtrl',
                restricted: true
                 /* reload:true*/

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('tags', {
                url:'/tags',
                templateUrl:'partials/tags.html',
                controller: 'tagsCtrl',
                restricted: true
                 /* reload:true*/

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                 .state('settings', {
                url:'/settings',
                templateUrl:'partials/settings.html',
                controller: 'settingsCtrl',
                restricted: true 

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('report', {
                url:'/report',
                templateUrl:'partials/report.html',
                controller: 'reportCtrl',
                restricted: true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('chome', {
                url:'/chome',
                templateUrl:'partials/consumerHome.html',
                controller: 'consumerHomeCtrl',
                restricted: true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('profile', {
                url:'/profile',
                templateUrl:'partials/consumerProfile.html',
                controller: 'consumerProfileCtrl',
                restricted: true
                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('bypins', {
                url:'/bypins',
                templateUrl:'partials/consumerByMePins.html',
                controller: 'consumerPinsMyMeCtrl',
                restricted: true
                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })

                .state('forpins', {
                url:'/forpins',
                templateUrl:'partials/consumerForMePins.html',
                controller: 'consumerPinsForMeCtrl',
                restricted: true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })

                .state('export', {
                url:'/export',
                templateUrl:'partials/consumerExportHistory.html',
                controller: 'consumerPinsExportHistoryCtrl',
                restricted: true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('notifications', {
                url:'/notifications',
                templateUrl:'partials/consumerNotifications.html',
                controller: 'consumerNotificationsCtrl',
                restricted: true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('resetpassword', {
                url:'/resetpassword',
                templateUrl:'partials/resetPassword.html',
                controller: 'ctrResetPassword',
                restricted: true,
                reload:true
             })

                .state('charts', {
                url:'/charts',
                templateUrl:'partials/charts.html',
                controller: 'chartsCtrl',
                restricted: true,
                reload:true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('chartsobj', {
                url:'/chartsobj',
                templateUrl:'partials/charts1.html',
                controller: 'chartsCtrlObjective',
                restricted: true,
                reload:true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('chartsPerObj', {
                url:'/chartsPerObj',
                templateUrl:'partials/charts2.html',
                controller: 'chartsperRatingCtrl',
                restricted: true,
                reload:true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
                .state('changepwd', {
                url:'/changepwd',
                templateUrl:'partials/changePwd.html',
                controller: 'changePwdCtrl',
                restricted: true,
                reload:true

                /* breadcrumb: {
                    title: 'Objective'
                    
                  },*/
               
             })
            .state('error', {
                url: '/error',
                templateUrl: 'partials/error.html',           
              
            })

        /* $locationProvider.html5Mode(true);/* use this line if u need to remove hash from url
*/ /*if(window.history && window.history.pushState){
            //$locationProvider.html5Mode(true); will cause an error $location in HTML5 mode requires a  tag to be present! Unless you set baseUrl tag after head tag like so: <head> <base href="/">

         // to know more about setting base URL visit: https://docs.angularjs.org/error/$location/nobase

         // if you don't wish to set base URL then use this
         $locationProvider.html5Mode({
                 enabled: true,
                 requireBase: false
          });
        }*/
   $urlRouterProvider.otherwise('/chome');
   $httpProvider.interceptors.push('APIInterceptor');
});


pin_app.run(['$rootScope', '$location', function($rootScope, $location){
   var path = function() { return $location.path();};
   $rootScope.$watch(path, function(newVal, oldVal){
     $rootScope.activetab = newVal;
   });
}]);

pin_app.run(function($rootScope, localStorageService) {
    if (!localStorageService.isSupported) {
        $rootScope.storageType = 'cookieStorage';
    }
    else if (localStorageService.getStorageType().indexOf('session') >= 0) {
        $rootScope.storageType = 'sessionStorage';
    }
    else {
        $rootScope.storageType = 'localStorage';
    }
});

pin_app.run(function($rootScope, localStorageService,$http,$location) {
    if (localStorageService.userData) {
        $http.defaults.headers.common.Authorization = 'Basic ' + localStorageService.userData.authToken;
    }


    
});



pin_app.run(function($rootScope, $location,$window) {
   
    $rootScope.$on('$locationChangeSuccess', function() {
        if($rootScope.previousLocation == $location.path()) {
            console.log("Back Button Pressed");
             // window.onbeforeunload = function() { redirect(window.history.back(1)); }; 
              $rootScope.$watch(function() { return $location.path() },
        function(newLocation, oldLocation) {
            if($rootScope.isAdmin){
                  if($rootScope.actualLocation == newLocation && $rootScope.previousLocation == '/login')
                   {
                        //$location.path('newLocation');
                    //  manageMenuBar(true);
                      //$location.path('/home');
                    location.reload(true);
                       //window.onbeforeunload = function() { redirect(window.history.back(1)); }; 
                      //window.history.forward();
                }
            }
            else if($rootScope.actualLocation == newLocation && $rootScope.previousLocation == '/login') {
               //  $location.path('/chome');
                //$window.location.reload();
               // manageMenuBar(true);
               //$location.path('/chome');
                location.reload(true);
            }
            else 
            {

                if($rootScope.actualLocation == '/resetpassword')
                 $location.path('/resetpassword');

            }
           
            
            
          
        }); 
        }
        $rootScope.previousLocation = $rootScope.actualLocation;
        $rootScope.actualLocation = $location.path();
    });
    
     
   /* $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
       
        if(error.status == 401){
            console.log("401 detected. Redirecting...");

            authService.deniedState = toState.name;
            alert(message);
            $state.go("login");
        }
    });*/


});
/*
*/
pin_app.factory('authHttpResponseInterceptor',['$q','$location',function($q,$location){
    return {
        response: function(response){
            if (response.status === 401) {
                console.log("Response 401");
               
                $.unblockUI();
                $location.path('/login');
                location.reload();
            }
            return response || $q.when(response);
        },
        responseError: function(rejection) {
            if (rejection.status === 401) {
                console.log("Response Error 401",rejection);
               // alert(rejection.statusText);
                $.unblockUI();
                localStorage.clear();
                $location.path('/login');
                location.reload();
            }
            return $q.reject(rejection);
        }
    }
}])
pin_app.config(['$httpProvider',function($httpProvider) {
    //Http Intercpetor to check auth failures for xhr requests
    $httpProvider.interceptors.push('authHttpResponseInterceptor');
}]);















