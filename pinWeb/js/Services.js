/*
  Version 0.0.1
  Created by Mrunmayi.
*/
var app = angular.module('pinApp.services',['ngResource']);
//var live = "http://10.0.1.40:8181"; //saurabh left right mind internet
//var live = "http://10.10.10.181:8181"; //saurabh mobile id internet
//var live = "http://10.10.10.164:8181"; // shrish's mobile id
//var live = "http://10.0.1.14:8181"; // chetan's mobile id
//var live = "http://10.0.1.12:8181"; // chetan's mobile id
//var live = "http://10.10.10.56:8181"; // arjuns's mobile id
var live = "http://52.41.4.52:8080/gs-rest-service-0.1.0"; //live aws link
//var live = "http://10.10.10.129:8080/gs-rest-service-0.1.0"; //live aws link


var path = live;
/*========================User Services for Login =================================*/
app.service("userService", function($resource){    
    
    this.loginUser = function(reqData, nextStep){
       var src = $resource(path +'/auth/login' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 

        src.get({},
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
                         
          });
    };

     this.logoutUser = function(reqData, nextStep){
       var src = $resource(path +'/auth/logout' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      
        src.get({},
            function(result){
                nextStep.call(this, result)
          });
    };

    this.getNotifications = function(reqData, nextStep){
       var src = $resource(path +'/notifications/getallnotifications' ,{},{'post':{method:'POST'}}); 
        src.post({},
            function(result){
                nextStep.call(this, result)
          });
    };
 this.getunreadNotifications = function(reqData, nextStep){
       var src = $resource(path +'/notifications/getunreadnotifications' ,{},{'post':{method:'POST'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    
    this.marknotification = function(reqData, nextStep){
       var src = $resource(path +'/notifications/marknotificationread' ,{},{'post':{method:'POST'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
 this.getUnreadNotificationsCount = function(reqData, nextStep){
       var src = $resource(path +'/notifications/getunreadnotificationcount' ,{},{'post':{method:'POST'}}); 
        src.post({},
            function(result){
                nextStep.call(this, result)
          });
    };
   
  this.getUsers = function(reqData, nextStep){
       var src = $resource(path +'/user/getallusers' ,{'skipMe':true},{'get':{method:'GET'}}); 
      
        src.get('',
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
              });       
          };
this.getUsersById = function(reqData, nextStep){
       var src = $resource(path +'/user/getallusers' ,{},{'get':{method:'GET'}}); 
      
        src.get('',
            function(result){
                if(result.status == 401){
                  resetAlert()
                  alertify.alert(result.message)
                }
                else{
                  nextStep.call(this, result);
                }
              });       
          };

          this.getUsersForConsumer = function(reqData, nextStep){
       var src = $resource(path +'/user/getallusers' ,{'skipMe':true,'skipDisabled':true},{'get':{method:'GET'}}); 
      
        src.get('',
            function(result){
                if(result.status == 401){
                  resetAlert();
                  alertify.alert(result.message);
                }
                else{
                  nextStep.call(this, result);
                }
              });       
          };
   
this.getUsersCount = function(reqData, nextStep){
       var src = $resource(path +'/user/getusercount' ,{},{'get':{method:'GET'}}); 
      //$http.defaults.headers.common['Authorization'] = 'Basic ' + reqData; 
        src.get('',
            function(result){
                nextStep.call(this, result)
                         
          });
    };

  this.getFavourites = function(reqData, nextStep){
       var src = $resource(path +'/user/getfavourites' ,{},{'get':{method:'GET'}}); 
      //$http.defaults.headers.common['Authorization'] = 'Basic ' + reqData; 
        src.get('',
            function(result){
                nextStep.call(this, result)
                         
          });
    };

  this.getAllPins = function(reqData, nextStep){
       var src = $resource(path +'/pin/getuserpins' ,{},{'post':{method:'POST'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
                         
          });
    };

    this.getForMePins = function(reqData, nextStep){
       var src = $resource(path +'/pin/getpinstome' ,{},{'post':{method:'POST'}}); 
        src.post('',
            function(result){
                nextStep.call(this, result)
          });
    }; 

  this.getObjectives = function(reqData, nextStep){
       var src = $resource(path +'/misc/getobjectives' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      //$http.defaults.headers.common['Authorization'] = 'Basic ' + reqData; 
        src.get('',
            function(result){
                nextStep.call(this, result)
          });
  };

 this.gettagsAcctoObj = function(reqData, nextStep){
       var src = $resource(path +'/misc/getalltagforobj' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      //$http.defaults.headers.common['Authorization'] = 'Basic ' + reqData; 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
  };
 /*   this.getObjByTag = function(reqData, nextStep){
       var src = $resource(path +'/objective/getobjbytags' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    }; */

this.getObjByTag = function(reqData, nextStep){
       var src = $resource(path +'/misc/getobjbytags' ,{},{'post':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
  this.getMetaData = function(reqData, nextStep){
       var src = $resource(path +'/misc/getmeta' ,{},{'get':{method:'GET'}}); 
      //$http.defaults.headers.common['Authorization'] = 'Basic ' + reqData; 
        src.get('',
            function(result){
                nextStep.call(this, result)
                         
          });
    };


    
    this.registerNewUser = function(reqData, nextStep){
       var src = $resource(path +'/user/addemployee' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.uploadProfile = function(reqData, nextStep){
       var src = $resource(path +'/user/uploadprofileimage' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.deactivateUser = function(reqData, nextStep){
       var src = $resource(path +'/user/deactivateemployee' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.activateUser = function(reqData, nextStep){
       var src = $resource(path +'/notifications/test/activateuser' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updateUserFromDB = function(reqData, nextStep){
       var src = $resource(path +'/user/updateuserprofile' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
      
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };


    this.addNewTag = function(reqData, nextStep){
       var src = $resource(path +'/misc/addtag' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.deleteTagFromDB = function(reqData, nextStep){
       var src = $resource(path +'/misc/deletetag' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.updateTagInDB = function(reqData, nextStep){
       var src = $resource(path +'/misc/updatetag' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.addNewObj = function(reqData, nextStep){
       var src = $resource(path +'/misc/addobjective' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.deleteObjFromDB = function(reqData, nextStep){
       var src = $resource(path +'/misc/deleteobjective' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updateObjInDB = function(reqData, nextStep){
       var src = $resource(path +'/misc/updateobjectivetags' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.addNewRating = function(reqData, nextStep){
       var src = $resource(path +'/misc/addrating' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.deleteRatingFromDB = function(reqData, nextStep){
       var src = $resource(path +'/misc/deleterating' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updateRatingInDB = function(reqData, nextStep){
       var src = $resource(path +'/misc/updaterating' ,{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.magicAPI = function(reqData, nextStep){
       var src = $resource(path +'/userrelationships/getmagic',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.savePinToDB = function(reqData, nextStep){
       var src = $resource(path +'/pin/addpin',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updatePasswordfromMail = function(reqData, nextStep){
       var src = $resource(path +'/openservices/updatepasswordusingtoken',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
     this.changepassword = function(reqData, nextStep){
       var src = $resource(path +'/user/updateuserpassword',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.updateRelFromDB = function(reqData, nextStep){
       var src = $resource(path +'/userrelationships/updateuserrelationship',{},{'save':{method:'POST'},headers: {'Content-Type': 'application/json; charset=UTF-8', 'Accept' :  '*.*'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.exportHistoryToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/producecsv' ,{},{'save':{method:'POST'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
  this.importHistoryToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/uploadcsv' ,{},{'save':{method:'POST'},headers: {'Content-Type': undefined}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.importTagsToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/uploadtagcsv' ,{},{'save':{method:'POST'},headers: {'Content-Type': undefined}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

    this.importObjToDB = function(reqData, nextStep){
       var src = $resource(path +'/syscon/uploadobjectivecsv' ,{},{'save':{method:'POST'},headers: {'Content-Type': undefined}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

   this.getChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/getdata' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

 this.getAvgChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/avg' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

     this.getAvgObjectiveChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/avgobjseries' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };

      this.getAvgPerObjectiveChartData = function(reqData, nextStep){
       var src = $resource(path +'/charts/ratingsperobjective' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
      this.getobjectivesParPinnee = function(reqData, nextStep){
       var src = $resource(path +'/charts/avgobjseriesobjs' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.sendEmailInlink = function(reqData, nextStep){
       var src = $resource(path +'/openservices/forgotpassword' ,{},{'post':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.post(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.updateSettings = function(reqData, nextStep){
       var src = $resource(path +'/syscon/updatesettings' ,{},{'save':{method:'POST'},headers: {'Content-Type':'application/json'}}); 
        src.save(reqData,
            function(result){
                nextStep.call(this, result)
          });
    };
    this.getAllsettings = function(reqData, nextStep){
       var src = $resource(path +'/syscon/getsettings' ,{},{'get':{method:'GET'},headers: {'Content-Type': 'application/json'}});  
      
        src.get({},
            function(result){
               
                  nextStep.call(this, result);
                
              });       
          };


}) 


/*========================User Services =================================*/
  app.service('APIInterceptor', function($rootScope, $location) {
    this.responseError = function(response) {
    //  console.log(JSON.stringify(response));
        if (response.data.status === 401) {
            $rootScope.$broadcast('unauthorized');
           // alert(response.data.message);
            $location.path('/login');
        }
        return response;
    };
})



/*app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);*/