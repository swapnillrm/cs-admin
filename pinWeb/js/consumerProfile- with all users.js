var app = angular.module('pinConsumerProfileCtrl.controllers', []);

app.controller('consumerProfileCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {
  
  if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.req = {};
  $scope.save = {};
  $scope.selDisplay = {};
  $scope.slider1 = {};
  var newItem ={};

  
  $scope.item = [];
  $scope.userRelationship = [];
  $scope.editRelationsList = [];
  $scope.allEmpList = [];
  $scope.allTags = [];
  $scope.allRatings = [];
  $scope.checkedTag = [];


  $scope.hasRelation = false;
  $scope.selected = 0;
  $scope.activePart = 'step1';
  $scope.editMode = false;
  $scope.date = new Date();

    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

	console.log('We are in consumer');
  panelHeight(); //for window height
  datePicker(); // for date picker
  scrollPageToTop(); //scrolling page to top 

// opening the wizard to add pin
$scope.openPinWizard= false;

  $scope.addPin= function(){
    $scope.openPinWizard= true;
  }
  $scope.cancelPin= function(){
    $scope.openPinWizard= false;
  }

// taking to the next step
  $scope.showNextStep = function(step){
        $scope.activePart = step;
        $('.step-pane').addClass('active');
  }




/*  GET META  */

$scope.getMetaSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          if (resData.data.length != 0) {
            $scope.allTags = resData.data.tagsList;
            $scope.allObj = resData.data.objectiveList;
            $scope.allRatings = resData.data.ratingList;
            $scope.ratingLength = parseInt($scope.allRatings.length) - 1;

            //angular slider
             if ($scope.allRatings[0].ratingValue == 0) {

                 $scope.slider1 = {
                  val:$scope.allRatings[1].ratingValue,
                  min: $scope.allRatings[1].ratingValue,
                  max: $scope.allRatings[$scope.ratingLength].ratingValue,
                  step: 1
              };
            }
            else{
              $scope.slider1 = {
                  val:$scope.allRatings[0].ratingValue,
                  min: $scope.allRatings[0].ratingValue,
                  max: $scope.allRatings[$scope.ratingLength].ratingValue,
                  step: 1
              };
            }
            console.log('slide' + JSON.stringify($scope.slider1))



            //console.log('all rating only '+ JSON.stringify($scope.allRatings))
            

            angular.forEach($scope.allTags, function(value, key){
              $scope.objRelatedToTag = value.objectivesList;
             // console.log('objRelatedToTag '+JSON.stringify($scope.objRelatedToTag));
            });
          }
          else{
            resetAlert();
            alertify.alert('Tags not found.')
          }  
      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
} 

 $scope.getMeta = function() {
      
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.getMetaData(reqData,$scope.getMetaSuccess);
  }



$scope.myArray = {};

  /*    GET ALL USERS   */
$scope.getAllUserSuccess = function(resData) {


      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            $scope.allEmpList = resData.data;
            
              $scope.selDisplay = resData.data[0];

      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert('Unauthorized access.');
                  
          $state.go('login', {},{'reload':true})
      }
  }
} 



  $scope.select= function(index,item) {
       $scope.selected = index; 
       $scope.selDisplay = item;
       return $scope.selDisplay; 
  };

  $scope.getAllUsers = function() {
      
      var reqData = {};
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.getUsers(reqData,$scope.getAllUserSuccess);
  }
 





 /*    GET ALL FAVOURITES   */

$scope.getAllFavouriteSuccess = function(resData1) {
$scope.myArray = resData1;
console.log(JSON.stringify($scope.myArray));
      if(resData1.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          if (resData1.data.length != 0) {
             console.log('pingu '+ JSON.stringify(resData1.data));
             $scope.favList = resData1.data;

           /* angular.forEach($scope.favList, function(value, key){
              //$scope.favFinalList = value;
              //console.log(JSON.stringify($scope.favList));
            });*/
          }

          else{
            resetAlert();
            alertify.alert('No favourites found.')
          }
      }
     
      else  
      {
        if(resData1.status == 'FAILURE'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData1.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert('Unauthorized access.');
                  
          $state.go('login', {},{'reload':true})
      }
  }
} 


  $scope.getAllFavourite = function() {
      
      var reqData1 = {};
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      
      userService.getFavourites(reqData1,$scope.getAllFavouriteSuccess);
  }



  $scope.select= function(index,item) {
       $scope.selected = index; 
       $scope.selDisplay = item;

       $('.tab-container').tabs();
       $('.tab-container .history').removeClass('active');
       $('.tab-container .performance').addClass('active');
       $('.tab-container').tabs({active:0});
       $("#ul.tabs").tabs("div.panes > div", { history: true });
       return $scope.selDisplay; 

  };






/****************** GET MAGIC API *********************/


$scope.getMagicAPISuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            if(resData.data.userRelationship && resData.data.userRelationship != undefined)
            {
              $scope.hasRelation = true;
              //$scope.save.rad1 = resData.data.userRelationship.user2_rel_role_description;
            }
            else{
              $scope.hasRelation = false;
            }


          if(resData.data.length != 0)
          {
            $scope.userRelationship = resData.data.userRelationship;
            $scope.editRelationsList = resData.data.editRelationsList;
            console.log('userRelationship '+JSON.stringify($scope.userRelationship));
            console.log('editRelationsList '+JSON.stringify($scope.editRelationsList));

            if ($scope.userRelationship == undefined && $scope.editRelationsList == undefined) 
            {
              $state.reload();  
              resetAlert();
              alertify.alert('You cannot pin yourself')
            }
            angular.forEach($scope.userRelationship, function(value, key){
              //console.log(JSON.stringify($scope.empFinalList));
            });
          }
          else{
            resetAlert();
            alertify.alert('You lalala')
          }
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          $state.reload();
          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}



$scope.getMagicAPI = function(e_id){

      var reqData = {};
      reqData.pinner = localStorageService.get('userData').id;
      reqData.pinnee = e_id;

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      console.log('ftats '+JSON.stringify(reqData))
     
      userService.magicAPI(reqData, $scope.getMagicAPISuccess);

}

/*****************  Adding Pin ******************/

$scope.savePinSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
              $state.reload();  
              resetAlert();
              alertify.alert('PIN saved successfully')
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          $state.reload();
          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}


$scope.savePin = function() {
  $scope.save.rating = $('#ratingTextValue').val();

  newItem.pinner = localStorageService.get('userData').id;
  newItem.pinnee = $scope.selDisplay.id;
  newItem.pinneeRelationship= parseInt($scope.save.pinneeRelationship);
  newItem.event = $scope.save.event;
  newItem.comment = $scope.save.comment;
  newItem.ratingSet = 1; // for now this will always be 1, later we shall have to change it
  newItem.rating = parseInt($scope.save.rating);
  newItem.tag = $scope.save.tags;
  newItem.objective = parseInt($scope.save.objective);
  newItem.pinDate = $scope.save.pinDate;

  $scope.item.push(newItem);
 
  var reqData = {};
  reqData = $scope.item[0];

  $http.defaults.headers.common = { 'Authorization' : undefined};      
  $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
  $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
  console.log('ftats '+JSON.stringify(reqData))
     
  userService.savePinToDB(reqData, $scope.savePinSuccess);
  
};

//getting checked tags
$('#check-button').click(function() {
$scope.save.tags=($('.Checkbox:checked').map(function() {
    return this.value;
}).get().join(', '));

console.log('meow '+$scope.save.tags)

});

$scope.tagsForDisplay = {};
$scope.objForDisplay = {};
$scope.relForDisplay = {};
$scope.showPreview = function(){

  $scope.relForDisplay = $('.Relation:checked').next().text();

  $scope.tagsForDisplay = ($('.Checkbox:checked').map(function() {
     return $(this).next("label").text().split(' ').join(',');
  }).get()).toString();

  $scope.objForDisplay = $('.objktive:checked').next().text();
  console.log( $scope.objForDisplay + ' lala '+ $scope.tagsForDisplay + 'lala' + $scope.relForDisplay)

}
/************** UPDATE RELATIONSHIP **************/
$scope.updateUserRelSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          $state.reload();
          resetAlert();
          alertify.alert('Relationship updated.');

      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          $state.reload();
          resetAlert();
          alertify.alert('Relationship could not be updated.');
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}

$scope.updateUserRel = function(){
      $scope.selDisplay.editedVal= $('#editedVal').val();
      var reqData = {};
      reqData.user1 = localStorageService.get('userData').id;
      reqData.user2 = $scope.selDisplay.id;
      reqData.user2_rel_role_id = parseInt($scope.selDisplay.editedVal);

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      console.log('ftats '+JSON.stringify(reqData))
     
      userService.updateRelFromDB(reqData, $scope.updateUserRelSuccess);

}







        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };

   /************* Function calls ***********/ 
    $scope.getMeta();    
     $scope.getAllUsers();
    $scope.getAllFavourite();
   
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
