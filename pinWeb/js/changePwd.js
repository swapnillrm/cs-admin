var app = angular.module('pinchangePwdCtrl.controllers', []);

app.controller('changePwdCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {
  
  if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

 $scope.req = {};
 $scope.clickedBtn = false;
    $scope.formSubmit = function(){
                $scope.clickedBtn = true;
        }
   $scope.clickedBtn1 = false;
    $scope.formSubmit1 = function(){
                $scope.clickedBtn1 = true;
        }
   $scope.LogoutSuccess = function(resData) {

      if(resData.status == 'SUCCESS')
      {
          delete $rootScope.user;  
        localStorageService.clearAll();
        
        $state.go('login', {}, {reload: true}); 
     
      }
      else
      {
          delete $rootScope.user;  
          localStorageService.clearAll();
         
          $state.go('login', {},{'reload':true}); 
       
      }
    }

    $scope.logOutUser = function() {
      var reqData = {};
      //$http.defaults.headers.common['Authorization'] = 'Basic '+ localStorageService.get('authToken');
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.logoutUser(reqData, $scope.LogoutSuccess)
    }

$scope.changePwdSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
           
            $state.reload();
            resetAlert();
            alertify.alert(resData.msg,function(e){
              if (e) {
                $scope.logOutUser();
                    }
                  });
            
            
        }
        else
        {   
            $state.reload();
            resetAlert();
            alertify.alert(resData.msg);
            
        }  
    }
    
$scope.changePwd = function() {
        console.log('as' + JSON.stringify($scope.item))
        var reqData = {}
      delete($scope.req.confPassword);
      console.log('reg data' + JSON.stringify($scope.req));
      reqData = $scope.req;
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';

      userService.changepassword(reqData, $scope.changePwdSuccess)
    }


        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };


   
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
