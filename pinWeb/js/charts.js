var app = angular.module('pinChartsCtrl.controllers', []);

app.controller('chartsCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http','$location','$window',
	function($scope,userService, $state, $rootScope,localStorageService,$http,$location,$window) {
 
/*********************** Google charts with trend lines *************************/
 //google.charts.load('current', {'packages':['corechart']});
$scope.drawGoogleChart = function(response)
{
  
        if(response.status == 'SUCCESS')
        {
           
           var path= [];
           path =  $location.search();
           google.charts.setOnLoadCallback();

           google.charts.load('current', {'packages':['corechart']});
            
                 
                      google.charts.setOnLoadCallback(drawCharts);
                      
                     
                      function drawCharts() {
                        var data = google.visualization.arrayToDataTable(changeChartData(response.data.chartsDataPoints));
                         var titles = response.data.chartsDataPoints[0].xTitle;

                        //console.log(changeChartData(response.data.chartsDataPoints));
                        var options = {
                          title:  titles.toUpperCase(),
                          hAxis: {title: 'Time', textStyle: {
                color: '#525b60',
                fontSize: '10'
            }},
                          vAxis: {title: 'Rating'},
                          legend: { position: 'bottom' },
                          lineWidth: 2,
                          width:path.width,
                          height:250,
                          trendlines: { 0: {} }    // Draw a trendline for data series 0.
                        };

                        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
                       
                        chart.draw(data, options);

                      }

      }
       else  
      {
        
        //alert(response.msg);
      }
  
}
//end google charts

$scope.avgchartRender = function()
{
        var reqData = {}
        var xAuth = "x-auth-token";
        var path = []
        path = $location.search();
        console.log(JSON.stringify(path.auth));
        reqData.pinnee = path.pinnee;

        console.log(path.auth)
       
        reqData.xAuth = path.auth;
        $http.defaults.headers.common['X-Auth-Token'] = path.auth;
        $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
        userService.getAvgChartData(reqData, $scope.drawGoogleChart)
       
}
$scope.avgchartRender();
  
}])

