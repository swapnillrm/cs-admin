function resetAlert () {
      $("#toggleCSS").attr("href", "./css/alertify.default.css");
      alertify.set({
        labels : {
          ok     : "OK",
          cancel : "Cancel"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
      });
    }
function resetConfirm () {
      $("#toggleCSS").attr("href", "./css/alertify.default.css");
      alertify.set({
        labels : {
          ok     : "Yes",
          cancel : "No"
        },
        delay : 5000,
        buttonReverse : true,
        buttonFocus   : "ok"
      });
    }

function removeHref () {
  $("a").removeAttr("href");
}

function useTags(){
    $(".tags").select2({
      placeholder: "Enter your tags",
      /*commented bcz we want them to select only from dropdown -->
      tags: true,*/
      allowClear: true

    });
}

function panelHeight(){
    $(window).resize(function() {
         $('.panel').height('100%');
    });
      
    $(window).trigger('resize');
}


function removeModalData(){
 $(".modal").on("hidden.bs.modal", function(){
      $(".form-group input").val("");
      $(".form-group textarea").val("");
    });
}

function manageMenuBar(type){
  $('.viewPort').show()
  //$('.menuBar').hide()
  if(type == true){
     $(".wrapperClass").removeAttr("padding");
    $('#sidebarRight').show()
    $('#menu_bar').show()
    $('.menuBar').show()
  }else{
     $(".wrapperClass").css("padding",0);
    $('#sidebarRight').hide()
    $('#menu_bar').hide()
    $('.menuBar').hide()
  }
}

function closeModal(){
  $('body').removeClass('modal-open');
  $('body').removeAttr('class')
  $('.modal').hide();
  $('.modal-backdrop').hide();
}

function closeEditModal(){
  $('body').removeClass('modal-open');
  $('body').removeAttr('class')
  $('#edit-emp').hide();
  $('.modal-backdrop').hide();
}

function resetAlert () {
      $("#toggleCSS").attr("href", "./css/alertify.default.css");
      alertify.set({
        labels : {
          ok     : "OK",
          cancel : "Cancel"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
      });
    }

// date picker
function datePicker(){
  var today = new Date();
   $(".datetimepicker").datetimepicker({
      autoclose: true,
      componentIcon: '.s7-date',
      navIcons:{
        rightIcon: 's7-angle-right',
        leftIcon: 's7-angle-left'
      },
       dateFormat:'MM-dd-yyyy HH:mm:ss',
            endDate:new Date(),

            startDate:new Date(today.setDate(today.getDate() - 7)),
    });
}

function scrollPageToTop(){
  $("html, body").animate({ scrollTop: 0 }, "fast");
}

function changeChartData(chartsDataPoints)
{

var final = [];
var first =[];
first = ['X-Axis', 'Y-Axis'];
 final.push(first);
$.each(chartsDataPoints, function(i, item) {
    var myarray =[];
    var xvalue = item.xValue;
    var yvalue = parseInt(item.yValue);
    myarray.push(xvalue,yvalue);

final.push(myarray)

});


return final;
}

function changeChartDatatoFloat(chartsDataPoints)
{

var finals = [];

j=1;

$.each(chartsDataPoints, function(i, item) {
if(i>0){
var row = [];
  $.each(item,function(j,list)
  {

    if(j > 0)
    {
      if(list !== null)
      {
        row.push(parseFloat(list));
      }
      else
      {
        row.push(null);
      }
    }
    else
    {
      row.push(list);
    }

  })
  finals.push(row);
}
else
{
  finals.push(item);
}
  
});
return finals;

}

 function clearInputFile(f){
        if(f.value){
            try{
                f.value = ''; //for IE11, latest Chrome/Firefox/Opera...
            }catch(err){
            }
            if(f.value){ //for IE5 ~ IE10
                var form = document.createElement('form'), ref = f.nextSibling;
                form.appendChild(f);
                form.reset();
                ref.parentNode.insertBefore(f,ref);
            }
        }
    }
function changeRatingarray(ratingArray)
{
  var final = []
  $.each(ratingArray, function(i, item) {
    if(item.ratingValue != 0){

       var myarray ={};
    myarray.value = parseInt(item.ratingValue);
    myarray.legend = item.ratingTitle;
    myarray.rateId = item.id;
    myarray.getTickColor = item.hexvalue;

final.push(myarray)
    }

  })
  console.log(JSON.stringify(final.sort()));
  return final;
}

function getSliderId(rid,ratingArray)
{
   var final=[];
   $.each(ratingArray, function(i, item) {
    
    if(item.ratingValue == rid)
    {
      
     final.push(item.id);
  
    
    }
    
   });
   return final[0];
}

function checkTrue(alltags,selected)
{ 
  console.log('alltags entry'+JSON.stringify(alltags))
   $.each(alltags, function(i, item) {
console.log(JSON.stringify(item))
      $.each(selected, function(j, selecteditem) {
console.log(JSON.stringify(selecteditem))
      if(item.id == selecteditem.id)
      {
        console.log(selecteditem.id)
        return true;
      }
      else
      {
        return false;
      }

      })

   })
}

function modifytags(allTags,selectedIds)
{  var final = [];

    $.each(allTags, function(i, item) {
      $.each(selectedIds, function(j, selecteditem) {
      if(item.id == selecteditem)
      {
        item.selectedMegha = 'YES';
       
        
      }
      else
      {
       item.selectedMegha = 'NO';
        //
        
      }
      })
      //console.log(JSON.stringify(item))
        final.push(item);

      // console.log(JSON.stringify(final))
      return final
   })


}