var app = angular.module('pinSettingsCtrl.controllers', []);


//normal controller
app.controller('settingsCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$modal','$log','$http',  
  function($scope,userService, $state, $rootScope,localStorageService,$modal,$log,$http) {
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $scope.req = {};
  $scope.updateSettingsSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {  
            $state.reload();
            resetAlert();
            alertify.alert(resData.msg);

            
        }
        else
        {
            $state.reload();
            resetAlert();
            alertify.alert(resData.msg);
            
        }  
    }
$scope.updateSettingsOnly = function() {
      //  console.log('obj '+JSON.stringify(obj.tagIds))
        var reqData = {}
        reqData.global_emails = ($scope.req.global_emails == true?"YES":"NO");
              reqData.global_notifications = ($scope.req.global_notifications== true?"YES":"NO");
      $.blockUI({ message: '<h4> Updating...</h4>' });
            
       // reqData= $scope.req;
       console.log(JSON.stringify(reqData))
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
       
       userService.updateSettings(reqData, $scope.updateSettingsSuccess)
    }
$scope.reload = function()
{
  $state.reload();
}
$scope.req={};
   $scope.getSettings = function() {
      //  console.log('obj '+JSON.stringify(obj.tagIds))
        var reqData = {}
      $.blockUI({ message: '<h4> Loading Settings...</h4>' });
       
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
       $http.defaults.headers.common['Accept'] = 'application/json';
        userService.getAllsettings(reqData,function(resData)
        {
           if(resData.status == "SUCCESS")
        {  
             $.unblockUI();
            if(resData.data != null)
            {
              $scope.req.global_emails = (resData.data.global_emails == "YES"?true:false);
              $scope.req.global_notifications = (resData.data.global_notifications== "YES"?true:false);
            
            }
            console.log(JSON.stringify(resData.data))
            
        }
        else
        {
            resetAlert();
            alertify.alert(resData.msg);
            
        }  

        });
    }    
   $scope.getSettings()
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
