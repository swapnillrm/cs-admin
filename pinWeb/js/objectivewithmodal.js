var app = angular.module('pinObjCtrl.controllers', []);


//modal controller
app.controller('ModalObjCtrl',['$scope','$modalInstance','userService','objList','test','$timeout','$interval'],
function ($scope,$modalInstance,userService,objList,test,$timeout,$interval) {

//console.log(JSON.stringify(peopleList)); 
$scope.item = objList;
/*$scope.people = peopleList;
$scope.f = $scope.people[0].name;
console.log($scope.people[0].name); */

$scope.test = test;
$scope.selected = {
item: $scope.item[0]

};


console.log('selected someting '+$scope.test)

$scope.ok = function () {

  $modalInstance.close($scope.selected.item);
};

//$scope.item = {}
$scope.editObj=function()
{
 // console.log(JSON.stringify($scope.item));
  var req = {};
  req = $scope.item;
   userService.editObjective(req, function(responce) {
           
           if(responce.status == "success")
            {
              $scope.ok();
            }
            else
            {
              alert('no luck');
            }
        });
}

$scope.cancel = function () {
  $modalInstance.dismiss('cancel');
  };


 $scope.names ="meghs"; 



//filter for ui-select
              




// ui-select code
 // var vm = this;
  
  /*vm.clear = function() {
    vm.person.selected = undefined;
  };
  vm.person = {};
*/

  
});

app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
        
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

/********************************  Objective controller  *************************/

app.controller('objectiveCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$modal','$log','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$modal,$log,$http) {
  
//manageMenuBar(true);
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }
  
  $scope.user.role= $rootScope.user.role;
    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

  panelHeight(); //for window height
  //useTags();     //using select2 tags
  scrollPageToTop(); //scrolling page to top 


// trying some custom select2

var tagsData = [
  {id:1,tag:'Apple'},
  {id:2,tag:'Banana'},
  {id:3,tag:'Cherry'},
  {id:4,tag:'Cantelope'},
  {id:5,tag:'Grapefruit'},
  {id:6,tag:'Grapes',selected:true},
  {id:7,tag:'Lemon'},
  {id:8,tag:'Lime'},
  {id:9,tag:'Melon'},
  {id:10,tag:'Orange'},
  {id:11,tag:'Strawberry'},
  {id:11,tag:'Watermelon'}
];

$scope.items = tagsData;
$("#mySel2").select2({
    /*closeOnSelect:true,
    placeholder: "Enter your tags",
    allowClear :true*/
});
    $(".tags").select2({
      placeholder: "Enter your tags",
      /*commented bcz we want them to select only from dropdown -->
      tags: true,*/
      allowClear: true

    });

// start with defaults
    $scope.client = { 
        primary_address: {
            country: ""
        }, 
        countries: []
    };
    // update at some later time

        $scope.client.primary_address.country = "Canada";   
        $scope.client.countries = [
                {name: "Canada"},
                {name: "USA"},
                {name: "France"}
                ];


/*   Get objectives   */  
$scope.allTags = [];
$scope.allObj = [];

$scope.getMetaSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          if (resData.data.objectiveList.length > 0) {

            $scope.allTags = resData.data.tagsList;
            $scope.allObj = resData.data.objectiveList;

            //console.log('all tags only '+ JSON.stringify($scope.allTags))
            angular.forEach($scope.allTags, function(value, key){
              $scope.tagsListFinal = value;
              //console.log(JSON.stringify($scope.tagsListFinal));
            });
            
          }
          else{
            resetAlert();
            alertify.alert('Objectives not found.')
          }  
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }
} 

 $scope.getMeta = function() {
      
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['x-auth-token'] = localStorageService.get('authToken');
      userService.getMetaData(reqData,$scope.getMetaSuccess);
  }
  $scope.getMeta();

/*  Add Tags  */  
$scope.addObjSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {
            resetAlert();
            alertify.alert('Objective added successfully.');
            $scope.getMeta();
        }
        else
        {
            resetAlert();
            alertify.alert('Objective insertion failed.');
            $state.go('objective',{'reload':true});
        }  
    }
$scope.addObj = function() {

        var reqData = {}
        reqData = $scope.register;
        console.log('reg data' + JSON.stringify(reqData) );

        userService.addNewObj(reqData, $scope.addObjSuccess)
    }


/*   DELETE TAGS   */    
$scope.deleteTagSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {
            resetAlert();
            alertify.alert('Objective deleted successfully.');
            $scope.getMeta();
        }
        else
        {
            resetAlert();
            alertify.alert('Objective deletion failed.');
            $state.go('objective',{'reload':true});
        }  
    }
$scope.deleteTag = function(id) {

        var reqData = {}
        reqData.id = id;
        console.log('reg data' + JSON.stringify(reqData));
        resetConfirm();
        alertify.confirm("Do you want to delete the objective?", function (e) {
            if (e) {
                userService.deleteObjFromDB(reqData, $scope.deleteTagSuccess)
            }
            else{
                alertify.error('Cancelled');
            }
        });    
    }
    $scope.people = [
    { name: 'manu', email: 'adam@email.com', age: 12, country: 'United States' },
    { name: 'Chinu', email: 'amalie@email.com', age: 12, country: 'Argentina' },
    { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
    { name: 'Adrian', email: 'adrian@email.com', age: 21, country: 'Ecuador' },
    { name: 'Wladimir', email: 'wladimir@email.com', age: 30, country: 'Ecuador' },
    { name: 'Samantha', email: 'samantha@email.com', age: 30, country: 'United States' },
    { name: 'Nicole', email: 'nicole@email.com', age: 43, country: 'Colombia' },
    { name: 'Natasha', email: 'natasha@email.com', age: 54, country: 'Ecuador' },
    { name: 'Michael', email: 'michael@email.com', age: 15, country: 'Colombia' },
    { name: 'Nicolás', email: 'nicolas@email.com', age: 43, country: 'Colombia' }
  ];

  $scope.editObj = function(obj) {
    //console.log('adjajd' +JSON.stringify(people))
    var modalInstance = $modal.open({
      controller: 'ModalObjCtrl',
      templateUrl: 'partials/editObjectiveModal.html',
      test:'test variable',
      resolve: {
        objList: function() {
          return obj;
        },
         test: function () {
          return 'test variable';
        }
      }
    });

   /*modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
        $log.info('Modal dismissed at: ' + new Date()+'__' +$scope.selected);
        }); */
  }




        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
