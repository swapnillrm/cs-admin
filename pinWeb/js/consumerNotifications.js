var app = angular.module('pinConsumerNotificationsCtrl.controllers', []);

app.controller('consumerNotificationsCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
  function($scope,userService, $state, $rootScope,localStorageService,$http) {
  
  if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

 
  $scope.openPinWizard= false;
  $scope.date = new Date();

    $scope.navDetails = function()
    {
      if($rootScope.user)
    {     
      $scope.user = $rootScope.user; 

      }    
    }

  console.log('We are in consumer');
  panelHeight(); //for window height
  datePicker(); // for date picker
  scrollPageToTop(); //scrolling page to top 
  $scope.notificationCount = [];
 $scope.noteMsg;


$scope.getNotificationsSuccess = function(resData) {

      if(resData.status == 'SUCCESS')
      {
           if($rootScope.storageType == 'localStorage') 
              {
                  localStorageService.set('userData',$rootScope.user);
                  localStorageService.set('authToken', $rootScope.user.authstring);
              }

              $.unblockUI();

              $scope.noteMsg = resData.msg;
                if(resData.data != 0)
                {
                  $scope.notificationCount = resData.data;
               console.log('note '+JSON.stringify($scope.notificationCount));
                }

                else{
                  resetAlert();
                  alertify.error('No notifications found');
                }
              
      }
      else
      {
          $.unblockUI();
          resetAlert();
          alertify.alert('Operation failed.');
          $state.reload();
      }
    } 

$scope.notifications = function(){
  var reqData = {};
     $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 25000});

      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      userService.getNotifications(reqData, $scope.getNotificationsSuccess)

}

$scope.notifications();

 
$scope.readNotificationSuccess = function(resData) {

      if(resData.status == 'SUCCESS')
      {
           if($rootScope.storageType == 'localStorage') 
              {
                  localStorageService.set('userData',$rootScope.user);
                  localStorageService.set('authToken', $rootScope.user.authstring);
              }
                $state.reload()
                 resetAlert();
                  alertify.error(resData.msg); 
                  
      }
      else
      {
        $state.reload()
          resetAlert();
          alertify.error(resData.msg);
      }
    } 

$scope.readNotification = function(id){
  var reqData = {};
  reqData.id = id;
    
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      userService.marknotification(reqData, $scope.readNotificationSuccess)

}

  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
app.filter('moment', [
    function () {
      return function (date, method) {
        var momented = moment(date);
        return momented[method].apply(momented, Array.prototype.slice.call(arguments, 2));
      };
    }
  ])