var app = angular.module('lpinMenu.controllers', []);

app.controller('menuCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {
if(localStorageService.get('userData')) {
		$rootScope.user = localStorageService.get('userData');
		} else {
		delete $rootScope.user;
		}
   $scope.getPartial = function () {
  $scope.headerTemplate= "";
    if(($rootScope.user != undefined) ||  (localStorageService.get('userData')!= null) )
    {     
     
      if(localStorageService.get('userData').role == 'ROLE_ADMIN')
      {
      
        $scope.templates = {
         template: { url: 'partials/AdminHeader.html' }
        };

       $scope.headerTemplate = $scope.templates.template.url;
        return $scope.headerTemplate;
      }

       else 
        {
        
          $scope.templates = {
           template: { url: 'partials/ConsumerHeader.html' }
          };

         $scope.headerTemplate = $scope.templates.template.url;
          //console.log(JSON.stringify($rootScope.user));
           return $scope.headerTemplate;
        }
      }
}
$scope.navDetails = function()
    {
      if($rootScope.user != null)
      {
          $scope.user.Name = $rootScope.user.uName; 
        
      }
      else if(localStorageService.get('userData')) 
      {
      $rootScope.user = localStorageService.get('userData'); 
      $scope.user.Name = $rootScope.user.uName;
   
    } 

 else {
    delete $rootScope.user;
    } 

}


/* get notifications */
 $scope.notificationsUnread = [];
 $scope.noteMsg;
 $scope.notificationunreadCount = 0;
$scope.getNotificationsSuccess = function(resData) {

      if(resData.status == 'SUCCESS')
      {
           if($rootScope.storageType == 'localStorage') 
              {
                  localStorageService.set('userData',$rootScope.user);
                  localStorageService.set('authToken', $rootScope.user.authstring);
              }

             // $scope.noteMsg = resData.msg;
                if(resData.data != 0)
                {
                  $scope.notificationunreadCount = resData.data.notificationCount?resData.data.notificationCount:0;
               //console.log('note '+JSON.stringify($scope.notificationCount));
                }

                else{
                  /*resetAlert();
                  alertify.error('No notifications found');*/
                  console.log('No notifications found')
                }
              
      }
      else
      {
          resetAlert();
          alertify.error('No notifications found');
      }
    } 

$scope.notificationscount = function(){
  var reqData = {};
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      userService.getUnreadNotificationsCount(reqData, $scope.getNotificationsSuccess)

}
$scope.getunreadNotificationsSuccess = function(resData) {

      if(resData.status == 'SUCCESS')
      {
           if($rootScope.storageType == 'localStorage') 
              {
                  localStorageService.set('userData',$rootScope.user);
                  localStorageService.set('authToken', $rootScope.user.authstring);
              }

             // $scope.noteMsg = resData.msg;
                if(resData.data != 0)
                {
                  $scope.notificationsUnread = resData.data;
               //console.log('note '+JSON.stringify($scope.notificationsUnread));
                }

                else{
                  /*resetAlert();
                  alertify.error('No notifications found');*/
                  console.log('No notifications found')
                }
              
      }
      else
      {
          resetAlert();
          alertify.error('No notifications found');
      }
    } 

$scope.notificationsUnreads = function(){
  var reqData = {};
  reqData.showLimit = 5;
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      userService.getunreadNotifications(reqData, $scope.getunreadNotificationsSuccess)

}

if($rootScope.user != null || localStorageService.get('userData'))
      {
          
          $scope.notificationscount();
          $scope.notificationsUnreads(); 

      }
      else
      {
      	$state.go("login");
      }
      

  $scope.goHome = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 25000});
      
    $state.go('home',{},{});
  }
  $scope.goObj = function(){
  
      $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('objective',{},{});
  }
  $scope.goRate = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('ratings',{},{reload: true});
  }
  $scope.goTags = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('tags',{},{reload: true});
  }
  $scope.goReport = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('report',{},{reload: true});
  }
  $scope.goSettings = function(){
    $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('settings',{},{});
  }
  $scope.gocHome = function(){
    $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 25000});
      
    $state.go('chome',{},{});
  }
  $scope.goProfile = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 5000});
      
    $state.go('profile',{},{reload:true});
  }
  $scope.gochangePwd = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('changepwd',{},{reload: true});
  }
  $scope.goByMe = function(){
    $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('bypins',{},{reload: true});
  }
  $scope.goForMe = function(){
  $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('forpins',{},{reload: true});
  }
  $scope.goExport = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
      
    $state.go('export',{},{reload: true});
  }
  $scope.goNotify = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 2500});
     
    $state.go('notifications',{},{reload: true});
  }
  $scope.goConsumer = function(){
   $.blockUI({ message: '<h4> Loading...</h4>' ,timeout: 25000});
      
    $state.go('chome',{},{});
  }

    $scope.LogoutSuccess = function(resData) {
//$http.defaults.headers.common['x-auth-token'] = undefined;
 //delete $http.defaults.headers.common['x-auth-token'];
   //localStorageService.clearAll();

			if(resData.status == 'SUCCESS')
      {
        delete $rootScope.user; 
        delete $http.defaults.headers.common['X-Auth-Token'];
         window.localStorage.removeItem(localStorageService.get('authToken'));
         localStorage.clear();
          localStorageService.clearAll();  
       
       
        $state.go('login', {}, {reload:true}); 
      
        
			}
			else
			{
         delete $rootScope.user; 
        delete $http.defaults.headers.common['X-Auth-Token'];
         window.localStorage.removeItem(localStorageService.get('authToken'));
         localStorage.clear();
          localStorageService.clearAll();  
       
       
        $state.go('login', {}, {reload:true}); 
      
        			}
		}

		$scope.logOutUser = function() {
      var reqData = {};
      $.blockUI({ message: '<h4> Logging Out...</h4>'});
      //$http.defaults.headers.common['Authorization'] = 'Basic '+ localStorageService.get('authToken');
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.logoutUser(reqData, $scope.LogoutSuccess)
		}



}])
