var app = angular.module('pinHomeCtrl.controllers', []);


//modal controller
app.controller('ModalObjCtrl', function ($scope, $modalInstance, objList,userService) {
//console.log(JSON.stringify(objList)); 
$.unblockUI();
$scope.item = objList;
$scope.selected = {
item: $scope.item[0]
};

$scope.ok = function () {
  $modalInstance.close($scope.selected.item);
};
   
  //$('#edit-emp').modal({backdrop: 'static', keyboard: false})  

//$scope.item = {}
$scope.editObj=function()
{
 // console.log(JSON.stringify($scope.item));
  var req = {};
  req = $scope.item;
   userService.editObjective(req, function(responce) {
           
           if(responce.status == "success")
            {
              $scope.ok();
            }
            else
            {
              alert('no luck');
            }
            
        });

}

$scope.cancel = function () {
  $modalInstance.dismiss('cancel');
  };
});


//normal controller
app.controller('homeCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$modal','$log','$http','$parse',
	function($scope,userService, $state, $rootScope,localStorageService,$modal,$log,$http,$parse) {
  //localStorageService.clearAll();
//manageMenuBar(true);

  if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $scope.req = {};
  $scope.editVal = {};
  //$scope.act=false;

  $scope.allEmpList = [];

  $scope.selected = 0;


	console.log('We are in home');
  removeHref(); //to remove href attr from <a>
  panelHeight(); //for window height
  scrollPageToTop(); //scrolling page to top 
  //removeModalData(); //remove data 


/*  REGISTER USER*/  

	 $scope.registerUserSuccess = function(resData) {
           
    if(resData.status == "SUCCESS")
    {

      $state.reload();
      resetAlert();
      alertify.alert(resData.msg);
    }
    else
    {
      $state.reload();
      resetAlert();
      alertify.alert(resData.msg);
    }  
}

$scope.registerUser = function() {
    
    closeModal();
    var reqData = {}
    reqData = $scope.register;
    reqData.username = $scope.register.email;
    console.log('reg data' + JSON.stringify(reqData));
    userService.registerNewUser(reqData, $scope.registerUserSuccess)
}






/*    GET ALL USERS   */


// this code also handles any other error status like 401,400,500 etc.
$scope.getAllUserSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            $scope.allEmpList = resData.data;
            //console.log('all users list '+JSON.stringify(resData.data[0].enabled))
          
              $scope.selDisplay = resData.data[0];
              $scope.editVal = angular.copy($scope.selDisplay)

           /* angular.forEach($scope.allEmpList, function(value, key){
              
             console.log(JSON.stringify($scope.empFinalList.is_active));
                 if(value.enabled == true){
                   $scope.act = false;
                  }
                  else {
                    $scope.act = true;
                  }
            });*/


      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          $state.reload()

          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          $state.go('login', {},{'reload':true});

          resetAlert();
          alertify.alert('Unauthorized access');
      }
  }
  $.unblockUI();
} 

 $scope.getAllUsers = function() {
      $.blockUI({ message: '<h4> Loading users...</h4>' });
      var reqData = {};
      //reqData.skipMe = true;
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
         
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
    // $http.defaults.headers.common['Access-Control-Request-Headers'] = "X-Requested-With";
      // $http.defaults.headers.common['Accept'] = 'application/json,text/javascript';
      //  $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
      //  $http.defaults.headers.common[' X-Content-Type-Options'] = 'nosniff';
         
    
     userService.getUsers(reqData,$scope.getAllUserSuccess);
  }
  $scope.getAllUsers();




 /*  DEACTIVATE USER  */ 

$scope.deactivateEmp = function(id){
  var reqData ={}
  reqData.id = id;
  console.log(JSON.stringify(reqData))

      resetConfirm();
      alertify.confirm("Do you want to deactivate this user?", function (e) {
            if (e) 
            {
              $.blockUI({ message: '<h4>Deactivating User...</h4>' });

              $http.defaults.headers.common = { 'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              
              userService.deactivateUser(reqData, function(resData)
              {
                if(resData.status=="SUCCESS")
                {
                 // $scope.act = false;
                  $.unblockUI();
                  resetAlert();
                  alertify.alert('Employee deactivated.')
                  $state.reload();
                }
                else{
                 // $scope.act = true;
                  resetAlert();
                  alertify.alert('Could not deactivate. Please try again.')
                }
              })
            }
            else{
                alertify.error('Cancelled');
            }
        });

          
}



/*  ACTIVATE USER  */   

$scope.activateEmp = function(id){
  var reqData ={}
  reqData.notificationTo = id;

      resetConfirm();
      alertify.confirm("Do you want to activate this user?", function (e) {
            if (e) 
            {
              $.blockUI({ message: '<h4>Activating User...</h4>' });

              $http.defaults.headers.common = {'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              
              userService.activateUser(reqData, function(resData)
              {
                if(resData.status=="SUCCESS")
                {
                 // $scope.act = false;
                  $.unblockUI();
                  resetAlert();
                  alertify.alert('Account activation email has been sent to the employee.')
                  $state.reload();
                }
                else{
                 // $scope.act = true;
                  resetAlert();
                  alertify.alert('Could not activate. Please try again.')
                }
              })
            }
            else{
                alertify.error('Cancelled');
            }
        });/*


          userService.activateUser(reqData, function(resData){
            if(resData.status=="SUCCESS"){
             // $scope.act = true;
              $state.reload()
              resetAlert();
              alertify.alert('Employee activated.')
            }
            else{
             // $scope.act = false;
              resetAlert();
              alertify.alert('Could not activate. Please try again.')
            }

          })*/
}

$scope.clear = function()
{
  console.log('sd');

  $('#imgUpload').hide();
  $('#fileUploadBtn').css('display','none');
  //removeModalData();
    /*$(".modal").on("hidden.bs.modal", function(){
      $(".form-group input").val("");
      $(".form-group textarea").val("");
    });*/
}
  $scope.select= function(index,item) {
       $scope.selected = index; 
       $scope.selDisplay = item;
       $scope.editVal = angular.copy($scope.selDisplay);
       //return $scope.selDisplay; 
  };

$scope.getUsercountSuccess = function(resp)
{
   if(resp.status == "SUCCESS")
      {
        $scope.activate = resp.data.activeUserCount;
        $scope.deactive = resp.data.deactiveUserCount;
        $scopeTotal = resp.data.totalUserCount;
      }
      else
      {
        $scope.activate = 0;
        $scope.deactive = 0;
        $scopeTotal = 0;
      }
}

$scope.getCount = function()
{
      var reqData = {};
     
      $http.defaults.headers.common = { 'Authorization' : undefined};      
         
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
    userService.getUsersCount(reqData,$scope.getUsercountSuccess);

}

$scope.getCount();

/* UPDATE USER*/
$scope.updateUserSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
           closeModal();
           $state.reload();
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
           
            resetAlert();
            alertify.alert('Update successful.')
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          closeModal();
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
            closeModal();
            $state.reload();
            resetAlert();
            alertify.alert('Update not successful')
      }
  }
} 

 $scope.updateUser = function(employee) {
      
      var reqData = {};
      reqData.firstname = employee.firstname;
      reqData.lastname = employee.lastname;
      reqData.username = employee.username;
      reqData.department = employee.department;
      reqData.location = employee.location;

      console.log('update '+JSON.stringify(reqData))

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.updateUserFromDB(reqData,$scope.updateUserSuccess);
  }


$scope.pfImage='';

$scope.imgSizeRestrict=false;



$scope.profileImgUpload = function(){

  $scope.imageStrings = [];
  $scope.processFiles = function(files){
    angular.forEach(files, function(flowFile, i){
       var fileReader = new FileReader();
$('#imgUpload').show();

        $scope.fileSize= flowFile.file.size/1024;

        console.log('file size '+ $scope.fileSize)

        if($scope.fileSize > 0){
          $('#fileUploadBtn').css('display','inline');
        }

       if($scope.fileSize > 1024){
        $('#fileUploadBtn').attr('disabled','disabled');
        $('#errText').html('<p class="row errorSpan"> File size is too large to upload. Please upload another image.</p>')
        $scope.imgSizeRestrict = true;
       }
       else{

        if(flowFile.file.type == 'image/png' || flowFile.file.type == 'image/jpg' || flowFile.file.type == 'image/jpeg' || flowFile.file.type == 'image/gif')
         {
            $('#fileUploadBtn').removeAttr('disabled');
            $('#errText').css('display','none');
            $scope.imgSizeRestrict=false;
            fileReader.onload = function (event) {
            var uri = event.target.result;
              $scope.imageStrings[i] = uri;

               $scope.typeofFile = flowFile.file.type; 
               $scope.nameofFile = flowFile.file.fileName;  
                
          };
          fileReader.readAsDataURL(flowFile.file);
        }
        else
        {
            $('#fileUploadBtn').attr('disabled','disabled');
            $('#errText').css('display','block');
            $('#errText').html('<p class="row errorSpan"> Allowed formats are jpg,jpeg and png. Please upload another image.</p>')
            $scope.imgSizeRestrict = true;
        }
       }
          
    });
  };
       
}



/* Profile Image to DB */

$scope.isUploading = false;
$scope.profileOfUserSuccess = function(resData) {
      $.unblockUI();
      if(resData.status == "SUCCESS")
      {
           closeModal();
           $state.reload();
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
           
            resetAlert();
            alertify.alert('Profile image changed.')
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
            $state.reload();
            resetAlert();
            alertify.alert('Update not successful.')
      }
  }
} 

 $scope.saveProfile = function(id) {
  $.blockUI({ message: '<h4> Uploading image.. </h4>' });

      $scope.isUploading = true;
      $('#edit-profile').css('background-color','#37373b');
      $('.login-modal-content').css('opacity','0.8');
      
      var reqData = {};

      $scope.imgProfile = JSON.parse($('#profileImg').val()); 
      $scope.img = JSON.stringify($scope.imgProfile)
         

          $scope.imgType = $scope.img.slice(2);


          $scope.imgProfileOrg2 = $scope.imgType.split('"');
          var projectImage= $scope.imgProfileOrg2[0];
          $scope.base = projectImage.split('base64,');
    
      reqData.base64String = $scope.base[1];
      reqData.fileType = $('#typeoffile').val();
      reqData.id = id;

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.uploadProfile(reqData,$scope.profileOfUserSuccess);
  }
//Profile Image to DB

//Import CSV
$scope.uploadedFile = function(element) {
 $scope.$apply(function($scope) {
   $scope.files = element.files;         
 });
}
$scope.addFile = function() {
            var fd = new FormData();
            var files = $scope.files
            angular.forEach(files,function(file)
            {
                 fd.append('file',file);
            });
            $http.defaults.headers.common = { 'Authorization' : undefined};      
            $http.defaults.headers.common = { 'Content-Type' : undefined};      
            $http.defaults.headers.post = {'Content-Type' : undefined};
            $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');

            userService.importHistoryToDB(fd,function( resData ) // success
            {
                  if(resData.status == "SUCCESS")
                    {
                         closeModal();
                         $state.reload();
                        if($rootScope.storageType == 'localStorage') 
                        {
                            localStorageService.set('userData',$rootScope.user);
                            localStorageService.set('authToken', $rootScope.user.authstring);
                        }
                         
                          resetAlert();
                          alertify.alert('Your file has been saved to the Database.')
                    }
     
                    else  
                    {
                      if(resData.msg == 'UNAUTHORIZED_ACCESS'){
                        delete $rootScope.user;  
                        localStorageService.clearAll();
                        resetAlert();
                        alertify.alert(resData.msg);
                                
                        $state.go('login', {},{'reload':true}); 
                    }
                    else{
                          closeModal();
                          resetAlert();
                          alertify.alert('Operation failed. Please try again.')
                          $state.reload();
                    }
                }
            });
}

//Import CSV ENDS HERE

// to set modal to pristine after clicking close
$scope.resetModal = function()
{
   $(".modal").on("hidden.bs.modal", function(){
      $(".form-group input").val("");
      $(".form-group textarea").val("");
    });
  $scope.addEmpForm.$setUntouched(); // to make the form untouched
  $scope.addEmpForm.$setPristine();  // to make the form pristine
  $scope.register = '';
}




        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
