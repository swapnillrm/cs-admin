var app = angular.module('pinObjCtrl.controllers', []);


/********************************  Objective controller  *************************/

app.controller('objectiveCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$modal','$log','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$modal,$log,$http) {
  
  $scope.register = [];
  $scope.obj = [];
  $scope.allObj1 = [];
  $scope.allTags = [];
  $scope.allObj = [];
  $scope.tagList = [];

//manageMenuBar(true);
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }
  
  $scope.user.role= $rootScope.user.role;
    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

  panelHeight(); //for window height
  removeModalData(); //remove data 
  scrollPageToTop(); //scrolling page to top 
   $scope.editAppKey = function() {
        // How do I get the data from the row that was edited?
        
        // How do I turn off editMode for that row?
        $scope.editMode = false;
    }
/*   Get objectives   */  
$scope.tagsSeleted = [];
/*  ONLY OBJECTIVES  */
$scope.getOnlyObjSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
       
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          if (resData.data.length != 0) 
          {
            
            $scope.allObj1 = resData.data; 
          // console.log('obj' + JSON.stringify(resData.data))
            localStorageService.set('selectedtags',resData.data);
             
          }
          else{
            resetAlert();
            alertify.alert('Objectives not found.')
          } 
        
           
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }

//$.unblockUI();
} 
$scope.getIdofObj = function(id)
{
  //$scope.gettagsselected(id);
 $("#mySel"+id).select2({
            tags: true,
            //selectOnClose: true,
            closeOnSelect:false,
            placeholder: 'Enter tags',
            width: '100%'
});
}
$scope.getOnlyObj = function() {
      
      var reqData = {};
     
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.getObjectives(reqData,$scope.getOnlyObjSuccess);
}
 $scope.getOnlyObj(); 


/*  ADD OBJECTIVES  */  
$scope.addObjSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
            closeModal();
            $state.reload();
            resetAlert();
            alertify.alert('Objective added successfully.');
            
        }
        else
        {   closeModal();
            $scope.reload();
            resetAlert();
            alertify.alert('Objective insertion failed.');
            
        }  
    }
    //$scope.item;
$scope.addObj = function() {
        //console.log('as' + JSON.stringify($scope.item))
        var reqData = {}
        reqData.title = $scope.register.title.replace(/^\s+/, '').replace(/\s+$/, '');
        reqData.description = $scope.register.description.replace(/^\s+/, '').replace(/\s+$/, '');

        var tagId="";
        tagId= $scope.register.tagIds.toString();
        reqData.tagIds =tagId;
      
       // console.log('reg data' + JSON.stringify(reqData));
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';

       userService.addNewObj(reqData, $scope.addObjSuccess)
    }





/*   DELETE OBJECTIVES   */    
$scope.deleteObjSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {  
            $state.reload();
            resetAlert();
            alertify.alert('Objective deleted successfully.');
            
        }
        else
        {
            resetAlert();
            alertify.alert('Objective deletion failed. Please refresh the page and try again.');
            $state.reload();
            
        }  
    }
$scope.deleteObj = function(id) {

        var reqData = {}
        reqData.id = id;
       // console.log('reg data' + JSON.stringify(reqData));
        resetConfirm();
        alertify.confirm("Do you want to delete the objective?", function (e) {
            if (e) {
              $http.defaults.headers.common = { 'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
                userService.deleteObjFromDB(reqData, $scope.deleteObjSuccess)
            }
            else{
                alertify.error('Cancelled');
            }
        });    
    }

/*  UPDATE OBJECTIVES  */  
$scope.updateObjSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
            $state.reload();
            resetAlert();
            alertify.alert('Objective updated successfully.');
            
        }
        else
        {
            $state.reload();
            resetAlert();
            alertify.alert('Objective updation failed.');
            
        }  
    }
$scope.updateObj = function(obj) {
      //  console.log('obj '+JSON.stringify(obj.tagIds))
        var reqData = {}
        reqData.id = obj.id;
        reqData.title = obj.title.replace(/^\s+/, '').replace(/\s+$/, '');
        reqData.description = obj.description.replace(/^\s+/, '').replace(/\s+$/, '');

        var tagId="";
        //console.log($("#mySel"+obj.id).val());
        tagId= $("#mySel"+obj.id).val().toString();
        reqData.tagIds =tagId;
      
        
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
       // console.log('reg data for update' + JSON.stringify(reqData) );

      //  userService.updateObjInDB(reqData, $scope.updateObjSuccess)
      if(reqData.title.length > 0 && reqData.description.length > 0 && reqData.tagIds.length > 0)
      {
        userService.updateObjInDB(reqData, $scope.updateObjSuccess)
        //resetAlert();
        //alertify.alert('lalalall.');
      }
      else
      {
        //$('#updateTagBtn').attr('disabled','disabled');
        resetAlert();
        alertify.alert('You cannot keep the fields empty.');
        $state.reload();
      }
    }



/*   Get objectives   */  

$scope.getMetaSuccess1 = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
         

          $scope.allTags = resData.data.tagsList;
       

         

          //console.log('all tags only '+ JSON.stringify($scope.allTags))
          if (resData.data.objectiveList.length > 0) {
            $scope.allObj = resData.data.objectiveList;

            angular.forEach($scope.allTags, function(value, key){
              $scope.tagsListFinal = value;
              //console.log(JSON.stringify($scope.tagsListFinal));
            });
           // $('#addObjModal').modal('show'); 
            $('#addObjModal').modal({backdrop: 'static', keyboard: false})  

            return $scope.allTags;
          }
          else{
            resetAlert();
            alertify.alert('Objectives not found.')
          }  
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }
} 

 $scope.getMeta1 = function() {
      
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.getMetaData(reqData,$scope.getMetaSuccess1);
  }


//Import CSV
$scope.uploadedFile = function(element) {
 $scope.$apply(function($scope) {
   $scope.files = element.files;         
 });
}
$scope.addFile = function() {
            var fd = new FormData();
            var files = $scope.files
            angular.forEach(files,function(file)
            {
                 fd.append('file',file);
            });
            $http.defaults.headers.common = { 'Authorization' : undefined};      
            $http.defaults.headers.common = { 'Content-Type' : undefined};      
            $http.defaults.headers.post = {'Content-Type' : undefined};
            $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');

            userService.importObjToDB(fd,function( resData ) // success
            {
                  if(resData.status == "SUCCESS")
                    {
                         closeModal();
                        if($rootScope.storageType == 'localStorage') 
                        {
                            localStorageService.set('userData',$rootScope.user);
                            localStorageService.set('authToken', $rootScope.user.authstring);
                        }
                         
                          resetAlert();
                          alertify.alert('Your file has been saved in the database.')
                         $state.reload();
                    }
     
                    else  
                    {
                      if(resData.msg == 'UNAUTHORIZED_ACCESS'){
                        delete $rootScope.user;  
                        localStorageService.clearAll();
                        resetAlert();
                        alertify.alert(resData.msg);
                                
                        $state.go('login', {},{'reload':true}); 
                    }
                    else{
                          $state.reload();
                          resetAlert();
                          alertify.alert('Operation failed. Please refresh the page and try again.')
                    }
                }
            });
}

//Import CSV ENDS HERE

// to set modal to pristine after clicking close
$scope.resetModal = function()
{
  $(".form-group select").find('option').remove().end().val('');

  $scope.addObjForm.$setUntouched(); // to make the form untouched
  $scope.addObjForm.$setPristine();  // to make the form pristine
  $scope.register = '';
}


        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };

/*********************** Function calls *********************/      
 
 

  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
