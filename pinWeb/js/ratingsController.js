var app = angular.module('pinRatingsCtrl.controllers', []);

app.controller('ratingsCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {
  
//manageMenuBar(true);
 if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.register = {};
  $scope.ratings = {};
 
  $scope.allRatings = [];
  
 $('.cancel').on('click', function() {
            $('form').find('input').val('');
        });
    $scope.navDetails = function()
    {
    	if($rootScope.user)
		{    	
			$scope.user = $rootScope.user; 
   		}    
    }

	panelHeight() //panel window height
  scrollPageToTop(); //scrolling page to top 
  removeModalData(); //remove data 
  
  /*   Get objectives   */  


$scope.getMetaSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          //console.log('all ratings '+JSON.stringify(resData.data.ratingList))

          if (resData.data.ratingList.length > 0) {

           $scope.allRatings = resData.data.ratingList;
           console.log('all ratings '+JSON.stringify($scope.allRatings))
/*
            angular.forEach($scope.allRatings, function(value, key){
              $scope.tagsListFinal = value;
            });
            return $scope.allRatings;*/
          }
          else{
            resetAlert();
            alertify.alert('Ratings not found.')
          }  
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }

  $.unblockUI();
} 

 $scope.getMeta = function() {
      $.blockUI({ message: '<h4> Loading...</h4>' });
      
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.getMetaData(reqData,$scope.getMetaSuccess);
  }
  $scope.getMeta();




  /*  ADD RATINGS  */  
$scope.addRatingSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
            closeModal(); 
            $state.reload();
            resetAlert();
            alertify.alert('Ratings added successfully.');
            
        }
        else
        {   closeModal();
            $scope.reload();
            resetAlert();
            alertify.alert('Ratings insertion failed.');
            
        } 

        $.unblockUI(); 
    }
    
$scope.addRating = function() {
        $.blockUI({ message: '<h4> Adding Ratings...</h4>' });
      
        var reqData = {}
        $scope.ratings.assetOn = "2";
        $scope.ratings.assetOf = "2";
        $scope.ratings.setID = 1; //set default to 1 only
        //reqData = $scope.ratings;
       reqData.ratingTitle = $scope.ratings.ratingTitle.replace(/^\s+/, '').replace(/\s+$/, '');
       reqData.ratingDescription = $scope.ratings.ratingDescription.replace(/^\s+/, '').replace(/\s+$/, '');
       reqData.ratingValue = $scope.ratings.ratingValue.replace(/^\s+/, '').replace(/\s+$/, '');
       reqData.hexvalue = $scope.ratings.hexvalue.replace(/^\s+/, '').replace(/\s+$/, '');
        
      console.log('reg data' + JSON.stringify(reqData));
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';

      userService.addNewRating(reqData, $scope.addRatingSuccess)
    }


/*   DELETE RATINGS   */    
$scope.deleteRatingSuccess = function(resData) {
        $.unblockUI();

        if(resData.status == "SUCCESS")
        {   
            $state.reload();
            resetAlert();
            alertify.alert('Ratings deleted successfully.');
        }
        else
        { 
            $state.reload();
            resetAlert();
            alertify.alert('Ratings deletion failed.');
        }  

    }
$scope.deleteRating = function(id) {
      
        var reqData = {}
        reqData.id = id;
        console.log('reg data' + JSON.stringify(reqData));
        resetConfirm();
        alertify.confirm("Do you want to delete the rating?", function (e) {
            if (e) {
                $.blockUI({ message: '<h4> Deleting Rating...</h4>' });

              $http.defaults.headers.common = { 'Authorization' : undefined};      
              $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
              $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
              userService.deleteRatingFromDB(reqData, $scope.deleteRatingSuccess)
            }
            else{
                alertify.error('Cancelled');
            }
        });    
    }

/*  UPDATE RATINGS  */  
$scope.updateRatingSuccess = function(resData) {

        if(resData.status == "SUCCESS")
        {   
            $state.reload();
            resetAlert();
            alertify.alert('Rating updated successfully.');
        }
        else
        {
            $state.reload();
            resetAlert();
            alertify.alert('Rating updation failed.');
        }  

        $.unblockUI();
    }
$scope.updateRating = function(rate) {
  $.blockUI({ message: '<h4> Update Rating...</h4>' });
      
        var reqData = {}
        reqData.id = rate.id;
        reqData.setID = rate.setID;
        reqData.ratingValue = rate.ratingValue;
        reqData.ratingDescription = rate.ratingDescription.replace(/^\s+/, '').replace(/\s+$/, '');;
        reqData.ratingTitle = rate.ratingTitle.replace(/^\s+/, '').replace(/\s+$/, '');;
        reqData.hexvalue = rate.hexvalue;

        
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      
      console.log('reg data for update' + JSON.stringify(reqData));
      
      console.log('kk '+ reqData.ratingValue.length + reqData.ratingDescription.length + reqData.ratingTitle.length + reqData.hexvalue.length)


      if(reqData.ratingDescription.length > 0 && reqData.ratingTitle.length > 0 && reqData.hexvalue.length > 0)
      {
         userService.updateRatingInDB(reqData, $scope.updateRatingSuccess)
         //resetAlert();
        //alertify.alert('lalalall.');
      }
      else
      {
        //$('#updateTagBtn').attr('disabled','disabled');
        resetAlert();
        alertify.alert('You cannot keep the fields empty.');
        $state.reload();
      }

     
    }


// to set modal to pristine after clicking close
$scope.resetModal = function()
{
  $scope.addRatingForm.$setUntouched(); // to make the form untouched
  $scope.addRatingForm.$setPristine();  // to make the form pristine
  $scope.ratings = '';
}

        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
app.filter('orderObjectBy', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }

    array.sort(function(a, b){
        a = parseInt(a[attribute]);
        b = parseInt(b[attribute]);
        return a - b;
    });
    return array;
 }
});