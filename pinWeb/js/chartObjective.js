var app = angular.module('pinChartsObjCtrl.controllers', []);
/***********************Controller for Objective******************/

app.controller('chartsCtrlObjective', ['$scope','userService','$state','$rootScope','localStorageService','$http','$location','$window',
  function($scope,userService, $state, $rootScope,localStorageService,$http,$location,$window) {
  //  console.log(JSON.stringify(localStorageService.get('authToken')));

/*********************** Google charts with trend lines *************************/
 //google.charts.load('current', {'packages':['corechart']});
$scope.drawGoogleChart = function(response)
{
  
        if(response.status == 'SUCCESS')
        {
           var path = []
        path = $location.search();
          console.log(response.data.chartsDataPoints.length);
           google.charts.setOnLoadCallback();

         google.charts.load('current', {'packages':['corechart']});
            
                 
                      console.log(JSON.stringify(changeChartDatatoFloat(response.data.chartsDataPoints[0].rows)));
                     // google.charts.setOnLoadCallback();

                      google.charts.setOnLoadCallback(drawCharts);
                      
                     
                      function drawCharts() {
                        var data = google.visualization.arrayToDataTable(changeChartDatatoFloat(response.data.chartsDataPoints[0].rows));
                        

                      
                        var options = {
                          title:  "OBJECTIVE",
                         hAxis: {title: 'Time', textStyle: {
                              color: '#525b60',
                              fontSize: '10'
                          }},
                          vAxis: {title: 'Rating'},
                          legend: { position: 'bottom' },
                          lineWidth: 2,
                          width:path.width,
                          height:250,
                          trendlines: { 0: {} }    // Draw a trendline for data series 0.
                        };

                        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                       
                        chart.draw(data, options);

                      }

      }
       else  
      {
        
         alert(response.msg);
      }
  
}
//end google charts

$scope.avgObjchartRender = function()
{
        var reqData = {}
        var xAuth = "x-auth-token";
        var paths = []
        paths = $location.search();
        console.log(JSON.stringify(paths));
        reqData.pinnee = paths.pinnee;

        console.log(paths.auth)
       
        reqData.xAuth = paths.auth;
        $http.defaults.headers.common['X-Auth-Token'] = paths.auth;
       // $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
        userService.getAvgObjectiveChartData(reqData, $scope.drawGoogleChart)
       
}
$scope.avgObjchartRender();
  
}])
