var app = angular.module('pinConsumerByMePinsCtrl.controllers', []);

app.controller('consumerPinsMyMeCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http',
	function($scope,userService, $state, $rootScope,localStorageService,$http) {
  

    if(localStorageService.get('userData') || $rootScope.user == {}) {
    $rootScope.user = localStorageService.get('userData');
  }
  else {
    
    delete $rootScope.user;  
  }

  $scope.activePart = 'step1';

  $scope.req = {};
  $scope.save = {};
  $scope.selDisplay = {};
  
  var newItem ={};
  
  $scope.item = [];
  $scope.userRelationship = [];
  $scope.editRelationsList = [];
  $scope.byList = [];
  $scope.allEmpList = [];
  $scope.allTags = [];
  $scope.allRatings = [];
  $scope.checkedTag = [];
  $scope.pinHistory = [];
  

  $scope.hasRelation = false;
  $scope.selected = 0;
  $scope.editMode = false;

  $scope.date = new Date();

    $scope.navDetails = function()
    {
    	if($rootScope.user)
		  {    	
			$scope.user = $rootScope.user; 
   		}    
    }

	console.log('We are in consumer');
  //panelHeight(); //for window height
  datePicker(); // for date picker
  scrollPageToTop(); //scrolling page to top 
  

// opening the wizard to add pin
$scope.openPinWizard= false;
 $scope.select= function(index,item) {
       $scope.selected = index; 
       $scope.selDisplay = item;
       
       return $scope.selDisplay; 
  };
 /* $scope.selectOnlyEMp= function(index,id) {
       $scope.selected = index; 
       
       $scope.selDisplay = item;
       
       return $scope.selDisplay; 
  };*/

  $scope.addPin= function(){
    $scope.openPinWizard= true;
    $scope.save = {};
    $scope.activePart = 'step1';
    $('input[type="checkbox"]:checked').removeAttr('checked');
  }
  $scope.cancelPin= function(){
     $state.forceReload();
    $scope.openPinWizard= false;
  }

// taking to the next step
  $scope.showNextStep = function(step){
        $scope.activePart = step;
        $('.step-pane').addClass('active');
  }



  /*    GET ALL USERS   */
$scope.getAllUserSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
              $scope.allEmpList = resData.data;

              $scope.selDisplay = resData.data[0];
              //console.log('first one '+JSON.stringify($scope.allEmpList))

            angular.forEach($scope.allEmpList, function(value, key){
              $scope.fullname = value.firstname+' '+value.lastname;
              $scope.empFinalList = value;
            });
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }

  $.unblockUI();
}


  $scope.getAllUsers = function() {
      $.blockUI({ message: '<h4> Loading Users...</h4>' });
      
      var reqData = {} 
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};     
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      userService.getUsersForConsumer(reqData,$scope.getAllUserSuccess);
  }
  $scope.getAllUsers();


/*    GET PINS BY ME   */

$scope.getByPinsSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          $.unblockUI();

          if (resData.data) {
             console.log('pingu' +JSON.stringify($scope.byList));

             $scope.byList = resData.data.pins;
             console.log(JSON.stringify($scope.byList));


            angular.forEach($scope.byList, function(value, key){
              //console.log(JSON.stringify($scope.byList));
            });
          }

          else{
            resetAlert();
            alertify.alert('No pins found.')
          }
      }
     
      else  
      {
        if(resData.msg == 'UNAUTHORIZED_ACCESS'){
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert(resData.msg);
                  
          $state.go('login', {},{'reload':true}); 
      }
      else{
          resetAlert();
          alertify.alert(resData.msg);
      }
  }

} 


  $scope.getByPins = function() {
      $.blockUI({ message: '<h4> Loading Pins...</h4>'});
      
      var reqData = {};
      reqData.pinner = localStorageService.get('userData').id;
      reqData.whoAmI = localStorageService.get('userData').id;
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      console.log('user data '+JSON.stringify(reqData))
  
      userService.getAllPins(reqData,$scope.getByPinsSuccess);
  }
  $scope.getByPins();


  $scope.refreshSlider = function () {
    console.log('df');
      //  $timeout(function () {
            //$scope.$broadcast('rzSliderForceRender');
            $rootScope.$broadcast('rzSliderForceRender');
      //  });
    };
   $scope.slider2 = {};


 $scope.rating =function(){
    if($rootScope.allRatings != [] && localStorageService.get('ratings') != [] )
    {
       $scope.refreshSlider()
      console.log(JSON.stringify(localStorageService.get('ratings')));
  var stepsArray = changeRatingarray(localStorageService.get('ratings'));
   console.log(JSON.stringify(stepsArray));
      $scope.slider2 = {
            value: 0,
            options: {
              showTicksValues: true,
              stepsArray: stepsArray
           } 
        };
    }
}



/*  GET META  */

$scope.getMetaSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          if (resData.data.length != 0) {
            $scope.allTags = resData.data.tagsList;
            $scope.allObj = resData.data.objectiveList;
            $scope.allRatings = resData.data.ratingList;
            $scope.ratingLength = parseInt($scope.allRatings.length) - 1;
          //  $rootScope.allRatings = resData.data.ratingList;
           // localStorageService.set('ratings',$rootScope.allRatings);

        

            angular.forEach($scope.allTags, function(value, key){
              $scope.objRelatedToTag = value.objectivesList;
             // console.log('objRelatedToTag '+JSON.stringify($scope.objRelatedToTag));
            });
          }
          else{
            resetAlert();
            alertify.alert('Tags not found.')
          }  
      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();
          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }

  $.unblockUI();
} 

 $scope.getMeta = function() {
      $.blockUI({ message: '<h4> Loading Pins...</h4>' });
      
      var reqData = {};

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      userService.getMetaData(reqData,$scope.getMetaSuccess);
  }
  $scope.getMeta();
$scope.showSlider = false;

/****************** GET MAGIC API *********************/


$scope.getMagicAPISuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
            if(resData.data.userRelationship && resData.data.userRelationship != undefined)
            {
              $scope.hasRelation = true;
              //$scope.save.rad1 = resData.data.userRelationship.user2_rel_role_description;
            }
            else{
              $scope.hasRelation = false;
            }


          if(resData.data.length != 0)
          {
            $scope.userRelationship = resData.data.userRelationship;
            $scope.editRelationsList = resData.data.editRelationsList;
            console.log('userRelationship '+JSON.stringify($scope.userRelationship));
            console.log('editRelationsList '+JSON.stringify($scope.editRelationsList));

            if ($scope.userRelationship == undefined && $scope.editRelationsList == undefined) 
            {
              $state.reload();  
              resetAlert();
              alertify.alert('You cannot pin yourself')
            }

              if(resData.data.isRatingPossible == true)
            {
               $scope.showSlider = true;
               $scope.rating();
            }
            else
            {
               $scope.showSlider = false;
            }
            angular.forEach($scope.userRelationship, function(value, key){
              //console.log(JSON.stringify($scope.empFinalList));
            });
          }
          else{
            resetAlert();
            alertify.alert(resData.msg)
          }
      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          $state.reload();
          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }

  $.unblockUI();
}



$scope.getMagicAPI = function(e_id){
$.blockUI({ message: '<h4> Loading dataS...</h4>' });
      var reqData = {};
      reqData.pinner = localStorageService.get('userData').id;
      reqData.pinnee = e_id;

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      console.log('ftats '+JSON.stringify(reqData))
     
      userService.magicAPI(reqData, $scope.getMagicAPISuccess);

}
$scope.checkforslider=function(){
if($scope.save != null && $scope.save.pinneeRelationship == '3')
            {
               $scope.showSlider = true;
              $scope.rating();
            }
}
/*****************  Adding Pin ******************/

$scope.savePinSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
              //$state.reload();  
              resetAlert();
              alertify.alert('PIN saved successfully');
              $scope.getByPins();
              $scope.openPinWizard= false;
              //smoothScroll(document.getElementById('megha'));

      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          $state.reload();
          resetAlert();
          alertify.alert(resData.msg);
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }

  
}


$scope.savePin = function() {

  var ratevalue = $('#ratingTextValue').text();
  
  if(ratevalue == 'NaN')
  {
    var rating = 0;
   
  }
   else
   {
    $scope.save.rating = parseInt($('#ratingTextValue').text());
        if(localStorageService.get('ratings') != []){
         
                 var rateId = getSliderId($scope.save.rating,localStorageService.get('ratings'));
               
                 console.log(rateId);
                var rating = parseInt(rateId);
              }
             
    }

  newItem.pinner = localStorageService.get('userData').id;
  newItem.pinnee = $scope.selDisplay.id;
  newItem.pinneeRelationship= parseInt($scope.save.pinneeRelationship);
  newItem.event = $scope.save.event;
  newItem.comment = $scope.save.comment;
  newItem.ratingSet = 1; // for now this will always be 1, later we shall have to change it
  newItem.rating = rating;
  newItem.tag = $scope.save.tags;
  newItem.objective = parseInt($scope.save.objective);
  newItem.pinDate = $scope.save.pinDate;

  $scope.item.push(newItem);
 
  var reqData = {};
  reqData = $scope.item[0];

  $http.defaults.headers.common = { 'Authorization' : undefined};      
  $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
  $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
  console.log('ftats '+JSON.stringify(reqData))
$.blockUI({ message: '<h4> Save Pin...</h4>' });
     
  userService.savePinToDB(reqData, $scope.savePinSuccess);
  
};

//getting checked tags
$('#check-button').click(function() {
$scope.save.tags=($('.Checkbox:checked').map(function() {
    return this.value;
}).get().join(', '));

console.log('meow '+$scope.save.tags)

});

$scope.tagsForDisplay = {};
$scope.objForDisplay = {};
$scope.relForDisplay = {};
$scope.showPreview = function(){

  $scope.relForDisplay = $('.Relation:checked').next().text();

  $scope.tagsForDisplay = ($('.Checkbox:checked').map(function() {
     return $(this).next("label").text().split(' ').join(',');
  }).get()).toString();

  $scope.objForDisplay = $('.objktive:checked').next().text();
  console.log( $scope.objForDisplay + ' lala '+ $scope.tagsForDisplay + 'lala' + $scope.relForDisplay)

}


/************** UPDATE RELATIONSHIP **************/
$scope.updateUserRelSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }
          $state.reload();
          resetAlert();
          alertify.alert('Relationship updated.');

      }
     
      else  
      {
        if(resData.status == "FAILURE"){

          $state.reload();
          resetAlert();
          alertify.alert('Relationship could not be updated.');
      }
      else{
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert('Unauthorized access');
                  
          $state.go('login', {},{'reload':true}); 
      }
  }
}

$scope.updateUserRel = function(){
      $scope.selDisplay.editedVal= $('#editedVal').val();
      var reqData = {};
      reqData.user1 = localStorageService.get('userData').id;
      reqData.user2 = $scope.selDisplay.id;
      reqData.user2_rel_role_id = parseInt($scope.selDisplay.editedVal);

      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
      console.log('ftats '+JSON.stringify(reqData))
     
      userService.updateRelFromDB(reqData, $scope.updateUserRelSuccess);

}

/********************* Get objectives by TAG ID *********************/

$scope.relatedObj = [];
$scope.foundObj = false;

$scope.getRelatedObjSuccess = function(resData) {
      if(resData.status == "SUCCESS")
      {
          if($rootScope.storageType == 'localStorage') 
          {
              localStorageService.set('userData',$rootScope.user);
              localStorageService.set('authToken', $rootScope.user.authstring);
          }

          if (resData.data.length != 0) {

              $scope.relatedObj = resData.data
              $scope.foundObj = true;

            angular.forEach($scope.relatedObj, function(value, key){
              //console.log(JSON.stringify($scope.byList));
            });

            console.log('ja '+JSON.stringify($scope.relatedObj))
            return $scope.relatedObj;
          }

          else
          {
              $scope.foundObj = false;
          }
      }
     
      else  
      {
        if(resData.status == "FAILURE"){
          
          resetAlert();
          alertify.alert(resData.msg);
      }

      else
      {
          delete $rootScope.user;  
          localStorageService.clearAll();

          resetAlert();
          alertify.alert(resData.msg);

          $state.go('login', {},{'reload':true}); 
      }
  }
} 


  $scope.getRelatedObj = function() {

      var reqData = {};
      reqData.tags = $scope.save.tags;
      
      //reset authorization header to undefined instead of deleteing.
      //Set the desire header/headers to undefined like this, then it will not affect the global settings
      $http.defaults.headers.common = { 'Authorization' : undefined};      
      $http.defaults.headers.common['X-Auth-Token'] = localStorageService.get('authToken');
      $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
      console.log('getRelatedObj  '+JSON.stringify(reqData))
  
      userService.getObjByTag(reqData,$scope.getRelatedObjSuccess);
  }







        function escapeRegExp(string)
        {
                return '(' 
                    + string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").trim().split(/\s+/).join('|') 
                    + ')';
        }
       $scope.search = '';
        var regex;
         
        $scope.$watch('search', function (value) {
            
            regex = new RegExp('\\b' + escapeRegExp(value), 'i');
        });
        
        $scope.filterBySearch = function(name) {
            if (!$scope.search) return true;
            return regex.test(name);
        };
  
}])

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });
