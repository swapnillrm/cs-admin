var app = angular.module('pinLoginCtrl.controllers', []);

app.controller('LoginCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http','$window',
	function($scope,userService, $state, $rootScope,localStorageService,$http,$window) {
  $.unblockUI();
   // manageMenuBar(false);
    $(".wrapperClass").css("padding",0);
    $('#sidebarRight').hide()
    $('#menu_bar').hide()
    $('.menuBar').hide()
    var storageType = localStorageService.getStorageType();
    console.log('storageType '+$rootScope.storageType);


  $scope.sidebar = false;

$scope.LoginUserSuccess = function(resData) {
	$.unblockUI();
	    if (resData.status = 'SUCCESS' && resData.data != null) 
	    {

	      $rootScope.user = resData.data;
	      $scope.user = resData.data;
	      $rootScope.loggedIn = true;
        $rootScope.authToken = resData.data.authstring;



              if($rootScope.storageType == 'localStorage') 
              {
                  localStorageService.set('userData',$rootScope.user);
                  localStorageService.set('authToken', $rootScope.user.authstring);
              }

              $scope.sidebar = true;
              //$scope.notifications();
              if(resData.data.role =="ROLE_ADMIN")
                {
                  $rootScope.isAdmin = true;
                  $http.defaults.headers.common['Authorization'] = 'Basic ' +resData.data.authstring;
                  $state.go('chome',{});
                }

                else
                {
                  $rootScope.isAdmin = false;
                  $http.defaults.headers.common['Authorization'] = 'Basic ' +resData.data.authstring;
                  $state.go('chome',{});
                }
	    } 
	    else {
            if (resData.status = 'FAILURE')
      	    {
      	      resetAlert();
      	      alertify.alert(resData.msg);
      	      
      	    }
          else{
                $state.reload()
                resetAlert();
                alertify.alert('Try again.');
              } 
            }   
	}
  
   
  $rootScope.isAdmin = false;
     $scope.login = function() {
      $.blockUI({ message: '<h4> Loading...</h4>',timeout: 2500});

     	var reqData= {}
        reqData.uName = $scope.userName;
        reqData.pwd = $scope.passWord ;
        $http.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
        $http.defaults.headers.post['Accept'] = 'application/json,text/javascript';

    
        $http.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
       
        $http.defaults.headers.common['Authorization'] = 'Basic ' +btoa($scope.userName + ':' + $scope.passWord);
        $http.defaults.headers.common['Access-Control-Request-Method'] = 'GET';
        userService.loginUser(reqData, $scope.LoginUserSuccess);
    }




 



 $scope.sendMailSuccess = function(response)
    {
        if(response.status == 'SUCCESS')
        {
            resetAlert();
            alertify.alert("Check Your Mail to Reset Your Password");
             //$.unblockUI();
              closeModal();
             $state.go('login', {}, {reload: true}); 

        }
        else
        {
            resetAlert();
            alertify.alert("Your Mail Id Is not registered with us Please Signup");
           // $.unblockUI();
           closeModal();
            $state.go('login', {}, {reload: true}); 

        }

    }
    $('#resetbtn').one('click', function() {
    $('#resetbtn').attr('disabled',true);
}); 

    $scope.resetPasswordSend = function()
    {
      // blockUI();
        var reqData = {}
        reqData.email = $scope.resetEmail;
        userService.sendEmailInlink(reqData, $scope.sendMailSuccess)
            
    }
  


  
}])


//base64 factory

/*app.factory('Base64', function() {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
            'QRSTUVWXYZabcdef' +
            'ghijklmnopqrstuv' +
            'wxyz0123456789+/' +
            '=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };
});*/