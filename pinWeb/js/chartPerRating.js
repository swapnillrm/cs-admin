var app = angular.module('pinChartsperRatingCtrl.controllers', []);

app.controller('chartsperRatingCtrl', ['$scope','userService', '$state','$rootScope','localStorageService','$http','$location','$window',
  function($scope,userService, $state, $rootScope,localStorageService,$http,$location,$window) {
  //  console.log(JSON.stringify(localStorageService.get('authToken')));

/*********************** Google charts with trend lines *************************/
 //google.charts.load('current', {'packages':['corechart']});
$scope.drawGoogleChart = function(response)
{
  
        if(response.status == 'SUCCESS')
        {
           
          console.log(response.data.chartsDataPoints.length);
           google.charts.setOnLoadCallback();

         google.charts.load('current', {'packages':['corechart']});
            
                 
                      console.log(JSON.stringify(response.data.chartsDataPoints));
                     // google.charts.setOnLoadCallback();

                      google.charts.setOnLoadCallback(drawCharts);
                      
                     var path= [];
                     path =  $location.search();
                      function drawCharts() {
                        var data = google.visualization.arrayToDataTable(changeChartData(response.data.chartsDataPoints));
                         var titles = response.data.chartsDataPoints[0].xTitle;

                       // console.log(changeChartData(response.data.chartsDataPoints));
                        var options = {
                          title:  titles.toUpperCase(),
                          hAxis: {title: 'Time', textStyle: {
                color: '#525b60',
                fontSize: '10'
            }},
                          vAxis: {title: 'Rating'},
                          legend: { position: 'bottom' },
                          lineWidth: 2,
                          width:path.width,
                          height:250,
                          trendlines: { 0: {} }    // Draw a trendline for data series 0.
                        };

                        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
                       
                        chart.draw(data, options);

                      }

      }
       else  
      {
        
        //alert(response.msg);
      }
  
}
//end google charts

$scope.avgchartRenderPerObjective = function()
{
        var reqData = {}
        var xAuth = "x-auth-token";
        var path = []
        path = $location.search();
        console.log(JSON.stringify(path.auth));
        reqData.pinnee = path.pinnee;
        reqData.objective = path.objective;
        
        reqData.xAuth = path.auth;
        $http.defaults.headers.common['X-Auth-Token'] = path.auth;
        $http.defaults.headers.common['Access-Control-Request-Method'] = 'POST';
        userService.getAvgPerObjectiveChartData(reqData, $scope.drawGoogleChart)
       
}
$scope.avgchartRenderPerObjective();
  
}])

